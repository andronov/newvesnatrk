from django.conf.urls import patterns, include, url
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from filebrowser.sites import site

admin.autodiscover()

api_packages_urls = patterns(
    '',
    url(r'^captcha/', include('spring.captcha.urls')),
    url(r'^django-captcha/', include('captcha.urls')),
    url(r'^shops/', include('spring.shops.urls')),
    url(r'^content/', include('spring.mainpage.urls')),
    url(r'^mainmenu/', include('spring.mainmenu.urls')),
    url(r'^events/', include('spring.events.urls')),
    url(r'^events/', include('spring.subscription.urls')),
    url(r'^entertainments/', include('spring.entertainments.urls')),
    url(r'^cinema/', include('spring.cinema.urls')),
    url(r'^children/', include('spring.children.urls')),
    url(r'^gallery/', include('spring.gallery.urls')),
    url(r'^ga/', include('spring.google_analytics.urls')),
    url(r'^about/', include('spring.about.urls')),
    url(r'^search/', include('spring.search.urls')),
    url(r'^wifi/$', 'spring.subscription.views.wifi')
)

flat_page_urls = patterns(
    'spring.seopages.views',
    url(r'^(?P<url>[a-zA-Z0-9]*)$', 'flat_page')
)


api_urls = i18n_patterns(
    '',
    url(r'^api/', include(api_packages_urls, app_name='api')),
    url(r'^', include(flat_page_urls, app_name='api'))
)

urlpatterns = patterns(
    '',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^admin/filebrowser/', include(site.urls)),
    url(r'^ckeditor/', include('ckeditor.urls')),
    url(r'^social/', include('social.apps.django_app.urls', namespace='social')),
    url(r'^update_sms_status/', 'spring.subscription.views.update_sms_status'),
)

urlpatterns += api_urls
