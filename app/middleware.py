# -*- coding: utf-8 -*-

from django import http
from rest_framework.status import is_success, is_client_error, is_server_error

from spring.site.models import Settings
from spring.site.serializers import SettingSerializer


try:
    from django.conf import settings
    XHR_SHARING_ALLOWED_ORIGINS = settings.XHR_SHARING_ALLOWED_ORIGINS
    XHR_SHARING_ALLOWED_HEADERS = settings.XHR_SHARING_ALLOWED_HEADERS
    XHR_SHARING_ALLOWED_METHODS = settings.XHR_SHARING_ALLOWED_METHODS
except AttributeError:
    XHR_SHARING_ALLOWED_ORIGINS = '*'
    XHR_SHARING_ALLOWED_HEADERS = ['X-Requested-With', 'X-File-Name', 'Content-Type']
    XHR_SHARING_ALLOWED_METHODS = ['POST', 'GET', 'OPTIONS', 'PUT', 'DELETE']


class XhrSharing(object):
    @staticmethod
    def process_request(request):
        if 'HTTP_ACCESS_CONTROL_REQUEST_METHOD' in request.META:
            response = http.HttpResponse()
            response['Access-Control-Allow-Origin'] = XHR_SHARING_ALLOWED_ORIGINS
            response['Access-Control-Allow-Headers'] = ','.join(XHR_SHARING_ALLOWED_HEADERS)
            response['Access-Control-Allow-Methods'] = ','.join(XHR_SHARING_ALLOWED_METHODS)

            return response

        return None

    @staticmethod
    def process_response(request, response):
        if response.has_header('Access-Control-Allow-Origin'):
            return response

        response['Access-Control-Allow-Origin'] = XHR_SHARING_ALLOWED_ORIGINS
        response['Access-Control-Allow-Headers'] = ','.join(XHR_SHARING_ALLOWED_HEADERS)
        response['Access-Control-Allow-Methods'] = ','.join(XHR_SHARING_ALLOWED_METHODS)

        return response


class StatusResponseMiddleware(object):
    @staticmethod
    def process_template_response(request, response):
        if request.resolver_match.app_name == 'api':
            if is_success(response.status_code):
                response.data.update({
                    'settings': SettingSerializer(
                        Settings.objects.get(),
                        context={'request': request}
                    ).data
                })
                response.data = {
                    'status': 'success',
                    'data': response.data,
                }
                if request.method == 'POST' and not response.data['data']:
                    del response.data['data']
            elif is_client_error(response.status_code) or is_server_error(response.status_code):
                response.data = {
                    'status': 'error',
                    'error': response.data
                }
        return response
