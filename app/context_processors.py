# -*- coding: utf-8 -*-
from django.utils.translation import get_language


def current_lang(request):
    return {'current_lang': get_language()}