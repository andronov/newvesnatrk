# -*- coding: utf-8 -*-

CONFIG = __import__('app.config').config

# Debug settings
TEMPLATE_DEBUG = DEBUG = False

# Kinohod Cinema Id
KINOHOD_CINEMA_ID = 674

# Kinohod Api Key
KINOHOD_API_KEY = 'abd3307f-cb32-3155-b857-66b78f251fd7'
KINOHOD_CLIENT_API_KEY = 'bf28549b-f1c0-3d1c-9dd3-0444f46e0d62'

# Kinohod Server Url
KINOHOD_SERVER_URL = 'http://kinohod.ru'

# Kinopoisk Rating Url
KINOPOISK_RATING_URL = 'https://www.kinopoisk.ru/search/chrometoolbar.php?v=1&query='

# Rotten Tomatoes Api Key
TOMATOES_API_KEY = 'cau3zgdatup5aaekhj9yetr4'

# Rotten Tomatoes Server Url
TOMATOES_SERVER_URL = 'http://api.rottentomatoes.com'

# Project's title which will be shown in admin interface
GRAPPELLI_ADMIN_TITLE = 'Весна'

PROJECT_DIR = CONFIG.PATHS['APP_DIR']

# Default project dashboard, you probably won't need to change it
GRAPPELLI_INDEX_DASHBOARD = 'app.dashboard.CustomIndexDashboard'

# Don't change this for filebrowser to work properly
FILEBROWSER_DIRECTORY = ''

# Default filebrowser admin thumbnail size
FILEBROWSER_ADMIN_THUMBNAIL = 'medium'

# Default email address to use for various automated correspondence from the site manager(s).
DEFAULT_FROM_EMAIL = 'postmaster@vesna-trk.ru'

# The email address that error messages come from, such as those sent to ADMINS and MANAGERS.
SERVER_EMAIL = 'vesna-trk.ru <postmaster@vesna-trk.ru>'

# A tuple that lists people who get code error notifications. When DEBUG=False and a view raises
# an exception, Django will email these people with the full exception information. Each member
# of the tuple should be a tuple of (Full name, email address).
ADMINS = [('info', 'info@vesna-trk.ru')]
MANAGERS = (('error', 'error@ailove.ru'),)

# EMAIL_HOST = 'mx26.valuehost.ru'
# EMAIL_HOST_USER = 'postmaster@vesna-trk.ru'
# EMAIL_HOST_PASSWORD = 'vpeNB2'
#
# EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

# Subject-line prefix for email messages
EMAIL_SUBJECT_PREFIX = ''

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',  # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'
        'NAME': CONFIG.SETTINGS['DB_NAME'],                  # Or path to database file if using sqlite3
        'USER': CONFIG.SETTINGS['DB_USER'],                  # Not used with sqlite3
        'PASSWORD': CONFIG.SETTINGS['DB_PASSWORD'],          # Not used with sqlite3
        'HOST': CONFIG.SETTINGS['DB_HOST'],                  # Set to empty string for localhost. Not used with sqlite3
        'PORT': '',                                          # Set to empty string for default. Not used with sqlite3
    }
}

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.4/ref/settings/#allowed-hosts
ALLOWED_HOSTS = ['*']

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'Europe/Moscow'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'ru'

FILE_UPLOAD_PERMISSIONS = 0777

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# The directory where uploaded files larger than FILE_UPLOAD_MAX_MEMORY_SIZE will be stored
FILE_UPLOAD_TEMP_DIR = CONFIG.PATHS['TMP_DIR']

# Set this to control where Django stores session files in case if file-based session storage is used
SESSION_FILE_PATH = CONFIG.PATHS['TMP_DIR']

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = CONFIG.PATHS['DATA_DIR']

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/data/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = CONFIG.PATHS['STATIC_DIR']

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    # 'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# A tuple of directories where Django looks for translation files
LOCALE_PATHS = (
    CONFIG.PATHS['APP_DIR'] + 'app/locale/',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'kjsgfi983475938yoirhf98yw4hf93q84fhq03r498q3fh834g379fgqgfdssss'

# CKEditor directory for uploaded files
CKEDITOR_UPLOAD_PATH = MEDIA_ROOT + 'ckeditor/admin'

# CKEditor main config
CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': [
            {'name': 'document', 'items': ['Source']},
            {'name': 'clipboard',
             'items': ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
            {'name': 'editing', 'items': ['Find', 'Replace', '-', 'SelectAll', '-', 'SpellChecker', 'Scayt']},
            {'name': 'basicstyles',
             'items': ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
            '/',
            {'name': 'paragraph',
             'items': ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv',
                       '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr',
                       'BidiRtl']},
            {'name': 'links', 'items': ['Link', 'Unlink', 'Anchor']},
            {'name': 'insert', 'items': ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar']},
            '/',
            {'name': 'styles', 'items': ['Styles', 'Format', 'Font', 'FontSize']},
            {'name': 'colors', 'items': ['TextColor', 'BGColor']},
            {'name': 'tools', 'items': ['Maximize', 'ShowBlocks']}
        ],
        'filebrowserImageBrowseUrl': '/admin/filebrowser/browse?pop=3',
        'removeDialogTabs': 'link:upload;image:Upload;flash:Upload',
        'height': 300,
        'width': 755,
    },

    'big': {
        'toolbar': [
            {'name': 'document', 'items': ['Source']},
            {'name': 'clipboard',
             'items': ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
            {'name': 'editing', 'items': ['Find', 'Replace', '-', 'SelectAll', '-', 'SpellChecker', 'Scayt']},
            {'name': 'basicstyles',
             'items': ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
            '/',
            {'name': 'paragraph',
             'items': ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv',
                       '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr',
                       'BidiRtl']},
            {'name': 'links', 'items': ['Link', 'Unlink', 'Anchor']},
            {'name': 'insert', 'items': ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar']},
            '/',
            {'name': 'styles', 'items': ['Styles', 'Format', 'Font', 'FontSize']},
            {'name': 'colors', 'items': ['TextColor', 'BGColor']},
            {'name': 'tools', 'items': ['Maximize', 'ShowBlocks']}
        ],
        'filebrowserImageBrowseUrl': '/admin/filebrowser/browse?pop=3',
        'removeDialogTabs': 'link:upload;image:Upload;flash:Upload',
        'height': 300,
        'width': 755,
    },
    'small': {
        'toolbar': [
            {'name': 'document', 'items': ['Source']},
            {'name': 'clipboard',
             'items': ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
            {'name': 'editing', 'items': ['Find', 'Replace', '-', 'SelectAll', '-', 'SpellChecker', 'Scayt']},
            {'name': 'basicstyles',
             'items': ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
            '/',
            {'name': 'paragraph',
             'items': ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv',
                       '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr',
                       'BidiRtl']},
            {'name': 'links', 'items': ['Link', 'Unlink', 'Anchor']},
            {'name': 'insert', 'items': ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar']},
            '/',
            {'name': 'styles', 'items': ['Styles', 'Format', 'Font', 'FontSize']},
            {'name': 'colors', 'items': ['TextColor', 'BGColor']},
            {'name': 'tools', 'items': ['Maximize', 'ShowBlocks']}
        ],
        'filebrowserImageBrowseUrl': '/admin/filebrowser/browse?pop=3',
        'removeDialogTabs': 'link:upload;image:Upload;flash:Upload',
        'height': 200,
        'width': 755,
    },
}

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    #'django.template.loaders.eggs.Loader',
)


# Template context processors
TEMPLATE_CONTEXT_PROCESSORS = (
    'social.apps.django_app.context_processors.backends',
    'social.apps.django_app.context_processors.login_redirect',
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.request',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.tz',
    'django.contrib.messages.context_processors.messages',
    'app.context_processors.current_lang'
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    #'django.middleware.csrf.CsrfViewMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'app.middleware.StatusResponseMiddleware',
    'app.middleware.XhrSharing'
    #'app.middleware.CacheResetMiddleware'
    #Uncomment the next line for simple clickjacking protection:
    #'django.middleware.clickjacking.XFrameOptionsMiddleware',
)
AUTHENTICATION_BACKENDS = (
    'social.backends.facebook.FacebookOAuth2',
    'social.backends.vk.VKOAuth2',
    'django.contrib.auth.backends.ModelBackend',
)
ROOT_URLCONF = 'app.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'app.wsgi.application'

TEMPLATE_DIRS = (
    CONFIG.PATHS['APP_DIR'] + 'app/templates/',
)

INSTALLED_APPS = (
    'social.apps.django_app.default',
    'ckeditor',
    'south',
    'utilities',
    'utilities.capable',
    'utilities.templatetags.common',
    'grappelli.dashboard',
    'grappelli',
    'filebrowser',
    'colorful',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'spring.shops',
    'spring.events',
    'spring.subscription',
    'spring.entertainments',
    'spring.children',
    'spring.gallery',
    'spring.about',
    'spring.cinema',
    'spring.site',
    'spring.seopages',
    'spring.mainpage',
    'spring.mainmenu',
    'spring.user',
    'captcha',
    'spring.captcha',
    'spring.search',
    'rest_framework',
    'spring.google_analytics',
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'main_formatter': {
            'format': '%(levelname)s:%(name)s: %(message)s '
                      '(%(asctime)s; %(filename)s:%(lineno)d)',
            'datefmt': "%Y-%m-%d %H:%M:%S",
        },
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'main_formatter',
        },
        'production_file': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': 'main.log',
            'maxBytes': 1024 * 1024 * 5,  # 5 MB
            'backupCount': 7,
            'formatter': 'main_formatter',
        },
        'debug_file': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': 'main_debug.log',
            'maxBytes': 1024 * 1024 * 5,  # 5 MB
            'backupCount': 7,
            'formatter': 'main_formatter',
        },
        'null': {
            "class": 'django.utils.log.NullHandler',
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins', 'console'],
            'level': 'ERROR',
            'propagate': True,
        },
        'django': {
            'handlers': ['null', ],
        },
        'py.warnings': {
            'handlers': ['null', ],
        },
        '': {
            'handlers': ['console', 'production_file', 'debug_file'],
            'level': "DEBUG",
        },
    }
}

USE_SHORT_LANGUAGE_CODES = True

FALLBACK_LANGUAGES = {
    'ru': [],
    'en': ['ru'],
}

gettext = lambda s: s

LANGUAGES = (
    ('ru', gettext('Russian')),
    ('en', gettext('English')),
)

LANGUAGE_COOKIE_NAME = 'current_language'

FILEBROWSER_VERSIONS = {
    'thumbnail': {'verbose_name': 'Thumbnail', 'width': 80, 'height': '', 'opts': ''},
    '180x130': {'verbose_name': '180x130', 'width': 180, 'height': 130, 'opts': 'crop'},
    '130x130': {'verbose_name': '130x130', 'width': 130, 'height': 130, 'opts': 'resize'},
    '369x268': {'verbose_name': '369x268', 'width': 369, 'height': 268, 'opts': 'crop'},
    '310x150': {'verbose_name': '310x150', 'width': 310, 'height': 150, 'opts': 'crop'},
    '690x150': {'verbose_name': '690x150', 'width': 690, 'height': 150, 'opts': 'crop'},
    '692x500': {'verbose_name': '692x500', 'width': 692, 'height': 500, 'opts': 'crop'},
    '230_mail': {'verbose_name': '230_mail', 'width': 230, 'height': '', 'opts': 'resize'},
    'small': {'verbose_name': 'Small (2 col)', 'width': 140, 'height': '', 'opts': ''},
    'small_crop': {'verbose_name': 'Small (2 col)', 'width': 140, 'height': 140, 'opts': 'crop'},
    'medium': {'verbose_name': 'Medium (4col )', 'width': 300, 'height': '', 'opts': ''},
    'medium_crop': {'verbose_name': 'Medium (4col )', 'width': 300, 'height': 300, 'opts': 'crop'},
    'large': {'verbose_name': 'Large (8 col)', 'width': 680, 'height': '', 'opts': ''},
    'big': {'verbose_name': 'Big (8 col)', 'width': 680, 'height': '', 'opts': ''},
    'large_crop': {'verbose_name': 'Large (8 col)', 'width': 680, 'height': 680, 'opts': 'crop'},
    'big_poster': {'verbose_name': 'Big Poster', 'width': 448, 'height': 640, 'opts': ''},
    'small_poster': {'verbose_name': 'Small Poster', 'width': 180, 'height': 230, 'opts': ''},
    'mail_poster': {'verbose_name': 'Mail Poster', 'width': 225, 'height': 225, 'opts': 'crop'},
    'main_page_poster': {'verbose_name': 'Main Page Poster', 'width': 290, 'height': 290, 'opts': 'crop'},
    'entertainment_page_poster': {
        'verbose_name': 'Entertainment Page Poster',
        'width': 311,
        'height': 311,
        'opts': 'crop'
    },
    'calendar_image_160': {
        'verbose_name': 'calendar_image_160',
        'width': 160,
        'height': '',
        'opts': 'resize'
    },
    'calendar_image_90': {
        'verbose_name': 'calendar_image_160',
        'width': '',
        'height': 90,
        'opts': 'crop'
    },
    'event_list_image_500_height': {
        'verbose_name': 'event_list_image_500_height',
        'width': '',
        'height': 500,
        'opts': 'resize'
    },
    'entertainment_100_width': {
        'verbose_name': 'entertainment_100_width',
        'width': 100,
        'height': '',
        'opts': 'resize'},
    'entertainment_100_height': {
        'verbose_name': 'entertainment_100_height',
        'width': '',
        'height': 100,
        'opts': 'resize'
    },
    'brand_logo_width': {
        'verbose_name': 'brand_logo_width',
        'width': 100,
        'height': '',
        'opts': 'resize'
    },
    'brand_logo_height': {
        'verbose_name': 'brand_logo_height',
        'width': '',
        'height': 100,
        'opts': 'resize'
    }
}

FILEBROWSER_EXTENSIONS = {
    'Folder': [''],
    'Image': ['.jpg', '.jpeg', '.gif', '.png', '.tif', '.tiff'],
    'Archive': ['.rar', '.zip'],
    'Presentation': ['.pdf', '.ppt', '.key', '.doc', '.docx', '.rar', '.zip'],
    'Document': ['.doc', '.rtf', '.txt', '.xls', '.csv'],
    'Video': ['.mov', '.wmv', '.mpeg', '.mpg', '.avi', '.rm'],
    'Audio': ['.mp3', '.mp4', '.wav', '.aiff', '.midi', '.m4p']
}

FILEBROWSER_MAX_UPLOAD_SIZE = 5242880  # 5mb

# Login error url
LOGIN_ERROR_URL = '/'

# Social auth pipeline

SOCIAL_AUTH_PIPELINE = (
    'spring.user.social.pipeline.admin_check',
    'social.pipeline.social_auth.social_details',
    'social.pipeline.social_auth.social_uid',
    'social.pipeline.social_auth.auth_allowed',
    # 'social.pipeline.social_auth.social_user',
    'spring.user.social.pipeline.social_user',
    'social.pipeline.user.create_user',
    'social.pipeline.social_auth.associate_user',
    'social.pipeline.social_auth.load_extra_data',
    'social.pipeline.user.user_details',
)

SOCIAL_AUTH_LOGIN_REDIRECT_URL = 'http://172.16.200.253/capture/handler.py/index?appid=6'
# Facebook Oauth2 Key
SOCIAL_AUTH_FACEBOOK_KEY = CONFIG.SOCIAL.FB_APP_ID

# Facebook Oauth2 Secret
SOCIAL_AUTH_FACEBOOK_SECRET = CONFIG.SOCIAL.FB_APP_SECRET

# Facebook Oauth2 Extended Permissions
SOCIAL_AUTH_FACEBOOK_SCOPE = ['user_likes']

# Facebook Oauth2 Extra Data
# SOCIAL_AUTH_FACEBOOK_EXTRA_DATA = [
#     ('gender', 'gender'),
#     ('birthday', 'birthday'),
#     ('location', 'location'),
# ]

# VK Oauth2 Key
SOCIAL_AUTH_VK_OAUTH2_KEY = CONFIG.SOCIAL.VK_APP_ID

# VK Oauth2 Secret
SOCIAL_AUTH_VK_OAUTH2_SECRET = CONFIG.SOCIAL.VK_APP_SECRET

# VK Oauth2 Extra Data
# SOCIAL_AUTH_VK_OAUTH2_EXTRA_DATA = [
#     'sex',
#     'bdate',
#     'city',
#     'country',
#     'photo_big',
# ]

SOCIAL_AUTH_MERGE_ACCOUNTS = False

#CACHES = {
#    'default': {
#        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
#        'LOCATION': '127.0.0.1:11211',
#    }
#}

REST_FRAMEWORK = {
    'DEFAULT_RENDERER_CLASSES': ('rest_framework.renderers.UnicodeJSONRenderer',),
    'DEFAULT_AUTHENTICATION_CLASSES': ('utilities.permissions.SessionDisableCSRFAuthentication',)
}

SMS_API_KEY = 'b4615b9b-993e-c064-710f-f8c1ccf35bb5'

if CONFIG.SETTINGS['ENV'] == 'test':
    from settings_test import *
if CONFIG.SETTINGS['ENV'] == 'dev' or CONFIG.SETTINGS['ENV'] == 'local':
    from settings_dev import *
