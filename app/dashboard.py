# -*- coding: utf-8 -*-

from django.utils.translation import ugettext_lazy as _
from grappelli.dashboard import modules, Dashboard


class CustomIndexDashboard(Dashboard):
    def init_with_context(self, context):
        self.children.append(modules.Group(
            title=_('Administration'),
            column=1,
            collapsible=True,
            children=[
                modules.ModelList(
                    title=_('Users'),
                    models=('django.contrib.*', 'spring.site.*')
                )
            ]
        ))

        self.children.append(modules.Group(
            title=_('Site'),
            column=1,
            collapsible=True,
            children=[
                modules.ModelList(
                    title=_('Shops'),
                    models=('spring.shops.*',)
                ),
                modules.ModelList(
                    title=_('Actions and events'),
                    models=('spring.events.models.*',),
                    exclude=('spring.events.models.BaseEvent',)
                ),
                modules.ModelList(
                    title=_('Subscription'),
                    models=('spring.subscription.*',)
                ),
                modules.ModelList(
                    title=_('Entertainment'),
                    models=('spring.entertainments.*',)
                ),
                modules.ModelList(
                    title=_('Gallery'),
                    models=('spring.gallery.*',)
                ),
                modules.ModelList(
                    title=_('About mall'),
                    models=('spring.about.*',)
                ),
                modules.ModelList(
                    title=_('Cinema'),
                    models=('spring.cinema.*',)
                ),
                modules.ModelList(
                    title=_('SEO'),
                    models=('spring.seopages.*',)
                ),
                modules.ModelList(
                    title=_('Page content'),
                    models=(
                        'spring.mainpage.*',
                        'spring.mainmenu.*',
                    )
                ),
            ]
        ))
        self.children.append(modules.LinkList(
            _('Media'),
            column=2,
            children=[
                {
                    'title': _('FileBrowser'),
                    'url': '/admin/filebrowser/browse/',
                    'external': False,
                },
            ]
        ))
        self.children.append(modules.RecentActions(
            _('Recent Actions'),
            limit=5,
            collapsible=True,
            column=3,
        ))
