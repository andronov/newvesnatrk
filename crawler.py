from scrapy.selector import Selector
from scrapy.spider import Spider
from scrapy.http import Request
from urlparse import urlparse, urljoin


class MySpider(Spider):
    name = 'Crawler'

    def __init__(self, domain, *args, **kwargs):
        super(MySpider, self).__init__(*args, **kwargs)
        self.domain = domain

        self.allowed_domains = [urlparse(self.domain).netloc]
        self.start_urls = [
            urljoin(self.domain, '?_escaped_fragment_=/')
        ]

    def parse(self, response):

        sel = Selector(response)

        for url in sel.xpath('//a/@href').extract():
            if 'http://' not in url and 'https://' not in url:
                yield Request('%s/?_escaped_fragment_=%s' % (self.domain.rstrip('/'), url), callback=self.parse)