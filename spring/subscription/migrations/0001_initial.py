# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Subscriber'
        db.create_table(u'subscription_subscriber', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('is_agree', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('first_name', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('last_name', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('city', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('is_male', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(unique=True, max_length=255)),
        ))
        db.send_create_signal(u'subscription', ['Subscriber'])

        # Adding model 'SubscriptionLog'
        db.create_table(u'subscription_subscriptionlog', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('for_male', self.gf('django.db.models.fields.NullBooleanField')(default=None, null=True, blank=True)),
            ('message', self.gf('ckeditor.fields.RichTextField')(max_length=1000)),
            ('subject', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('age_from', self.gf('django.db.models.fields.PositiveSmallIntegerField')(max_length=2, null=True, blank=True)),
            ('age_to', self.gf('django.db.models.fields.PositiveSmallIntegerField')(max_length=2, null=True, blank=True)),
        ))
        db.send_create_signal(u'subscription', ['SubscriptionLog'])

        # Adding M2M table for field subscribers on 'SubscriptionLog'
        m2m_table_name = db.shorten_name(u'subscription_subscriptionlog_subscribers')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('subscriptionlog', models.ForeignKey(orm[u'subscription.subscriptionlog'], null=False)),
            ('subscriber', models.ForeignKey(orm[u'subscription.subscriber'], null=False))
        ))
        db.create_unique(m2m_table_name, ['subscriptionlog_id', 'subscriber_id'])

        # Adding model 'SubscriptionExample'
        db.create_table(u'subscription_subscriptionexample', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('message', self.gf('ckeditor.fields.RichTextField')(max_length=1000)),
            ('title', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
        ))
        db.send_create_signal(u'subscription', ['SubscriptionExample'])

        # Adding model 'SubscriptionMessage'
        db.create_table(u'subscription_subscriptionmessage', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('is_readed', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('subscriber', self.gf('django.db.models.fields.related.ForeignKey')(related_name='subscription_messages', to=orm['subscription.Subscriber'])),
            ('token', self.gf('django.db.models.fields.CharField')(default='', max_length=32)),
        ))
        db.send_create_signal(u'subscription', ['SubscriptionMessage'])


    def backwards(self, orm):
        # Deleting model 'Subscriber'
        db.delete_table(u'subscription_subscriber')

        # Deleting model 'SubscriptionLog'
        db.delete_table(u'subscription_subscriptionlog')

        # Removing M2M table for field subscribers on 'SubscriptionLog'
        db.delete_table(db.shorten_name(u'subscription_subscriptionlog_subscribers'))

        # Deleting model 'SubscriptionExample'
        db.delete_table(u'subscription_subscriptionexample')

        # Deleting model 'SubscriptionMessage'
        db.delete_table(u'subscription_subscriptionmessage')


    models = {
        u'subscription.subscriber': {
            'Meta': {'object_name': 'Subscriber'},
            'city': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '255'}),
            'first_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_agree': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_male': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'})
        },
        u'subscription.subscriptionexample': {
            'Meta': {'object_name': 'SubscriptionExample'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('ckeditor.fields.RichTextField', [], {'max_length': '1000'}),
            'title': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'})
        },
        u'subscription.subscriptionlog': {
            'Meta': {'object_name': 'SubscriptionLog'},
            'age_from': ('django.db.models.fields.PositiveSmallIntegerField', [], {'max_length': '2', 'null': 'True', 'blank': 'True'}),
            'age_to': ('django.db.models.fields.PositiveSmallIntegerField', [], {'max_length': '2', 'null': 'True', 'blank': 'True'}),
            'for_male': ('django.db.models.fields.NullBooleanField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('ckeditor.fields.RichTextField', [], {'max_length': '1000'}),
            'subject': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'subscribers': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'subscription_logs'", 'symmetrical': 'False', 'to': u"orm['subscription.Subscriber']"})
        },
        u'subscription.subscriptionmessage': {
            'Meta': {'object_name': 'SubscriptionMessage'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_readed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'subscriber': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'subscription_messages'", 'to': u"orm['subscription.Subscriber']"}),
            'token': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '32'})
        }
    }

    complete_apps = ['subscription']