# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Subscriber.created_at'
        db.delete_column(u'subscription_subscriber', 'created_at')


    def backwards(self, orm):
        # Adding field 'Subscriber.created_at'
        db.add_column(u'subscription_subscriber', 'created_at',
                      self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, null=True, blank=True),
                      keep_default=False)


    models = {
        u'cinema.movie': {
            'Meta': {'object_name': 'Movie'},
            'actors_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'actors_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'age_restriction': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'circle_image': ('filebrowser.fields.FileBrowseField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'countries_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'countries_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'directors_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'directors_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'duration': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'genre_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'genre_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_premiere': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'kinohod_id': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'long_desc_en': ('ckeditor.fields.RichTextField', [], {'max_length': '2000', 'null': 'True', 'blank': 'True'}),
            'long_desc_ru': ('ckeditor.fields.RichTextField', [], {'max_length': '2000', 'null': 'True'}),
            'poster_en': ('filebrowser.fields.FileBrowseField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'poster_ru': ('filebrowser.fields.FileBrowseField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'producers_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'producers_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'rating': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'scenario_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'scenario_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'short_desc_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'short_desc_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'super_poster_en': ('filebrowser.fields.FileBrowseField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'super_poster_ru': ('filebrowser.fields.FileBrowseField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'})
        },
        u'entertainments.entertainment': {
            'Meta': {'object_name': 'Entertainment'},
            'contacts': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'description_en': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '2000', 'blank': 'True'}),
            'description_ru': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '2000'}),
            'description_short_en': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '500', 'blank': 'True'}),
            'description_short_ru': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '500'}),
            'description_title_en': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '200', 'blank': 'True'}),
            'description_title_ru': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '200'}),
            'for_children': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'internal_page_image': ('filebrowser.fields.FileBrowseField', [], {'max_length': '254', 'blank': 'True'}),
            'internal_page_image_child': ('filebrowser.fields.FileBrowseField', [], {'max_length': '254', 'blank': 'True'}),
            'internal_page_image_frame': ('filebrowser.fields.FileBrowseField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'place_on_map': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'title_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '128', 'blank': 'True'}),
            'work_time': ('django.db.models.fields.CharField', [], {'default': "'10:00 - 19:00'", 'max_length': '13'})
        },
        u'events.baseevent': {
            'Meta': {'ordering': "['-created_at']", 'object_name': 'BaseEvent'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'baseevents'", 'null': 'True', 'to': u"orm['shops.Brand']"}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 9, 22, 0, 0)', 'auto_now_add': 'True', 'db_index': 'True', 'blank': 'True'}),
            'description_en': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'description_ru': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True'}),
            'end_date': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2014, 9, 22, 0, 0)', 'null': 'True', 'blank': 'True'}),
            'entertainment': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'baseevents'", 'null': 'True', 'to': u"orm['entertainments.Entertainment']"}),
            'for_children': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_en': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'image_ru': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'is_entertainment': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'pub_date': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2014, 9, 22, 0, 0)', 'db_index': 'True'}),
            'restaurant': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'baseevents'", 'null': 'True', 'to': u"orm['shops.Restaurant']"}),
            'short_description_en': ('ckeditor.fields.RichTextField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'short_description_ru': ('ckeditor.fields.RichTextField', [], {'max_length': '100', 'null': 'True'}),
            'start_date': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2014, 9, 22, 0, 0)', 'null': 'True', 'blank': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'})
        },
        u'events.springevent': {
            'Meta': {'ordering': "['-created_at']", 'object_name': 'SpringEvent', '_ormbases': [u'events.BaseEvent']},
            'background': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            u'baseevent_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['events.BaseEvent']", 'unique': 'True', 'primary_key': 'True'}),
            'color': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['shops.Color']", 'null': 'True', 'blank': 'True'}),
            'every_day_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'every_day_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'is_cycled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_special': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'shops.brand': {
            'Meta': {'ordering': "['place_number']", 'object_name': 'Brand'},
            'categories': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'brands'", 'symmetrical': 'False', 'to': u"orm['shops.BrandCategory']"}),
            'color': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'brands'", 'null': 'True', 'to': u"orm['shops.Color']"}),
            'contacts': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'description_en': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'description_ru': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True'}),
            'filter_apply': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo_en': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'logo_ru': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'number': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '256'}),
            'open_soon': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'place_number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1000', 'db_index': 'True'}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'show_on_main': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'url': ('spring.shops.fields.NullableURLField', [], {'default': 'None', 'max_length': '128', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'views_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'work_time': ('django.db.models.fields.CharField', [], {'default': "'10:00 - 19:00'", 'max_length': '13'})
        },
        u'shops.brandcategory': {
            'Meta': {'ordering': "('position',)", 'object_name': 'BrandCategory'},
            'banner': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'banner_url': ('django.db.models.fields.URLField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'for_children': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iphone_banner': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'}),
            'position': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        },
        u'shops.color': {
            'Meta': {'object_name': 'Color'},
            'circle_color': ('colorful.fields.RGBColorField', [], {'max_length': '7'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'promotion_color': ('colorful.fields.RGBColorField', [], {'max_length': '7'})
        },
        u'shops.restaurant': {
            'Meta': {'ordering': "['place_number']", 'object_name': 'Restaurant'},
            'categories': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'restaurants'", 'symmetrical': 'False', 'to': u"orm['shops.RestaurantCategory']"}),
            'color': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'restaurants'", 'null': 'True', 'to': u"orm['shops.Color']"}),
            'contacts': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'description_en': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'description_ru': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True'}),
            'filter_apply': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo_en': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'logo_ru': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'number': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '256'}),
            'open_soon': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'place_number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1000', 'db_index': 'True'}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'show_on_main': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'url': ('spring.shops.fields.NullableURLField', [], {'default': 'None', 'max_length': '128', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'views_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'work_time': ('django.db.models.fields.CharField', [], {'default': "'10:00 - 19:00'", 'max_length': '13'})
        },
        u'shops.restaurantcategory': {
            'Meta': {'ordering': "('position',)", 'object_name': 'RestaurantCategory'},
            'banner': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'banner_url': ('django.db.models.fields.URLField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iphone_banner': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'}),
            'position': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        },
        u'subscription.eventtomessage': {
            'Meta': {'object_name': 'EventToMessage'},
            'event': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['events.BaseEvent']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'other_events'", 'to': u"orm['subscription.MessageItem']"})
        },
        u'subscription.messageitem': {
            'Meta': {'object_name': 'MessageItem'},
            'accost': ('django.db.models.fields.CharField', [], {'default': "u'\\u0417\\u0434\\u0440\\u0430\\u0432\\u0441\\u0442\\u0432\\u0443\\u0439\\u0442\\u0435'", 'max_length': '255'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'for_male': ('django.db.models.fields.NullBooleanField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'subscribers': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['subscription.Subscriber']", 'symmetrical': 'False'})
        },
        u'subscription.messagequeue': {
            'Meta': {'unique_together': "(('subscriber', 'message'),)", 'object_name': 'MessageQueue'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['subscription.MessageItem']"}),
            'read': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'read_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'sent': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'sent_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'subscriber': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['subscription.Subscriber']"})
        },
        u'subscription.springeventtomessage': {
            'Meta': {'object_name': 'SpringEventToMessage'},
            'category': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'event': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['events.SpringEvent']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'spring_events'", 'to': u"orm['subscription.MessageItem']"})
        },
        u'subscription.subscriber': {
            'Meta': {'object_name': 'Subscriber'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'birthday': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'city': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '255'}),
            'first_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_agree': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_male': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '20', 'blank': 'True'}),
            'unsubscribed_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'subscription.subscribersms': {
            'Meta': {'object_name': 'SubscriberSMS'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'default': "''", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_agree': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_male': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '20', 'db_index': 'True'}),
            'unsubscribed_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'subscription.subscriptionexample': {
            'Meta': {'object_name': 'SubscriptionExample'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('ckeditor.fields.RichTextField', [], {'max_length': '1000'}),
            'title': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'})
        },
        u'subscription.subscriptionlog': {
            'Meta': {'object_name': 'SubscriptionLog'},
            'accost': ('django.db.models.fields.CharField', [], {'default': "u'\\u0417\\u0434\\u0440\\u0430\\u0432\\u0441\\u0442\\u0432\\u0443\\u0439\\u0442\\u0435, '", 'max_length': '255'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'entertainment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['entertainments.Entertainment']", 'null': 'True', 'blank': 'True'}),
            'for_male': ('django.db.models.fields.NullBooleanField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('ckeditor.fields.RichTextField', [], {'max_length': '1000'}),
            'movie': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cinema.Movie']", 'null': 'True', 'blank': 'True'}),
            'sub_type': ('django.db.models.fields.CharField', [], {'default': "'general'", 'max_length': '255'}),
            'subject': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'subscribers': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'subscription_logs'", 'symmetrical': 'False', 'to': u"orm['subscription.Subscriber']"}),
            'total_users': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'subscription.subscriptionmessage': {
            'Meta': {'object_name': 'SubscriptionMessage'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_readed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'subscriber': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'subscription_messages'", 'to': u"orm['subscription.Subscriber']"}),
            'token': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '64'})
        },
        u'subscription.subscriptionsmslog': {
            'Meta': {'object_name': 'SubscriptionSMSLog'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'for_male': ('django.db.models.fields.NullBooleanField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            'send_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 9, 22, 0, 0)'}),
            'subscribers': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'subscription_sms_logs'", 'symmetrical': 'False', 'to': u"orm['subscription.SubscriberSMS']"}),
            'total_users': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'subscription.subscriptionsmsmessage': {
            'Meta': {'object_name': 'SubscriptionSMSMessage'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'sms_id': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '50', 'db_index': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '1000'}),
            'status_code': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0', 'db_index': 'True'}),
            'subscriber': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'subscription_sms_messages'", 'to': u"orm['subscription.SubscriberSMS']"})
        },
        u'subscription.verifyingcode': {
            'Meta': {'object_name': 'VerifyingCode'},
            'code': ('django.db.models.fields.PositiveIntegerField', [], {'default': '123432'}),
            'email': ('django.db.models.fields.EmailField', [], {'default': "''", 'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '20'})
        }
    }

    complete_apps = ['subscription']