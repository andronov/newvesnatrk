# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'Unsubscribed'
        db.delete_table(u'subscription_unsubscribed')

        # Adding field 'Subscriber.active'
        db.add_column(u'subscription_subscriber', 'active',
                      self.gf('django.db.models.fields.BooleanField')(default=True),
                      keep_default=False)

        # Adding field 'Subscriber.unsubscribed_at'
        db.add_column(u'subscription_subscriber', 'unsubscribed_at',
                      self.gf('django.db.models.fields.DateField')(null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Adding model 'Unsubscribed'
        db.create_table(u'subscription_unsubscribed', (
            ('subscriber', self.gf('django.db.models.fields.related.ForeignKey')(related_name='unsubscribed', to=orm['subscription.Subscriber'])),
            ('unsubscribed_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'subscription', ['Unsubscribed'])

        # Deleting field 'Subscriber.active'
        db.delete_column(u'subscription_subscriber', 'active')

        # Deleting field 'Subscriber.unsubscribed_at'
        db.delete_column(u'subscription_subscriber', 'unsubscribed_at')


    models = {
        u'subscription.subscriber': {
            'Meta': {'object_name': 'Subscriber'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'birthday': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'city': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '255'}),
            'first_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_agree': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_male': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'unsubscribed_at': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'})
        },
        u'subscription.subscriptionexample': {
            'Meta': {'object_name': 'SubscriptionExample'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('ckeditor.fields.RichTextField', [], {'max_length': '1000'}),
            'title': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'})
        },
        u'subscription.subscriptionlog': {
            'Meta': {'object_name': 'SubscriptionLog'},
            'age_from': ('django.db.models.fields.PositiveSmallIntegerField', [], {'max_length': '2', 'null': 'True', 'blank': 'True'}),
            'age_to': ('django.db.models.fields.PositiveSmallIntegerField', [], {'max_length': '2', 'null': 'True', 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'for_male': ('django.db.models.fields.NullBooleanField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('ckeditor.fields.RichTextField', [], {'max_length': '1000'}),
            'subject': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'subscribers': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'subscription_logs'", 'symmetrical': 'False', 'to': u"orm['subscription.Subscriber']"}),
            'total_users': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'subscription.subscriptionmessage': {
            'Meta': {'object_name': 'SubscriptionMessage'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_readed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'subscriber': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'subscription_messages'", 'to': u"orm['subscription.Subscriber']"}),
            'token': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '64'})
        }
    }

    complete_apps = ['subscription']