# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Subscriber.email'
        db.alter_column(u'subscription_subscriber', 'email', self.gf('django.db.models.fields.EmailField')(max_length=255, null=True))

    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'Subscriber.email'
        raise RuntimeError("Cannot reverse this migration. 'Subscriber.email' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration
        # Changing field 'Subscriber.email'
        db.alter_column(u'subscription_subscriber', 'email', self.gf('django.db.models.fields.EmailField')(max_length=255))

    models = {
        u'entertainments.entertainment': {
            'Meta': {'object_name': 'Entertainment'},
            'contacts': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'description_en': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '2000', 'blank': 'True'}),
            'description_ru': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '2000'}),
            'description_short_en': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '500', 'blank': 'True'}),
            'description_short_ru': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '500'}),
            'description_title_en': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '200', 'blank': 'True'}),
            'description_title_ru': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '200'}),
            'for_children': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'internal_page_image': ('filebrowser.fields.FileBrowseField', [], {'max_length': '254', 'blank': 'True'}),
            'internal_page_image_child': ('filebrowser.fields.FileBrowseField', [], {'max_length': '254', 'blank': 'True'}),
            'internal_page_image_frame': ('filebrowser.fields.FileBrowseField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'place_on_map': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'title_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '128', 'blank': 'True'}),
            'work_time': ('django.db.models.fields.CharField', [], {'default': "'10:00 - 19:00'", 'max_length': '13'})
        },
        u'events.baseevent': {
            'Meta': {'ordering': "['-created_at']", 'object_name': 'BaseEvent'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'baseevents'", 'null': 'True', 'to': u"orm['shops.Brand']"}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 10, 2, 0, 0)', 'auto_now_add': 'True', 'db_index': 'True', 'blank': 'True'}),
            'description_en': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'description_ru': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True'}),
            'end_date': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2014, 10, 2, 0, 0)', 'null': 'True', 'blank': 'True'}),
            'entertainment': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'baseevents'", 'null': 'True', 'to': u"orm['entertainments.Entertainment']"}),
            'for_children': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_en': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'image_ru': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'is_entertainment': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'pub_date': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2014, 10, 2, 0, 0)', 'db_index': 'True'}),
            'restaurant': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'baseevents'", 'null': 'True', 'to': u"orm['shops.Restaurant']"}),
            'short_description_en': ('ckeditor.fields.RichTextField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'short_description_ru': ('ckeditor.fields.RichTextField', [], {'max_length': '100', 'null': 'True'}),
            'start_date': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2014, 10, 2, 0, 0)', 'null': 'True', 'blank': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'})
        },
        u'events.springevent': {
            'Meta': {'ordering': "['-created_at']", 'object_name': 'SpringEvent', '_ormbases': [u'events.BaseEvent']},
            'background': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            u'baseevent_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['events.BaseEvent']", 'unique': 'True', 'primary_key': 'True'}),
            'color': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['shops.Color']", 'null': 'True', 'blank': 'True'}),
            'every_day_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'every_day_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'is_cycled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_special': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'shops.brand': {
            'Meta': {'ordering': "['place_number']", 'object_name': 'Brand'},
            'categories': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'brands'", 'symmetrical': 'False', 'to': u"orm['shops.BrandCategory']"}),
            'color': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'brands'", 'null': 'True', 'to': u"orm['shops.Color']"}),
            'contacts': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'description_en': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'description_ru': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True'}),
            'filter_apply': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo_en': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'logo_ru': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'number': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '256'}),
            'open_soon': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'place_number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1000', 'db_index': 'True'}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'show_on_main': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'url': ('spring.shops.fields.NullableURLField', [], {'default': 'None', 'max_length': '128', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'views_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'work_time': ('django.db.models.fields.CharField', [], {'default': "'10:00 - 19:00'", 'max_length': '13'})
        },
        u'shops.brandcategory': {
            'Meta': {'ordering': "('position',)", 'object_name': 'BrandCategory'},
            'banner': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'banner_url': ('django.db.models.fields.URLField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'for_children': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iphone_banner': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'}),
            'position': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        },
        u'shops.color': {
            'Meta': {'object_name': 'Color'},
            'circle_color': ('colorful.fields.RGBColorField', [], {'max_length': '7'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'promotion_color': ('colorful.fields.RGBColorField', [], {'max_length': '7'})
        },
        u'shops.restaurant': {
            'Meta': {'ordering': "['place_number']", 'object_name': 'Restaurant'},
            'categories': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'restaurants'", 'symmetrical': 'False', 'to': u"orm['shops.RestaurantCategory']"}),
            'color': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'restaurants'", 'null': 'True', 'to': u"orm['shops.Color']"}),
            'contacts': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'description_en': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'description_ru': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True'}),
            'filter_apply': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo_en': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'logo_ru': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'number': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '256'}),
            'open_soon': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'place_number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1000', 'db_index': 'True'}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'show_on_main': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'url': ('spring.shops.fields.NullableURLField', [], {'default': 'None', 'max_length': '128', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'views_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'work_time': ('django.db.models.fields.CharField', [], {'default': "'10:00 - 19:00'", 'max_length': '13'})
        },
        u'shops.restaurantcategory': {
            'Meta': {'ordering': "('position',)", 'object_name': 'RestaurantCategory'},
            'banner': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'banner_url': ('django.db.models.fields.URLField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iphone_banner': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'}),
            'position': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        },
        u'subscription.eventtomessage': {
            'Meta': {'object_name': 'EventToMessage'},
            'event': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['events.BaseEvent']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'other_events'", 'to': u"orm['subscription.MessageItem']"})
        },
        u'subscription.messageitem': {
            'Meta': {'object_name': 'MessageItem'},
            'accost': ('django.db.models.fields.CharField', [], {'default': "u'\\u0417\\u0434\\u0440\\u0430\\u0432\\u0441\\u0442\\u0432\\u0443\\u0439\\u0442\\u0435'", 'max_length': '255'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'for_male': ('django.db.models.fields.NullBooleanField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'subscribers': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['subscription.Subscriber']", 'symmetrical': 'False'})
        },
        u'subscription.messagequeue': {
            'Meta': {'unique_together': "(('subscriber', 'message'),)", 'object_name': 'MessageQueue'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['subscription.MessageItem']"}),
            'read': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'read_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'sent': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'sent_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'subscriber': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['subscription.Subscriber']"})
        },
        u'subscription.springeventtomessage': {
            'Meta': {'object_name': 'SpringEventToMessage'},
            'category': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'event': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['events.SpringEvent']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'spring_events'", 'to': u"orm['subscription.MessageItem']"})
        },
        u'subscription.subscriber': {
            'Meta': {'object_name': 'Subscriber'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'birthday': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_agree': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_male': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'sms_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'sms_unsubscribed_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'unsubscribed_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'subscription.subscriptionsmslog': {
            'Meta': {'object_name': 'SubscriptionSMSLog'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'for_male': ('django.db.models.fields.NullBooleanField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            'send_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 10, 2, 0, 0)'}),
            'subscribers': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'subscription_sms_logs'", 'symmetrical': 'False', 'to': u"orm['subscription.Subscriber']"}),
            'total_users': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'subscription.subscriptionsmsmessage': {
            'Meta': {'object_name': 'SubscriptionSMSMessage'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'sms_id': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '50', 'db_index': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '1000'}),
            'status_code': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0', 'db_index': 'True'}),
            'subscriber': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'subscription_sms_messages'", 'to': u"orm['subscription.Subscriber']"})
        },
        u'subscription.verifyingcode': {
            'Meta': {'object_name': 'VerifyingCode'},
            'code': ('django.db.models.fields.PositiveIntegerField', [], {'default': '123432'}),
            'email': ('django.db.models.fields.EmailField', [], {'default': "''", 'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '20'})
        }
    }

    complete_apps = ['subscription']