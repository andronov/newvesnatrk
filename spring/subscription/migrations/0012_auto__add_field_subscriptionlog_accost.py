# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'SubscriptionLog.accost'
        db.add_column(u'subscription_subscriptionlog', 'accost',
                      self.gf('django.db.models.fields.CharField')(default=u'\u0417\u0434\u0440\u0430\u0432\u0441\u0442\u0432\u0443\u0439\u0442\u0435, ', max_length=255),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'SubscriptionLog.accost'
        db.delete_column(u'subscription_subscriptionlog', 'accost')


    models = {
        u'cinema.movie': {
            'Meta': {'object_name': 'Movie'},
            'actors_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'actors_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'age_restriction': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'circle_image': ('filebrowser.fields.FileBrowseField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'countries_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'countries_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'directors_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'directors_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'duration': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'genre_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'genre_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_premiere': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'kinohod_id': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'long_desc_en': ('ckeditor.fields.RichTextField', [], {'max_length': '2000', 'null': 'True', 'blank': 'True'}),
            'long_desc_ru': ('ckeditor.fields.RichTextField', [], {'max_length': '2000', 'null': 'True'}),
            'poster_en': ('filebrowser.fields.FileBrowseField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'poster_ru': ('filebrowser.fields.FileBrowseField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'producers_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'producers_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'rating': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'scenario_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'scenario_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'short_desc_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'short_desc_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'super_poster_en': ('filebrowser.fields.FileBrowseField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'super_poster_ru': ('filebrowser.fields.FileBrowseField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'})
        },
        u'entertainments.entertainment': {
            'Meta': {'object_name': 'Entertainment'},
            'contacts': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'description_en': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '2000', 'blank': 'True'}),
            'description_ru': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '2000'}),
            'description_short_en': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '500', 'blank': 'True'}),
            'description_short_ru': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '500'}),
            'description_title_en': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '200', 'blank': 'True'}),
            'description_title_ru': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '200'}),
            'for_children': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'internal_page_image': ('filebrowser.fields.FileBrowseField', [], {'max_length': '254', 'blank': 'True'}),
            'internal_page_image_child': ('filebrowser.fields.FileBrowseField', [], {'max_length': '254', 'blank': 'True'}),
            'place_on_map': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'title_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '128', 'blank': 'True'}),
            'work_time': ('django.db.models.fields.CharField', [], {'default': "'10:00 - 19:00'", 'max_length': '13'})
        },
        u'subscription.subscriber': {
            'Meta': {'object_name': 'Subscriber'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'birthday': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'city': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '255'}),
            'first_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_agree': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_male': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'unsubscribed_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'subscription.subscriptionexample': {
            'Meta': {'object_name': 'SubscriptionExample'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('ckeditor.fields.RichTextField', [], {'max_length': '1000'}),
            'title': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'})
        },
        u'subscription.subscriptionlog': {
            'Meta': {'object_name': 'SubscriptionLog'},
            'accost': ('django.db.models.fields.CharField', [], {'default': "u'\\u0417\\u0434\\u0440\\u0430\\u0432\\u0441\\u0442\\u0432\\u0443\\u0439\\u0442\\u0435, '", 'max_length': '255'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'entertainment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['entertainments.Entertainment']", 'null': 'True', 'blank': 'True'}),
            'for_male': ('django.db.models.fields.NullBooleanField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('ckeditor.fields.RichTextField', [], {'max_length': '1000'}),
            'movie': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cinema.Movie']", 'null': 'True', 'blank': 'True'}),
            'sub_type': ('django.db.models.fields.CharField', [], {'default': "'general'", 'max_length': '255'}),
            'subject': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'subscribers': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'subscription_logs'", 'symmetrical': 'False', 'to': u"orm['subscription.Subscriber']"}),
            'total_users': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'subscription.subscriptionmessage': {
            'Meta': {'object_name': 'SubscriptionMessage'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_readed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'subscriber': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'subscription_messages'", 'to': u"orm['subscription.Subscriber']"}),
            'token': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '64'})
        }
    }

    complete_apps = ['subscription']