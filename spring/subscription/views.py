# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import datetime
import random
from hashlib import sha1
from PIL import Image
from django.db.models import Q

from django.utils.translation import get_language
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from django.views.generic import View, RedirectView
from django.http import Http404
from django.template.loader import render_to_string
from django.core.mail import EmailMessage
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST, is_success
from rest_framework.views import APIView
from spring.subscription.helpers import SMS_STATUSES, SMS
from spring.subscription.models import VerifyingCode, SubscriptionSMSMessage, MessageQueue
from spring.subscription.serializers import SubscriberSerializer, SubscriberSmsSerializer, SMSSerializer
from spring.about.models import MessageTemplate
from spring.subscription.models import Subscriber
from utilities.renderers import AngularJSONPRenderer


class UnsubscribeView(RedirectView):
    sms = False
    permanent = False

    def __init__(self, **kwargs):
        super(UnsubscribeView, self).__init__(**kwargs)
        self.url = None

    def dispatch(self, request, *args, **kwargs):
        self.unsubscribe(kwargs.get('hash', None), kwargs.get('subscriber', None))
        self.url = '/unsubscribe/{}/{}/'.format(
            kwargs.get('subscriber'),
            kwargs.get('hash'),
        )
        return super(UnsubscribeView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        response = super(UnsubscribeView, self).get(request, *args, **kwargs)
        if self.sms:
            response.delete_cookie('is_subscribed')
            response.set_cookie('is_created', Subscriber.objects.get(pk=kwargs.get('subscriber')).phone)
        return response

    def unsubscribe(self, user_hash, user_id):
        try:
            subscriber = Subscriber.objects.get(pk=user_id)
            if user_hash != sha1(subscriber.phone if self.sms else subscriber.email).hexdigest():
                raise Http404
        except Subscriber.DoesNotExist:
            raise Http404
        if self.sms:
            subscriber.sms_active = False
        else:
            subscriber.active = False
        subscriber.save()
unsubscribe = UnsubscribeView.as_view(sms=False)
unsubscribe_sms = UnsubscribeView.as_view(sms=True)


class ReadView(View):
    def dispatch(self, request, *args, **kwargs):
        self.read(self.kwargs.get('message', None), self.kwargs.get('subscriber', None))
        return super(ReadView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        img = Image.new('RGBA', (1, 1), (255, 0, 0, 0))
        response = HttpResponse(mimetype="image/jpeg")
        img.save(response, "JPEG")
        return response

    def read(self, message_id, subscriber_id):
        try:
            queue = MessageQueue.objects.get(
                subscriber_id=subscriber_id,
                message_id=message_id,
                read=False
            )
            queue.read = True
            queue.read_at = datetime.datetime.now()
            queue.save()
        except MessageQueue.DoesNotExist:
            pass
read = ReadView.as_view()


class SubscriberCreate(CreateAPIView):
    sms = False

    def __init__(self, *args, **kwargs):
        super(SubscriberCreate, self).__init__(*args, **kwargs)
        self.serializer_class = SubscriberSmsSerializer if self.sms else SubscriberSerializer

    def create(self, request, *args, **kwargs):
        response = super(SubscriberCreate, self).create(request, *args, **kwargs)
        response.data.update({
            'form_accepted': True if is_success(response.status_code) else False
        })
        if self.sms and is_success(response.status_code):
            response.set_cookie('is_created', self.object.phone)
            response.set_cookie('is_subscribed', False)
        return response

    def pre_save(self, obj):
        if obj.phone.startswith('7') and len(obj.phone) == 11:
            obj.phone = obj.phone[1:]
            
        try:
            if (self.sms and obj.email != '') or (not self.sms and obj.phone != ''):
                try:
                    old_subscriber = Subscriber.objects.get(Q(phone=obj.phone) | Q(email=obj.email))
                except Subscriber.MultipleObjectsReturned:
                    old_subscribers = Subscriber.objects.filter(
                        Q(phone=obj.phone) | Q(email=obj.email)
                    ).order_by('-created_at')
                    old_subscriber = old_subscribers[0]
                    old_subscribers.exclude(id=old_subscriber.pk).delete()
            elif self.sms and obj.email == '':
                old_subscriber = Subscriber.objects.get(phone=obj.phone)
            else:
                old_subscriber = Subscriber.objects.get(email=obj.email)

            for field in obj._meta.fields:
                if getattr(obj, field.name) is None or getattr(obj, field.name) == '':
                    setattr(obj, field.name, getattr(old_subscriber, field.name))
        except Subscriber.DoesNotExist:
            pass

    def post_save_email(self, obj):
        message_template = MessageTemplate.objects.get(name='after_subscribe')

        unsubscribe_url = 'http://{}/unsubscribe/{}/{}/'.format(
            self.request.META['SERVER_NAME'],
            obj.pk,
            sha1(obj.email).hexdigest()
        )

        email = EmailMessage(
            subject=message_template.subject,
            body=render_to_string('general.html', {
                'accost': message_template.accost,
                'first_name': obj.first_name,
                'description': message_template.header,
                'unsubscribe_url': unsubscribe_url,
                'server_name': 'http://{}'.format(self.request.META['SERVER_NAME'])
            }),
            to=[obj.email]
        )
        email.content_subtype = 'html'
        email.send(True)

    def post_save_phone(self, obj):
        VerifyingCode.objects.filter(phone=obj.phone).delete()
        vc = VerifyingCode()
        vc.code = random.randint(100, 999)
        vc.phone = obj.phone
        vc.save()
        SMS().send_sms(data={'subscribers': [obj], 'text': u'Код подтверждения: %s' % vc.code})

    def post_save(self, obj, created=False):
        if self.sms:
            self.post_save_phone(obj)
        else:
            self.post_save_email(obj)
subscriber_create = SubscriberCreate.as_view(sms=False)
subscriber_sms_create = SubscriberCreate.as_view(sms=True)


class SubscribeAgain(APIView):
    serializer_class = SubscriberSerializer

    def get(self, request, pk, token):
        subscriber = Subscriber.objects.get(pk=pk)

        if token != sha1(subscriber.email).hexdigest():
            return Response('Subscriber token != url token', status=HTTP_400_BAD_REQUEST)

        subscriber.active = True
        subscriber.save()

        serializer = self.serializer_class(subscriber)

        return Response({
            'object': serializer.data
        })
subscribe_again = SubscribeAgain.as_view()


class WiFiView(APIView):
    renderer_classes = (AngularJSONPRenderer,)

    def __init__(self, *args, **kwargs):
        super(WiFiView, self).__init__(*args, **kwargs)
        self.in_vk = False
        self.in_fb = False
        self.vk_is_auth = False
        self.fb_is_auth = False

    def get(self, request):
        #if request.user.is_authenticated() and not request.user.is_staff:
            #self.vk_is_auth = Vkontakte(request.user).is_auth()
            #self.fb_is_auth = Facebook(request.user).is_auth()
            #self.in_vk = Vkontakte(request.user).user_in_group()
            #self.in_fb = Facebook(request.user).user_in_group()

        return Response({
            'is_subscribed': self.request.COOKIES.get('is_subscribed', None) in ['True', 'true', '1'],
            'is_created': 'is_created' in self.request.COOKIES,
            #'in_vk': self.in_vk,
            #'in_fb': self.in_fb,
            #'fb_is_auth': self.fb_is_auth,
            #'vk_is_auth': self.vk_is_auth,
            #'username': self.request.user.username or None
        })
wifi = WiFiView.as_view()


class ConfirmSMSCode(CreateAPIView):
    serializer_class = SMSSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.DATA, files=request.FILES)

        if serializer.is_valid():
            try:
                VerifyingCode.objects.get(
                    phone=serializer.object.phone,
                    code=serializer.object.code
                )
                subscriber = Subscriber.objects.get(phone=serializer.object.phone)
                subscriber.sms_active = True
                subscriber.save()

                response = Response({
                    'object': serializer.data
                })
                response.set_cookie('is_created', serializer.object.phone)
                response.set_cookie('is_subscribed', True)
                return response
            except (VerifyingCode.DoesNotExist, Subscriber.DoesNotExist):
                return Response('Subscriber with this credentials does not exist', status=HTTP_400_BAD_REQUEST)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)
subscriber_sms_confirm = ConfirmSMSCode.as_view()


class ResendSMSCode(APIView):
    def get(self, request):
        try:
            obj = Subscriber.objects.get(phone=request.COOKIES.get('is_created'))

            VerifyingCode.objects.filter(phone=obj.phone).delete()
            vc = VerifyingCode()
            vc.code = random.randint(100, 999)
            vc.phone = obj.phone
            vc.save()
            SMS().send_sms(data={'subscribers': [obj], 'text': u'Код подтверждения: %s' % vc.code})
            return Response({
                'object': 'Ok'
            })
        except (ValueError, KeyError, AttributeError):
            return Response('Bad phone', status=HTTP_400_BAD_REQUEST)
        except Subscriber.DoesNotExist:
            return Response('No subscriber with this phone', status=HTTP_400_BAD_REQUEST)
subscriber_sms_resend = ResendSMSCode.as_view()


class UpdateSMSStatus(View):
    http_method_names = ['post']

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        import logging
        logger = logging.getLogger()
        logger.debug('debug post')
        return super(UpdateSMSStatus, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        from spring.querystring_parser import parser





        datas = parser.parse(request.POST.urlencode()).get('data')

        update = {}

        for data in datas.iteritems():
            status = data[1].split("\n")
            if status[0] == 'sms_status':
                update[status[1]] = status[2]

        for sms_id, status_code in update.items():
            status_code = int(status_code)
            SubscriptionSMSMessage.objects.filter(sms_id=sms_id).update(status_code=status_code, status=SMS_STATUSES.get(status_code))
        return HttpResponse(content="100")
update_sms_status = UpdateSMSStatus.as_view()

