# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from hashlib import sha1
import datetime

from django.core.mail import EmailMessage
from django.core.management.base import BaseCommand
from django.template.loader import render_to_string
from django.conf import settings
from django.utils import translation
from django.core.urlresolvers import reverse

from spring.subscription.models import MessageQueue
from spring.subscription.models import EVENT_CATEGORY_CHOICES


class Command(BaseCommand):
    def handle(self, *args, **options):
        translation.activate('ru')
        count_per_iteration = 1000
        unsent = MessageQueue.objects.filter(sent=False)[:count_per_iteration]

        if settings.CONFIG.SETTINGS['ENV'] == 'prod':
            server_name = 'http://vesna-trk.ru'
        else:
            server_name = 'http://dev.spring.dev.ailove.ru'

        print 'SCRIPT START {}. Unsent messages count: {}'.format(
            datetime.datetime.now(),
            unsent.count()
        )
        for item in unsent:
            try:
                try:
                    favorite_weekend = item.message.spring_events.filter(
                        category=EVENT_CATEGORY_CHOICES[0][0]
                    )[0]
                except Exception:
                    favorite_weekend = None

                try:
                    for_children = item.message.spring_events.filter(
                        category=EVENT_CATEGORY_CHOICES[1][0]
                    )[0]
                except Exception:
                    for_children = None

                try:
                    special = item.message.spring_events.filter(
                        category=EVENT_CATEGORY_CHOICES[2][0]
                    )[0]
                except Exception:
                    special = None

                email_message = EmailMessage(
                    subject=item.message.subject,
                    body=render_to_string(
                        'email.html', {
                            'SERVER_NAME': server_name,
                            'accost': item.message.accost,
                            'first_name': item.subscriber.first_name,
                            'favorite_weekend': favorite_weekend,
                            'for_children': for_children,
                            'special': special,
                            'actions': item.message.other_events.all(),
                            'unsubscribe_url': '{}{}'.format(
                                server_name,
                                reverse('unsubscribe', args=[
                                    item.subscriber.pk,
                                    sha1(item.subscriber.email).hexdigest()
                                ])
                            ),
                            'read_url': '{}{}'.format(
                                server_name,
                                reverse('read', args=[
                                    item.subscriber.pk,
                                    item.message.pk
                                ])
                            )
                        }
                    ),
                    from_email=settings.DEFAULT_FROM_EMAIL,
                    to=[item.subscriber.email]
                )
                email_message.content_subtype = 'html'
                email_message.send()
                item.sent = True
                item.sent_at = datetime.datetime.now()
                item.save()
                print 'Message ({}) for subscriber ({}) SENT.'.format(
                    item.message.pk,
                    item.subscriber.pk
                )
            except Exception as e:
                print 'Message ({}) for subscriber ({}) NOT SENT. {}'.format(
                    item.message.pk,
                    item.subscriber.pk,
                    e
                )
        translation.deactivate()

