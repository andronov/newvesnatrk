# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import datetime
import re
import time
from django.utils import timezone
import requests

from django.core.management.base import BaseCommand
from django.conf import settings
from spring.subscription.helpers import SMS_STATUSES

from spring.subscription.models import SmsMessageQueue, SmsMessageItem


class Command(BaseCommand):
    def handle(self, *args, **options):
        SUCCESS_STATUSES = [100, 101, 102, 103]
        ERROR_NO_RETRY_STATUSES = [104, 105, 106, 107, 108, 202, 203, 205, 207, 208]
        count_per_iteration = 100

        try:
            message_id = SmsMessageQueue.objects.filter(sent=False).values_list('message')[0][0]
            message = SmsMessageItem.objects.get(pk=message_id)
        except IndexError:
            print 'No unsent sms'
            return


        unsent = SmsMessageQueue.objects.filter(
            sent=False,
            message_id=message.pk
        ).exclude(subscriber__phone=u'').order_by('?')[:count_per_iteration]

        print 'SCRIPT START {}. Unsent sms messages count: {}'.format(
            datetime.datetime.now(),
            len(unsent)
        )

        try:
            dt = message.to_send_at if message.to_send_at is not None else timezone.now()
            data = {
                'time': time.mktime(dt.timetuple())
            }

            for item in unsent:
                if (
                    re.match('^9\d{9}$', item.subscriber.phone) is not None
                ) or (
                    re.match('^89\d{9}$', item.subscriber.phone) is not None
                ):
                    data['multi[{}]'.format(item.subscriber.phone)] = message.message

            url = 'http://sms.ru/sms/send/?api_id={}'.format(settings.SMS_API_KEY)
            response = requests.post(url, data)

            if response.status_code == 200:
                status = response.text
            else:
                status = response
            status = status.split('\n')
            status_code = int(status[0])

            if status_code in ERROR_NO_RETRY_STATUSES:
                print 'SUCCESS ITERATION. BAD STATUS: {}.'.format(
                    status_code
                )
            elif status_code not in SUCCESS_STATUSES and message.retry:
                print 'SUCCESS ITERATION. BAD STATUS: {}. I\'LL TRY LATER'.format(
                    status_code
                )
                return
            else:
                print 'SUCCESS ITERATION. SENT COUNT: {}'.format(
                    unsent.count()
                )

            for i, item in enumerate(unsent):
                item.sent = True
                item.sent_at = datetime.datetime.now()
                item.status_code = status_code
                item.status = SMS_STATUSES[item.status_code]
                if item.status_code == 100:
                    item.sms_id = status[i + 1]
                item.save()
        except Exception as e:
            print 'ERROR. {}'.format(e)
