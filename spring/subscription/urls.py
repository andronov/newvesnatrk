from django.conf.urls import patterns, url

urlpatterns = patterns(
    'spring.subscription.views',
    url(r'^unsubscribe/(?P<subscriber>\d+)/(?P<hash>[a-f0-9]{40})/$', 'unsubscribe', name='unsubscribe'),
    url(r'^read/(?P<subscriber>\d+)/(?P<message>\d+)/$', 'read', name='read'),
    url(r'^subscribe/$', 'subscriber_create'),
    url(r'^subscribe_again/(?P<pk>\d+)/(?P<token>[0-9a-fA-F]+)/$', 'subscribe_again'),
    # SMS
    url(r'^subscribe/sms/create/$', 'subscriber_sms_create'),
    url(r'^subscribe/sms/resend/$', 'subscriber_sms_resend'),
    url(r'^subscribe/sms/confirm/$', 'subscriber_sms_confirm'),
    url(r'^unsubscribe/sms/(?P<subscriber>\d+)/(?P<hash>[a-f0-9]{40})/$', 'unsubscribe_sms'),
)
