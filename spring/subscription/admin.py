# -*- coding: utf-8 -*-
from django.contrib import admin
from spring.subscription.forms import EventToMessageForm, MessageItemForm, SmsMessageItemForm
from spring.subscription.models import (
    Subscriber, MessageItem, MessageQueue, SpringEventToMessage, EventToMessage,
    SmsMessageQueue, SmsMessageItem)


class SubscriptionMessageAdmin(admin.ModelAdmin):
    list_display = ('subscriber', 'is_readed')


class SubscriberAdmin(admin.ModelAdmin):
    list_display = ('email', 'phone', 'active', 'sms_active', 'created_at')
    list_filter = ('active', 'sms_active')
    search_fields = ('email', 'phone')


class SubscriptionSMSMessageAdmin(admin.ModelAdmin):

    def has_add_permission(self, request, obj=None):
        return False

    list_display = ('__unicode__', 'status')
    readonly_fields = ('subscriber', 'status')
    exclude = ('sms_id', )


class SpringEventToMessageInline(admin.TabularInline):
    model = SpringEventToMessage

    raw_id_fields = ('event',)

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return self.readonly_fields + ('event', 'category')
        return self.readonly_fields


class EventToMessageInline(admin.TabularInline):
    model = EventToMessage
    form = EventToMessageForm

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return self.readonly_fields + ('event',)
        return self.readonly_fields


class MessageItemAdmin(admin.ModelAdmin):
    filter_horizontal = ('subscribers',)
    form = MessageItemForm
    inlines = (SpringEventToMessageInline, EventToMessageInline)
    
    def __init__(self, model, admin_site):
        super(MessageItemAdmin, self).__init__(model, admin_site)
        self.readonly_fields = ()
        
    def add_view(self, request, form_url='', extra_context=None):
        self.readonly_fields = ()
        return super(MessageItemAdmin, self).add_view(request, form_url, extra_context)

    def change_view(self, request, object_id, form_url='', extra_context=None):
        self.readonly_fields = ('accost', 'subscribers', 'for_male', 'subject', 'created_at')
        return super(MessageItemAdmin, self).change_view(request, object_id, form_url, extra_context)


class SmsMessageItemAdmin(admin.ModelAdmin):
    list_display = ('total_users', 'message', 'for_male', 'created_at')
    filter_horizontal = ('subscribers',)
    form = SmsMessageItemForm

    def __init__(self, model, admin_site):
        super(SmsMessageItemAdmin, self).__init__(model, admin_site)
        self.readonly_fields = ('total_users',)

    def add_view(self, request, form_url='', extra_context=None):
        self.readonly_fields = ('total_users',)
        return super(SmsMessageItemAdmin, self).add_view(request, form_url, extra_context)

    def change_view(self, request, object_id, form_url='', extra_context=None):
        self.readonly_fields = ('subscribers', 'for_male', 'message', 'to_send_at', 'total_users', 'retry')
        return super(SmsMessageItemAdmin, self).change_view(request, object_id, form_url, extra_context)


class SmsMessageQueueAdmin(admin.ModelAdmin):
    list_display = ('subscriber', 'message', 'status', 'status_code', 'sms_id', 'sent', 'sent_at')
    list_filter = ('subscriber', 'sent', 'message__id')
    list_per_page = 50
    actions = None

    def __init__(self, *args, **kwargs):
        super(SmsMessageQueueAdmin, self).__init__(*args, **kwargs)
        self.list_display_links = (None, )

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class MessageQueueAdmin(admin.ModelAdmin):
    list_display = ('subscriber', 'message', 'sent', 'sent_at', 'read', 'read_at')
    list_filter = ('subscriber', 'message', 'sent', 'read')
    list_per_page = 50
    actions = None

    def __init__(self, *args, **kwargs):
        super(MessageQueueAdmin, self).__init__(*args, **kwargs)
        self.list_display_links = (None, )

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

admin.site.register(MessageItem, MessageItemAdmin)
admin.site.register(MessageQueue, MessageQueueAdmin)
admin.site.register(Subscriber, SubscriberAdmin)
admin.site.register(SmsMessageItem, SmsMessageItemAdmin)
admin.site.register(SmsMessageQueue, SmsMessageQueueAdmin)
