# -*- coding: utf-8 -*-
import datetime
from django import forms
from django.db.models import Q
from spring.subscription.helpers import SMS
from spring.subscription.models import EventToMessage, MessageItem, SmsMessageItem


class SmsMessageItemForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(SmsMessageItemForm, self).__init__(*args, **kwargs)
        try:
            self.fields['subscribers'].queryset = self.fields['subscribers'].queryset.exclude(
                sms_active=False
            ).exclude(
                Q(phone__isnull=True) |
                Q(phone='') |
                Q(phone__in=SMS().stop_list())
            ).filter(
                Q(phone__regex=r'^9\d{9}$') |
                Q(phone__regex=r'^89\d{9}$')
            )
        except KeyError:
            pass

    class Meta:
        model = SmsMessageItem


class MessageItemForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(MessageItemForm, self).__init__(*args, **kwargs)
        try:
            self.fields['subscribers'].queryset = self.fields['subscribers'].queryset.exclude(
                active=False
            )
        except KeyError:
            pass

    class Meta:
        model = MessageItem


class EventToMessageForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(EventToMessageForm, self).__init__(*args, **kwargs)
        try:
            self.fields['event'].queryset = self.fields['event'].queryset.filter(
                Q(entertainment__isnull=False) | Q(brand__isnull=False) | Q(restaurant__isnull=False)
            ).exclude(
                is_published=False,
                end_date__lt=datetime.datetime.today()
            )
        except KeyError:
            pass

    class Meta:
        model = EventToMessage
