��    $      <  5   \      0     1     8     F     O  
   T     _     m  L   |  
   �     �     �  	   �  	   �     �                    *  
   2     =     I     [     q     �     �     �     �     �     �     �     �  
   �  
   
  5     -   K  �  y     /     B     Q  
   k     v     �     �  �   �     6  ;   =     y     �     �     �  
   �  !   �     �     	     	     	     3	  #   M	     q	     �	     �	  #   �	     �	     
  
   
  %   !
     G
  )   _
  E   �
  X   �
  L   (                   $              #                   !          	                                                               "             
                                          Accost Active status Birthday City Created at Entertainment Entertainments Fields "Movie" and "Entertainment" must be empty for this subscription type. First name For male users General Is readed Last name Message Movie Must be checked. Sex is male Subject Subscriber Subscribers Subscribtion logs Subscribtion messages Subscription example Subscription examples Subscription log Subscription message Subscrpiption type Title Token Total users Unsubscribed date User agree User email You must select "Entertainment" as subscription type. You must select "Movie" as subscription type. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-05-23 13:49+0400
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Обращение Активен Дата рождения Город Создано Развлечение Развлечения Поля "Развлечение" и "Фильм" должны быть пустыми для этого типа подписки Имя Для пользователей мужского пола Основной Прочитано Фамилия Сообщение Фильм Обязательное поле Мужской пол Тема Подписчик Подписчики Логи подписки Сообщения подписки Пример подписки Примеры подписки Лог подписки Сообщение подписки Тип подписки Заголовок Токен Всего пользователей Дата отписки Согласие пользователя Адрес электронной почты пользователя Вы должны выбрать "Развлечение" как тип подписки Вы должны выбрать "Фильм" как тип подписки 