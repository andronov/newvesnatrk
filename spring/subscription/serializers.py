from rest_framework import serializers
from rest_framework.fields import WritableField
from rest_framework.serializers import ModelSerializer
from captcha.models import CaptchaStore
from django.utils.translation import ugettext_lazy as _

from spring.subscription.models import Subscriber, VerifyingCode


class SMSSerializer(ModelSerializer):
    class Meta:
        model = VerifyingCode
        exclude = ('id',)


class CaptchaSerializer(serializers.Serializer):
    captcha_0 = serializers.CharField(write_only=True)
    captcha_1 = serializers.CharField(write_only=True)

    @property
    def errors(self):
        errors = super(CaptchaSerializer, self).errors
        try:
            if 'captcha_1' in errors:
                errors['captcha'] = errors.pop('captcha_1')
                errors.pop('captcha_0')
        except KeyError:
            pass

        try:
            if 'captcha_0' in errors:
                errors['captcha'] = errors.pop('captcha_0')
                errors.pop('captcha_1')
        except KeyError:
            pass
        return errors

    def validate_captcha_1(self, attrs, captcha_1):
        captcha_0 = attrs['captcha_0']
        captcha_1 = attrs['captcha_1']

        try:
            CaptchaStore.objects.get(response=captcha_1.lower(), hashkey=captcha_0)
        except CaptchaStore.DoesNotExist:
            raise serializers.ValidationError(_('Invalid CAPTCHA.'))
        return attrs


class SubscriberSmsSerializer(ModelSerializer):
    is_agree = serializers.BooleanField(write_only=True)
    phone = WritableField(source='phone', required=True)
    email = WritableField(source='email', required=False)

    def validate_is_agree(self, attrs, is_agree):
        if not attrs[is_agree]:
            raise serializers.ValidationError(_('Must be checked.'))
        return attrs
    
    def save(self, **kwargs):
        kwargs.update({
            'force_insert': False
        })
        return super(SubscriberSmsSerializer, self).save(**kwargs)

    class Meta:
        model = Subscriber
        exclude = ('active', 'created_at')


class SubscriberSerializer(CaptchaSerializer, SubscriberSmsSerializer):
    phone = WritableField(source='phone', required=False)
    email = WritableField(source='email', required=True)
