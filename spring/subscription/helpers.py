# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import datetime
import re
import requests
from django.conf import settings
from spring.subscription.models import SubscriptionSMSMessage
from django.utils.dateformat import format


SMS_STATUSES = {
    100: 'Cообщение принято к отправке.',
    101: 'Сообщение передается оператору.',
    102: 'Сообщение отправлено (в пути).',
    103: 'Сообщение доставлено.',
    104: 'Не может быть доставлено: время жизни истекло.',
    105: 'Не может быть доставлено: удалено оператором.',
    106: 'Не может быть доставлено: сбой в телефоне.',
    107: 'Не может быть доставлено: неизвестная причина.',
    108: 'Не может быть доставлено: отклонено.',
    200: 'Неправильный api_id, обратитесь к разработчику.',
    201: 'Не хватает средств на лицевом счету.',
    202: 'Неправильно указан получатель.',
    203: 'Нет текста сообщения.',
    204: 'Имя отправителя не согласовано с администрацией sms.ru.',
    205: 'Сообщение слишком длинное (превышает 8 СМС).',
    206: 'Будет превышен или уже превышен дневной лимит на отправку сообщений.',
    207: 'На этот номер нельзя отправлять сообщения.',
    208: 'Параметр time указан неправильно.',
    209: 'Вы добавили этот номер (или один из номеров) в стоп-лист.',
    210: 'Используется GET, где необходимо использовать POST.',
    211: 'Метод не найден.',
    212: 'Текст сообщения необходимо передать в кодировке UTF-8 (вы передали в другой кодировке)',
    220: 'Сервис временно недоступен, попробуйте чуть позже.',
    230: 'Сообщение не принято к отправке, так как на один номер в день нельзя отправлять более 60 сообщений.',
    300: 'Неправильный token (возможно истек срок действия, либо ваш IP изменился).',
    301: 'Неправильный пароль, либо пользователь не найден.',
    302: 'Пользователь авторизован, но аккаунт не подтвержден (пользователь не ввел код, присланный в регистрационной смс).'
}


class SMS(object):
    api_key = settings.SMS_API_KEY
    api_url = 'http://sms.ru/sms/'

    def request(self, method, data):
        response = requests.post(self.api_url + method + '/?api_id=%s' % self.api_key, data)

        if response.status_code == 200:
            return response.text

        return response

    def stop_list(self):
        response = requests.post('http://sms.ru/stoplist/get?api_id={}'.format(self.api_key))
        if response.status_code == 200:
            try:
                return [int(phone.split(';')[0][1:]) for phone in response.text.split('\n')[1:]]
            except (ValueError, IndexError):
                return []
        return []

    def send_sms(self, data):
        subscribers = data['subscribers']
        if 'send_at' in data:
            send_at = int(format(data['send_at'], u'U'))
        else:
            send_at = format(datetime.datetime.now(), u'U')

        phones = {x.phone: x for x in subscribers if (
            re.match('^9\d{9}$', x.phone) is not None
        ) or (
            re.match('^89\d{9}$', x.phone) is not None
        )}

        sub_list = []
        statuses = []

        text = data['text']
        for i, phone in enumerate(phones.keys()):
            sub_list.append(phone)
            if i + 1 % 99 == 0:
                new_data = {}
                for p in sub_list:
                    new_data['multi[%s]' % p] = text
                sub_list = []
                new_data['time'] = send_at
                statuses.append(self.request('send', new_data))
        else:
            new_data = {}
            for p in sub_list:
                new_data['multi[%s]' % p] = text
            new_data['time'] = send_at
            statuses.append(self.request('send', new_data))

        for j, status in enumerate(statuses):
            status = status.split('\n')
            status_code = int(status[0])

            for i, phone in enumerate(phones.keys()[j * 99:]):

                sms = SubscriptionSMSMessage()
                sms.status = SMS_STATUSES[status_code]
                sms.status_code = status_code
                if status_code == 100:
                    sms.sms_id = status[i + 1]
                sms.subscriber = phones[phone]
                sms.save()
                if i + 1 % 99 == 0:
                    break
