# -*- coding: utf-8 -*-
from django.core.validators import RegexValidator
from django.db import models
from django.utils.translation import ugettext_lazy as _
from spring.events.models import SpringEvent, BaseEvent


SUB_TYPES = (
    ('movie', _('Movie')),
    ('entertainment', _('Entertainments')),
    ('general', _('General')),
)

EVENT_CATEGORY_CHOICES = (
    ('favorite_weekend', _('Favorite weekend')),
    ('for_children', _('For children')),
    ('special', _('Special'))
)


class Subscriber(models.Model):
    is_agree = models.BooleanField(_('User agree'), default=False)
    first_name = models.CharField(_('First name'), max_length=255, null=True, blank=True)
    last_name = models.CharField(_('Last name'), max_length=255, null=True, blank=True)
    city = models.CharField(_('City'), max_length=255, null=True, blank=True)
    is_male = models.BooleanField(_('Sex is male'), default=True)
    email = models.EmailField(_('User email'), max_length=255, null=True, blank=True)
    birthday = models.DateField(_('Birthday'), null=True, blank=True)
    active = models.BooleanField(_('Active status'), default=True)
    sms_active = models.BooleanField(_('Active status sms'), default=False)
    phone = models.CharField(
        _('Phone number'),
        max_length=20,
        validators=[RegexValidator(r'^9\d{9}$')],
        blank=True,
        null=True
    )
    created_at = models.DateTimeField(_('Created time'), auto_now_add=True, null=True)

    class Meta:
        verbose_name = _('Subscriber')
        verbose_name_plural = _('Subscribers')

    def __unicode__(self):
        return self.email or self.phone


class SubscriptionSMSMessage(models.Model):
    subscriber = models.ForeignKey(Subscriber, verbose_name=_('Subscriber'), related_name='subscription_sms_messages')
    status = models.CharField(_('Status'), default='', max_length=1000)
    sms_id = models.CharField(_('SMS ID from sms.ru'), max_length=50, default='', db_index=True)
    status_code = models.PositiveSmallIntegerField(_('Status code'), default=0, db_index=True)

    class Meta:
        verbose_name = _('Subscription SMS message')
        verbose_name_plural = _('Subscription SMS messages')

    def __unicode__(self):
        return self.subscriber.phone


class VerifyingCode(models.Model):
    code = models.PositiveIntegerField(_('Verifying code'), default=123432)
    phone = models.CharField(
        _('Phone number'),
        max_length=20,
        validators=[RegexValidator(r'^9\d{9}$')]
    )


class MessageItem(models.Model):
    accost = models.CharField(_('Accost'), default=u'Здравствуйте', max_length=255)
    subject = models.CharField(_('Message subject'), max_length=255)

    subscribers = models.ManyToManyField(Subscriber, verbose_name=_('Subscribers'))
    for_male = models.NullBooleanField(_('For male users'), default=None, null=True, blank=True)

    created_at = models.DateTimeField(_('Created at'), auto_now_add=True)

    class Meta:
        verbose_name = _('Message')
        verbose_name_plural = _('Messages')

    def __unicode__(self):
        return u'{} {}'.format(
            self.subject,
            self.created_at
        )


class SmsMessageItem(models.Model):
    message = models.TextField(_('Message'), max_length=200)
    subscribers = models.ManyToManyField(Subscriber, verbose_name=_('Subscribers'))
    for_male = models.NullBooleanField(_('For male users'), default=None, null=True, blank=True)
    created_at = models.DateTimeField(_('Created at'), auto_now_add=True)
    total_users = models.PositiveSmallIntegerField(_('Total users'), null=True, blank=True)
    to_send_at = models.DateTimeField(_('Send at'), blank=True, null=True)
    retry = models.BooleanField(_('Retry if fail'), default=True)

    class Meta:
        verbose_name = _('Sms message')
        verbose_name_plural = _('Sms messages')

    def __unicode__(self):
        return self.message[:30]


class MessageQueue(models.Model):
    subscriber = models.ForeignKey(Subscriber, verbose_name=_('Subscriber'))
    message = models.ForeignKey(MessageItem, verbose_name=_('Message item'))
    sent = models.BooleanField(_('Sent'), default=False)
    sent_at = models.DateTimeField(_('Sent time'), null=True, blank=True)
    read = models.BooleanField(_('Read'), default=False)
    read_at = models.DateTimeField(_('Read time'), null=True, blank=True)

    class Meta:
        verbose_name = _('Message queue')
        verbose_name_plural = _('Message queue')
        unique_together = ('subscriber', 'message')

    def __unicode__(self):
        return 'Message for subscriber {}. Message_id = {}'.format(
            self.subscriber.email,
            self.message_id
        )


class SmsMessageQueue(models.Model):
    subscriber = models.ForeignKey(Subscriber, verbose_name=_('Subscriber'))
    message = models.ForeignKey(SmsMessageItem, verbose_name=_('Message item'))
    status = models.CharField(_('Status'), blank=True, null=True, max_length=1000)
    status_code = models.PositiveSmallIntegerField(_('Status code'), blank=True, null=True)
    sms_id = models.CharField(_('SMS ID from sms.ru'), max_length=50, blank=True, null=True)
    sent = models.BooleanField(_('Sent'), default=False)
    sent_at = models.DateTimeField(_('Sent time'), null=True, blank=True)

    class Meta:
        verbose_name = _('Sms message queue')
        verbose_name_plural = _('Sms message queue')
        unique_together = ('subscriber', 'message')
        ordering = ('-pk',)

    def __unicode__(self):
        return 'Message for subscriber {}. Message_id = {}'.format(
            self.subscriber.phone,
            self.message_id
        )


class SpringEventToMessage(models.Model):
    event = models.ForeignKey(SpringEvent, verbose_name=_('Spring event'))
    category = models.CharField(_('Category'), max_length=255, choices=EVENT_CATEGORY_CHOICES)
    message = models.ForeignKey(MessageItem, verbose_name=_('Spring events'), related_name='spring_events')


class EventToMessage(models.Model):
    event = models.ForeignKey(BaseEvent, verbose_name=_('Event'))
    message = models.ForeignKey(MessageItem, verbose_name=_('Message'), related_name='other_events')


#SIGNALS
def subscribers_prepare(sender, instance, action, reverse, model, pk_set, **kwargs):
    if action == 'post_add':
        bulk = []

        for subscriber in instance.subscribers.all():
            if (
                instance.for_male is True and not subscriber.is_male
            ) or (
                instance.for_male is False and subscriber.is_male
            ):
                continue
            bulk.append(
                MessageQueue(
                    subscriber_id=subscriber.pk,
                    message_id=instance.pk,
                )
            )
        MessageQueue.objects.bulk_create(bulk)


def sms_subscribers_prepare(sender, instance, action, reverse, model, pk_set, **kwargs):
    if action == 'post_add':
        bulk = []

        for subscriber in instance.subscribers.all():
            if (
                instance.for_male is True and not subscriber.is_male
            ) or (
                instance.for_male is False and subscriber.is_male
            ):
                continue
            bulk.append(
                SmsMessageQueue(
                    subscriber_id=subscriber.pk,
                    message_id=instance.pk,
                )
            )
        SmsMessageQueue.objects.bulk_create(bulk)
        instance.total_users = len(bulk)
        instance.save()


def create_queue(sender, instance, created, **kwargs):
    if created:
        models.signals.m2m_changed.connect(subscribers_prepare, sender=instance.subscribers.through)


def create_sms_queue(sender, instance, created, **kwargs):
    if created:
        models.signals.m2m_changed.connect(sms_subscribers_prepare, sender=instance.subscribers.through)


def subscriber_statuses(sender, instance, **kwargs):
    if not instance.email or instance.email == '':
        instance.active = False
    if not instance.phone or instance.phone == '':
        instance.sms_active = False


models.signals.post_save.connect(create_queue, sender=MessageItem)
models.signals.post_save.connect(create_sms_queue, sender=SmsMessageItem)
models.signals.pre_save.connect(subscriber_statuses, sender=Subscriber)
