from django.conf.urls import patterns, url


urlpatterns = patterns(
    'spring.google_analytics.views',
    url(r'^page_view/$', 'page_view'),
    url(r'^open_form/$', 'open_form'),
    url(r'^sms_confirm/$', 'sms_confirm'),
)
