from requests import Request, Session
from rest_framework.response import Response
from rest_framework.views import APIView
from random import randint


class BaseGoogleAnalyticView(APIView):
    method = 'GET'

    def __init__(self, *args, **kwargs):
        super(BaseGoogleAnalyticView, self).__init__(*args, **kwargs)
        self.cid = None

    def get_google_request(self):
        raise NotImplementedError

    def post(self, request):
        self.cid = request.COOKIES.get('cid')
        if self.cid is None:
            self.cid = randint(1, 1000000)

        s = Session()
        google_request = self.get_google_request()
        prepared = google_request.prepare()
        google_response = s.send(
            prepared
        )
        response = Response({
            'response': google_response.status_code
        })

        if request.COOKIES.get('cid') is None:
            response.set_cookie('cid', self.cid)
        return response


class PageView(BaseGoogleAnalyticView):
    def get_google_request(self):
        return Request(
            method='GET',
            url='http://www.google-analytics.com/collect?v={}&tid={}&cid={}&t={}&dl={}'.format(
                1,
                'UA-48588008-1',
                self.cid,
                'pageview',
                '%2Fwifi.html'
            ),
        )
page_view = PageView.as_view()


class OpenFormEventView(BaseGoogleAnalyticView):
    def get_google_request(self):
        return Request(
            method='GET',
            url='http://www.google-analytics.com/collect?v={}&tid={}&cid={}&t={}&dl={}&el={}&ea={}&ec={}'.format(
                1,
                'UA-48588008-1',
                self.cid,
                'event',
                '%2Fwifi.html',
                'open_form',
                'click',
                'sms_button'
            ),
        )
open_form = OpenFormEventView.as_view()


class SmsConfirmEventView(BaseGoogleAnalyticView):
    def get_google_request(self):
        return Request(
            method='GET',
            url='http://www.google-analytics.com/collect?v={}&tid={}&cid={}&t={}&dl={}&el={}&ea={}&ec={}'.format(
                1,
                'UA-48588008-1',
                self.cid,
                'event',
                '%2Fwifi.html',
                'confirm',
                'click',
                'sms_button'
            ),
        )
sms_confirm = SmsConfirmEventView.as_view()
