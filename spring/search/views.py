# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db.models import Q
from rest_framework.response import Response
from rest_framework.views import APIView
from spring.cinema.models import Movie
from spring.cinema.serializers import CinemaToSearchSerializer
from spring.events.models import BaseEvent
from spring.events.serializers import EventToContentSerializer
from spring.shops.models import Brand, Restaurant
from spring.shops.serializers import BrandInEventsSerializer, RestaurantInEventSerializer


class NewSearchView(APIView):
    def get(self, request, query):
        return Response({
            'object_list': [BrandInEventsSerializer(item).data for item in Brand.objects.filter(
                Q(name_ru__icontains=query) |
                Q(name_en__icontains=query) |
                Q(synonyms__synonym__icontains=query)
            ).distinct()] + [RestaurantInEventSerializer(item).data for item in Restaurant.objects.filter(
                Q(name_ru__icontains=query) |
                Q(name_en__icontains=query) |
                Q(synonyms__synonym__icontains=query)
            ).distinct()] + [EventToContentSerializer(item).data for item in BaseEvent.objects.filter(
                Q(title_ru__icontains=query) |
                Q(title_en__icontains=query)
            ).distinct()] + [CinemaToSearchSerializer(item).data for item in Movie.objects.filter(
                Q(title_ru__icontains=query) |
                Q(title_en__icontains=query)
            ).distinct()]
        })
