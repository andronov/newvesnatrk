from django.conf.urls import patterns, url
from spring.search.views import NewSearchView


urlpatterns = patterns(
    '',
    url(r'^(?P<query>[-\w]+)/$', NewSearchView.as_view(), name='search'),

)
