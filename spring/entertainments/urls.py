from django.conf.urls import patterns, url


urlpatterns = patterns(
    'spring.entertainments.views',
    url(r'^$', 'entertainment_list'),
    url(r'^(?P<pk>\d+)$', 'entertainment_detail'),

)
