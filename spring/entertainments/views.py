# -*- coding: utf-8 -*-
from datetime import datetime
from django.db.models import Q
from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.response import Response
from spring.cinema.models import Movie
from spring.cinema.serializers import CinemaSerializer
from spring.entertainments.models import Entertainment
from spring.entertainments.serializers import EntertainmentSerializer
from spring.events.models import BaseEvent
from spring.events.serializers import EventInEntertainmentSerializer
from spring.mainpage.models import EntertainmentsContent
from spring.mainpage.serializers import EntertainmentsContentSerializer


class EntertainmentList(ListAPIView):
    model = Entertainment
    serializer_class = EntertainmentSerializer

    def get_queryset(self):
        qs = super(EntertainmentList, self).get_queryset().filter(published=True)
        search_string = self.request.GET.get('isearch', '')
        if search_string:
            qs = qs.filter(Q(title_ru__icontains=search_string) | Q(title_en__icontains=search_string))
        return qs

    def list(self, request, *args, **kwargs):
        serializer = self.get_serializer(self.get_queryset(), many=True)

        events = BaseEvent.objects.filter(
            is_published=True,
            is_entertainment=True,
            end_date__gte=datetime.today(),
            pub_date__lte=datetime.today()
        ).order_by('-springevent', '-pub_date')[:4]

        try:
            cinema = Movie.objects.filter(
                is_premiere=True,
                schedule__date__gte=datetime.today()
            ).order_by('?')[0]
        except Exception:
            cinema = None

        return Response({
            'object_list': serializer.data,
            'event_list': [EventInEntertainmentSerializer(item).data for item in events],
            'main_text': EntertainmentsContentSerializer(EntertainmentsContent.objects.get()).data,
            'cinema': CinemaSerializer(cinema).data if cinema else None
        })
entertainment_list = EntertainmentList.as_view()


class EntertainmentDetail(RetrieveAPIView):
    model = Entertainment
    serializer_class = EntertainmentSerializer

    def __init__(self, *args, **kwargs):
        super(EntertainmentDetail, self).__init__(*args, **kwargs)
        self.object = None

    def retrieve(self, request, *args, **kwargs):
        self.object = self.get_object()
        serializer = self.get_serializer(self.object)
        return Response({
            'object': serializer.data
        })
entertainment_detail = EntertainmentDetail.as_view()
