from django.utils.translation import get_language
from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import ModelSerializer
from spring.entertainments.models import Entertainment, EntertainmentImage
from utilities.fields import SerializerLocaleField


class EntertainmentImageSerializer(ModelSerializer):
    source = SerializerMethodField('get_source')

    def get_source(self, obj):
        try:
            return getattr(obj, 'source_{}'.format(get_language())).url
        except AttributeError:
            return ''

    class Meta:
        model = EntertainmentImage
        fields = ('source',)


class EntertainmentSerializer(ModelSerializer):
    description = SerializerLocaleField()
    description_short = SerializerLocaleField()
    description_title = SerializerLocaleField()
    title = SerializerLocaleField()
    internal_page_image = SerializerMethodField('get_internal_page_image')
    internal_page_image_child = SerializerMethodField('get_internal_page_image_child')
    internal_page_image_frame = SerializerMethodField('get_internal_page_image_frame')
    images = EntertainmentImageSerializer()

    def get_internal_page_image(self, obj):
        try:
            return obj.internal_page_image.url
        except AttributeError:
            return ''

    def get_internal_page_image_child(self, obj):
        try:
            return obj.internal_page_image_child.url
        except AttributeError:
            return ''

    def get_internal_page_image_frame(self, obj):
        try:
            return obj.internal_page_image_frame.url
        except AttributeError:
                return ''

    class Meta:
        model = Entertainment
        fields = (
            'contacts', 'description', 'description_short', 'description_title', 'id', 'images', 'internal_page_image',
            'internal_page_image_child', 'internal_page_image_frame', 'place_on_map', 'published', 'title',
            'url', 'work_time'
        )


class EntertainmentToContentSerializer(EntertainmentSerializer):
    class Meta(EntertainmentSerializer.Meta):
        fields = ('id', 'internal_page_image','url')


class EntertainmentInEventsSerializer(EntertainmentSerializer):
    class Meta(EntertainmentSerializer.Meta):
        fields = ('id', 'title')

