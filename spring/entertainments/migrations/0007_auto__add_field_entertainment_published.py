# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Entertainment.published'
        db.add_column(u'entertainments_entertainment', 'published',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Entertainment.published'
        db.delete_column(u'entertainments_entertainment', 'published')


    models = {
        u'entertainments.entertainment': {
            'Meta': {'object_name': 'Entertainment'},
            'contacts': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'description_en': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '2000', 'blank': 'True'}),
            'description_ru': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '2000'}),
            'description_short_en': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '500', 'blank': 'True'}),
            'description_short_ru': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '500'}),
            'description_title_en': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '200', 'blank': 'True'}),
            'description_title_ru': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '200'}),
            'for_children': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'internal_page_image': ('filebrowser.fields.FileBrowseField', [], {'max_length': '254', 'blank': 'True'}),
            'internal_page_image_child': ('filebrowser.fields.FileBrowseField', [], {'max_length': '254', 'blank': 'True'}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'title_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '128', 'blank': 'True'}),
            'work_time': ('django.db.models.fields.CharField', [], {'default': "'10:00 - 19:00'", 'max_length': '13'})
        },
        u'entertainments.entertainmentimage': {
            'Meta': {'object_name': 'EntertainmentImage'},
            'entertainment': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'images'", 'to': u"orm['entertainments.Entertainment']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'source_en': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'source_ru': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['entertainments']