# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Entertainment.description_title'
        db.add_column(u'entertainments_entertainment', 'description_title',
                      self.gf('ckeditor.fields.RichTextField')(default='', max_length=200),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Entertainment.description_title'
        db.delete_column(u'entertainments_entertainment', 'description_title')


    models = {
        u'entertainments.entertainment': {
            'Meta': {'object_name': 'Entertainment'},
            'description': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '2000'}),
            'description_title': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '200'}),
            'for_children': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'short_description': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '500'}),
            'title': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'})
        },
        u'entertainments.entertainmentimage': {
            'Meta': {'object_name': 'EntertainmentImage'},
            'entertainment': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'images'", 'to': u"orm['entertainments.Entertainment']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'source': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['entertainments']