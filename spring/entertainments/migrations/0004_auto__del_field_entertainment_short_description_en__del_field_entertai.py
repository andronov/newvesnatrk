# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Entertainment.short_description_en'
        db.delete_column(u'entertainments_entertainment', 'short_description_en')

        # Deleting field 'Entertainment.short_description_ru'
        db.delete_column(u'entertainments_entertainment', 'short_description_ru')

        # Adding field 'Entertainment.description_short_ru'
        db.add_column(u'entertainments_entertainment', 'description_short_ru',
                      self.gf('ckeditor.fields.RichTextField')(default='', max_length=500),
                      keep_default=False)

        # Adding field 'Entertainment.description_short_en'
        db.add_column(u'entertainments_entertainment', 'description_short_en',
                      self.gf('ckeditor.fields.RichTextField')(default='', max_length=500, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'Entertainment.short_description_en'
        db.add_column(u'entertainments_entertainment', 'short_description_en',
                      self.gf('ckeditor.fields.RichTextField')(default='', max_length=500, blank=True),
                      keep_default=False)

        # Adding field 'Entertainment.short_description_ru'
        db.add_column(u'entertainments_entertainment', 'short_description_ru',
                      self.gf('ckeditor.fields.RichTextField')(default='', max_length=500),
                      keep_default=False)

        # Deleting field 'Entertainment.description_short_ru'
        db.delete_column(u'entertainments_entertainment', 'description_short_ru')

        # Deleting field 'Entertainment.description_short_en'
        db.delete_column(u'entertainments_entertainment', 'description_short_en')


    models = {
        u'entertainments.entertainment': {
            'Meta': {'object_name': 'Entertainment'},
            'description_en': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '2000', 'blank': 'True'}),
            'description_ru': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '2000'}),
            'description_short_en': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '500', 'blank': 'True'}),
            'description_short_ru': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '500'}),
            'description_title_en': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '200', 'blank': 'True'}),
            'description_title_ru': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '200'}),
            'for_children': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'})
        },
        u'entertainments.entertainmentimage': {
            'Meta': {'object_name': 'EntertainmentImage'},
            'entertainment': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'images'", 'to': u"orm['entertainments.Entertainment']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'source_en': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'source_ru': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['entertainments']