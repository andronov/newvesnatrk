# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Entertainment'
        db.create_table(u'entertainments_entertainment', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('description', self.gf('ckeditor.fields.RichTextField')(default='', max_length=2000)),
            ('short_description', self.gf('ckeditor.fields.RichTextField')(default='', max_length=500)),
            ('for_children', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'entertainments', ['Entertainment'])

        # Adding model 'EntertainmentImage'
        db.create_table(u'entertainments_entertainmentimage', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('source', self.gf('filebrowser.fields.FileBrowseField')(default='', max_length=254, null=True, blank=True)),
            ('entertainment', self.gf('django.db.models.fields.related.ForeignKey')(related_name='images', to=orm['entertainments.Entertainment'])),
        ))
        db.send_create_signal(u'entertainments', ['EntertainmentImage'])


    def backwards(self, orm):
        # Deleting model 'Entertainment'
        db.delete_table(u'entertainments_entertainment')

        # Deleting model 'EntertainmentImage'
        db.delete_table(u'entertainments_entertainmentimage')


    models = {
        u'entertainments.entertainment': {
            'Meta': {'object_name': 'Entertainment'},
            'description': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '2000'}),
            'for_children': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'short_description': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '500'}),
            'title': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'})
        },
        u'entertainments.entertainmentimage': {
            'Meta': {'object_name': 'EntertainmentImage'},
            'entertainment': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'images'", 'to': u"orm['entertainments.Entertainment']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'source': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['entertainments']