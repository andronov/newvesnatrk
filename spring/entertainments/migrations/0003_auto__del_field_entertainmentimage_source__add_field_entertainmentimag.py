# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'EntertainmentImage.source'
        db.delete_column(u'entertainments_entertainmentimage', 'source')

        # Adding field 'EntertainmentImage.source_ru'
        db.add_column(u'entertainments_entertainmentimage', 'source_ru',
                      self.gf('filebrowser.fields.FileBrowseField')(default='', max_length=254, null=True, blank=True),
                      keep_default=False)

        # Adding field 'EntertainmentImage.source_en'
        db.add_column(u'entertainments_entertainmentimage', 'source_en',
                      self.gf('filebrowser.fields.FileBrowseField')(default='', max_length=254, null=True, blank=True),
                      keep_default=False)

        # Deleting field 'Entertainment.description'
        db.delete_column(u'entertainments_entertainment', 'description')

        # Deleting field 'Entertainment.title'
        db.delete_column(u'entertainments_entertainment', 'title')

        # Deleting field 'Entertainment.description_title'
        db.delete_column(u'entertainments_entertainment', 'description_title')

        # Deleting field 'Entertainment.short_description'
        db.delete_column(u'entertainments_entertainment', 'short_description')

        # Adding field 'Entertainment.title_ru'
        db.add_column(u'entertainments_entertainment', 'title_ru',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=255),
                      keep_default=False)

        # Adding field 'Entertainment.title_en'
        db.add_column(u'entertainments_entertainment', 'title_en',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True),
                      keep_default=False)

        # Adding field 'Entertainment.description_ru'
        db.add_column(u'entertainments_entertainment', 'description_ru',
                      self.gf('ckeditor.fields.RichTextField')(default='', max_length=2000),
                      keep_default=False)

        # Adding field 'Entertainment.description_en'
        db.add_column(u'entertainments_entertainment', 'description_en',
                      self.gf('ckeditor.fields.RichTextField')(default='', max_length=2000, blank=True),
                      keep_default=False)

        # Adding field 'Entertainment.description_title_ru'
        db.add_column(u'entertainments_entertainment', 'description_title_ru',
                      self.gf('ckeditor.fields.RichTextField')(default='', max_length=200),
                      keep_default=False)

        # Adding field 'Entertainment.description_title_en'
        db.add_column(u'entertainments_entertainment', 'description_title_en',
                      self.gf('ckeditor.fields.RichTextField')(default='', max_length=200, blank=True),
                      keep_default=False)

        # Adding field 'Entertainment.short_description_ru'
        db.add_column(u'entertainments_entertainment', 'short_description_ru',
                      self.gf('ckeditor.fields.RichTextField')(default='', max_length=500),
                      keep_default=False)

        # Adding field 'Entertainment.short_description_en'
        db.add_column(u'entertainments_entertainment', 'short_description_en',
                      self.gf('ckeditor.fields.RichTextField')(default='', max_length=500, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'EntertainmentImage.source'
        db.add_column(u'entertainments_entertainmentimage', 'source',
                      self.gf('filebrowser.fields.FileBrowseField')(default='', max_length=254, null=True, blank=True),
                      keep_default=False)

        # Deleting field 'EntertainmentImage.source_ru'
        db.delete_column(u'entertainments_entertainmentimage', 'source_ru')

        # Deleting field 'EntertainmentImage.source_en'
        db.delete_column(u'entertainments_entertainmentimage', 'source_en')

        # Adding field 'Entertainment.description'
        db.add_column(u'entertainments_entertainment', 'description',
                      self.gf('ckeditor.fields.RichTextField')(default='', max_length=2000),
                      keep_default=False)

        # Adding field 'Entertainment.title'
        db.add_column(u'entertainments_entertainment', 'title',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=255),
                      keep_default=False)

        # Adding field 'Entertainment.description_title'
        db.add_column(u'entertainments_entertainment', 'description_title',
                      self.gf('ckeditor.fields.RichTextField')(default='', max_length=200),
                      keep_default=False)

        # Adding field 'Entertainment.short_description'
        db.add_column(u'entertainments_entertainment', 'short_description',
                      self.gf('ckeditor.fields.RichTextField')(default='', max_length=500),
                      keep_default=False)

        # Deleting field 'Entertainment.title_ru'
        db.delete_column(u'entertainments_entertainment', 'title_ru')

        # Deleting field 'Entertainment.title_en'
        db.delete_column(u'entertainments_entertainment', 'title_en')

        # Deleting field 'Entertainment.description_ru'
        db.delete_column(u'entertainments_entertainment', 'description_ru')

        # Deleting field 'Entertainment.description_en'
        db.delete_column(u'entertainments_entertainment', 'description_en')

        # Deleting field 'Entertainment.description_title_ru'
        db.delete_column(u'entertainments_entertainment', 'description_title_ru')

        # Deleting field 'Entertainment.description_title_en'
        db.delete_column(u'entertainments_entertainment', 'description_title_en')

        # Deleting field 'Entertainment.short_description_ru'
        db.delete_column(u'entertainments_entertainment', 'short_description_ru')

        # Deleting field 'Entertainment.short_description_en'
        db.delete_column(u'entertainments_entertainment', 'short_description_en')


    models = {
        u'entertainments.entertainment': {
            'Meta': {'object_name': 'Entertainment'},
            'description_en': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '2000', 'blank': 'True'}),
            'description_ru': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '2000'}),
            'description_title_en': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '200', 'blank': 'True'}),
            'description_title_ru': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '200'}),
            'for_children': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'short_description_en': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '500', 'blank': 'True'}),
            'short_description_ru': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '500'}),
            'title_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'})
        },
        u'entertainments.entertainmentimage': {
            'Meta': {'object_name': 'EntertainmentImage'},
            'entertainment': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'images'", 'to': u"orm['entertainments.Entertainment']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'source_en': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'source_ru': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['entertainments']