# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _

from easymode.i18n.decorators import I18n
from filebrowser.fields import FileBrowseField
from ckeditor.fields import RichTextField
from spring.shops.models import ModelDiffMixin


@I18n('description', 'title', 'description_title', 'description_short')
class Entertainment(ModelDiffMixin, models.Model):
    title = models.CharField(_('Title'), max_length=255, default='')
    description = RichTextField(_('Description'), config_name='big', max_length=2000, default='')
    description_title = RichTextField(_('Title for description'), config_name='big', max_length=200, default='')
    description_short = RichTextField(_('Short description'), config_name='big', max_length=500, default='')
    internal_page_image = FileBrowseField(
        verbose_name=_('File'),
        directory='entertainments/',
        max_length=254,
        blank=True
    )
    internal_page_image_frame = FileBrowseField(
        verbose_name=_('Image frame'),
        directory='entertainments/',
        max_length=254,
        null=True,
        blank=True
    )
    internal_page_image_child = FileBrowseField(
        verbose_name=_('File for child page'),
        directory='entertainments/',
        max_length=254,
        blank=True
    )
    contacts = models.CharField(_('Contacts'), max_length=128, blank=True)
    url = models.URLField(_('URL'), max_length=128, blank=True)
    work_time = models.CharField(_('Work time'), max_length=13, default='10:00 - 19:00')
    for_children = models.BooleanField(_('For children'), default=False)
    published = models.BooleanField(_('Publish'), default=False)
    place_on_map = models.CharField(_('Place on map'), max_length=255, blank=True)

    @property
    def mail_image(self):
        image = getattr(self, 'internal_page_image')
        if image is None or isinstance(image, basestring):
            return None
        return image.version_generate('mail_poster') if image.exists() else None

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = _('Entertainment')
        verbose_name_plural = _('Entertainments')


@I18n('source')
class EntertainmentImage(models.Model):
    source = FileBrowseField(
        verbose_name=_('File'),
        directory='entertainments/',
        max_length=254,
        null=True,
        default='',
        blank=True)
    entertainment = models.ForeignKey(Entertainment, verbose_name=_('Entertainment'), related_name='images')

    def __unicode__(self):
        return self.entertainment.title

    class Meta:
        verbose_name = _('Entertainment image')
        verbose_name_plural = _('Entertainment images')