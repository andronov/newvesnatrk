# -*- coding: utf-8 -*-
from django.contrib import admin
from spring.entertainments.models import Entertainment, EntertainmentImage


class EntertainmentImageInline(admin.TabularInline):
    model = EntertainmentImage


class EntertainmentAdmin(admin.ModelAdmin):
    inlines = (EntertainmentImageInline,)

admin.site.register(Entertainment, EntertainmentAdmin)
