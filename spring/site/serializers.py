from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import ModelSerializer
from spring.seopages.models import PagesMetadata
from spring.seopages.serializers import PagesMetadataSerializer
from spring.site.models import Settings, Presentation
from utilities.fields import SerializerLocaleField
from django.conf import settings


class PresentationSerializer(ModelSerializer):
    presentation = SerializerMethodField('get_presentation')

    def get_presentation(self, obj):
        return obj.presentation.url

    class Meta:
        model = Presentation
        fields = ('name', 'presentation', 'id')


class SettingSerializer(ModelSerializer):
    slogan_one = SerializerLocaleField()
    slogan_two = SerializerLocaleField()
    slogan_three = SerializerLocaleField()
    slogan_four = SerializerLocaleField()
    slogan_five = SerializerLocaleField()
    slogan_cinema = SerializerLocaleField()
    slogan_entertainment = SerializerLocaleField()
    slogan_children = SerializerLocaleField()
    children_cat_name_one = SerializerLocaleField()
    children_cat_name_two = SerializerLocaleField()
    feedback_form_text = SerializerLocaleField()
    title_entertainments = SerializerLocaleField()
    title_child = SerializerLocaleField()
    title_events = SerializerLocaleField()
    title_restaurants = SerializerLocaleField()
    title_brands = SerializerLocaleField()
    renter_form_text = SerializerLocaleField()
    adv_form_text = SerializerLocaleField()
    address_footer = SerializerLocaleField()
    kinohod_api_key = SerializerMethodField('get_kinohod_api_key')
    kinohod_cinema_id = SerializerMethodField('get_kinohod_cinema_id')
    metadata = SerializerMethodField('get_metadata')
    presentations = PresentationSerializer()

    def get_kinohod_api_key(self, obj):
        return settings.KINOHOD_CLIENT_API_KEY

    def get_kinohod_cinema_id(self, obj):
        return settings.KINOHOD_CINEMA_ID

    def get_metadata(self, obj):
        metadata = self.context['request'].GET.get('metadata_for', None)
        if metadata:
            return PagesMetadataSerializer(
                PagesMetadata.objects.get_or_none(tag=metadata)
            ).data
        else:
            return None

    class Meta:
        model = Settings
        fields = (
            'slogan_one', 'slogan_two', 'slogan_three', 'slogan_four', 'slogan_five', 'slogan_cinema',
            'slogan_entertainment', 'slogan_children', 'children_cat_name_one', 'children_cat_name_two',
            'feedback_form_text', 'id', 'title_entertainments', 'title_child', 'title_events', 'title_restaurants',
            'title_brands', 'renter_form_text', 'adv_form_text', 'address_footer', 'email_advertisement',
            'email_renter_request', 'enable_gallery', 'kinohod_api_key', 'kinohod_cinema_id', 'metadata',
            'presentations', 'slogan_five_color', 'slogan_four_color', 'slogan_one_color', 'slogan_three_color',
            'slogan_two_color'
        )