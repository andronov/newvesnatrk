# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Settings.children_cat_name_one'
        db.add_column(u'site_settings', 'children_cat_name_one',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=255),
                      keep_default=False)

        # Adding field 'Settings.children_cat_name_two'
        db.add_column(u'site_settings', 'children_cat_name_two',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=255),
                      keep_default=False)

        # Adding M2M table for field children_shops_for_one on 'Settings'
        m2m_table_name = db.shorten_name(u'site_settings_children_shops_for_one')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('settings', models.ForeignKey(orm[u'site.settings'], null=False)),
            ('brand', models.ForeignKey(orm[u'shops.brand'], null=False))
        ))
        db.create_unique(m2m_table_name, ['settings_id', 'brand_id'])

        # Adding M2M table for field children_shops_for_two on 'Settings'
        m2m_table_name = db.shorten_name(u'site_settings_children_shops_for_two')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('settings', models.ForeignKey(orm[u'site.settings'], null=False)),
            ('brand', models.ForeignKey(orm[u'shops.brand'], null=False))
        ))
        db.create_unique(m2m_table_name, ['settings_id', 'brand_id'])


    def backwards(self, orm):
        # Deleting field 'Settings.children_cat_name_one'
        db.delete_column(u'site_settings', 'children_cat_name_one')

        # Deleting field 'Settings.children_cat_name_two'
        db.delete_column(u'site_settings', 'children_cat_name_two')

        # Removing M2M table for field children_shops_for_one on 'Settings'
        db.delete_table(db.shorten_name(u'site_settings_children_shops_for_one'))

        # Removing M2M table for field children_shops_for_two on 'Settings'
        db.delete_table(db.shorten_name(u'site_settings_children_shops_for_two'))


    models = {
        u'shops.brand': {
            'Meta': {'ordering': "['place_number', '-pk']", 'object_name': 'Brand'},
            'categories': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'brands'", 'symmetrical': 'False', 'to': u"orm['shops.BrandCategory']"}),
            'color': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'brands'", 'null': 'True', 'to': u"orm['shops.Color']"}),
            'contacts': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '128'}),
            'description_en': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'description_ru': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo_en': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'logo_ru': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'}),
            'number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'place_number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1000'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '128', 'blank': 'True'}),
            'views_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'work_time': ('django.db.models.fields.CharField', [], {'default': "'10:00 - 19:00'", 'max_length': '13'})
        },
        u'shops.brandcategory': {
            'Meta': {'object_name': 'BrandCategory'},
            'banner': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'banner_url': ('django.db.models.fields.URLField', [], {'max_length': '255'}),
            'for_children': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'})
        },
        u'shops.color': {
            'Meta': {'object_name': 'Color'},
            'circle_color': ('colorful.fields.RGBColorField', [], {'max_length': '7'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'promotion_color': ('colorful.fields.RGBColorField', [], {'max_length': '7'})
        },
        u'site.settings': {
            'Meta': {'object_name': 'Settings'},
            'children_cat_name_one': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'children_cat_name_two': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'children_shops_for_one': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'children_shops_for_one'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['shops.Brand']"}),
            'children_shops_for_two': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'children_shops_for_two'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['shops.Brand']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'slogan_children_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'slogan_children_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'slogan_cinema_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'slogan_cinema_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'slogan_entertainment_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'slogan_entertainment_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'slogan_five_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'slogan_five_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'slogan_four_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'slogan_four_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'slogan_one_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'slogan_one_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'slogan_three_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'slogan_three_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'slogan_two_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'slogan_two_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'})
        }
    }

    complete_apps = ['site']