# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Settings'
        db.create_table(u'site_settings', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('slogan_one_ru', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('slogan_one_en', self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True)),
            ('slogan_two_ru', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('slogan_two_en', self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True)),
            ('slogan_three_ru', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('slogan_three_en', self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True)),
            ('slogan_four_ru', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('slogan_four_en', self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True)),
            ('slogan_five_ru', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('slogan_five_en', self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True)),
            ('slogan_cinema_ru', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('slogan_cinema_en', self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True)),
            ('slogan_entertainment_ru', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('slogan_entertainment_en', self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True)),
            ('slogan_children_ru', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('slogan_children_en', self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True)),
        ))
        db.send_create_signal(u'site', ['Settings'])


    def backwards(self, orm):
        # Deleting model 'Settings'
        db.delete_table(u'site_settings')


    models = {
        u'site.settings': {
            'Meta': {'object_name': 'Settings'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'slogan_children_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'slogan_children_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'slogan_cinema_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'slogan_cinema_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'slogan_entertainment_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'slogan_entertainment_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'slogan_five_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'slogan_five_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'slogan_four_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'slogan_four_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'slogan_one_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'slogan_one_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'slogan_three_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'slogan_three_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'slogan_two_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'slogan_two_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'})
        }
    }

    complete_apps = ['site']