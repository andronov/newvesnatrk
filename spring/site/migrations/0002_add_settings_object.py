# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import DataMigration
from django.db import models

class Migration(DataMigration):

    def forwards(self, orm):
        settings = orm['site.Settings']()
        settings.save()

    def backwards(self, orm):
        orm['site.Settings'].objects.all().delete()

    models = {
        u'site.settings': {
            'Meta': {'object_name': 'Settings'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'slogan_children_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'slogan_children_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'slogan_cinema_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'slogan_cinema_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'slogan_entertainment_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'slogan_entertainment_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'slogan_five_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'slogan_five_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'slogan_four_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'slogan_four_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'slogan_one_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'slogan_one_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'slogan_three_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'slogan_three_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'slogan_two_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'slogan_two_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'})
        }
    }

    complete_apps = ['site']
    symmetrical = True
