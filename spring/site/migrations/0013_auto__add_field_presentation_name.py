# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Presentation.name'
        db.add_column(u'site_presentation', 'name',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Presentation.name'
        db.delete_column(u'site_presentation', 'name')


    models = {
        u'shops.brand': {
            'Meta': {'ordering': "['place_number']", 'object_name': 'Brand'},
            'categories': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'brands'", 'symmetrical': 'False', 'to': u"orm['shops.BrandCategory']"}),
            'color': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'brands'", 'null': 'True', 'to': u"orm['shops.Color']"}),
            'contacts': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'description_en': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'description_ru': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True'}),
            'filter_apply': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo_en': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'logo_ru': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'number': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '256'}),
            'open_soon': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'place_number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1000', 'db_index': 'True'}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'show_on_main': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'url': ('spring.shops.fields.NullableURLField', [], {'default': 'None', 'max_length': '128', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'views_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'work_time': ('django.db.models.fields.CharField', [], {'default': "'10:00 - 19:00'", 'max_length': '13'})
        },
        u'shops.brandcategory': {
            'Meta': {'ordering': "('position',)", 'object_name': 'BrandCategory'},
            'banner': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'banner_url': ('django.db.models.fields.URLField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'for_children': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iphone_banner': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'}),
            'position': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        },
        u'shops.color': {
            'Meta': {'object_name': 'Color'},
            'circle_color': ('colorful.fields.RGBColorField', [], {'max_length': '7'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'promotion_color': ('colorful.fields.RGBColorField', [], {'max_length': '7'})
        },
        u'site.presentation': {
            'Meta': {'object_name': 'Presentation'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'presentations'", 'to': u"orm['site.Settings']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'presentation': ('filebrowser.fields.FileBrowseField', [], {'max_length': '255'})
        },
        u'site.settings': {
            'Meta': {'object_name': 'Settings'},
            'adv_form_text_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'adv_form_text_ru': ('ckeditor.fields.RichTextField', [], {'null': 'True'}),
            'children_cat_name_one_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'children_cat_name_one_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'children_cat_name_two_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'children_cat_name_two_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'children_shops_for_one': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'children_shops_for_ones'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['shops.Brand']"}),
            'children_shops_for_two': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'children_shops_for_twos'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['shops.Brand']"}),
            'email_advertisement': ('django.db.models.fields.EmailField', [], {'max_length': '255'}),
            'email_renter_request': ('django.db.models.fields.EmailField', [], {'max_length': '255'}),
            'enable_gallery': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'feedback_form_text_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'feedback_form_text_ru': ('ckeditor.fields.RichTextField', [], {'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'renter_form_text_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'renter_form_text_ru': ('ckeditor.fields.RichTextField', [], {'null': 'True'}),
            'slogan_children_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'slogan_children_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'slogan_cinema_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'slogan_cinema_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'slogan_entertainment_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'slogan_entertainment_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'slogan_five_color': ('colorful.fields.RGBColorField', [], {'max_length': '7'}),
            'slogan_five_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'slogan_five_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'slogan_four_color': ('colorful.fields.RGBColorField', [], {'max_length': '7'}),
            'slogan_four_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'slogan_four_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'slogan_one_color': ('colorful.fields.RGBColorField', [], {'max_length': '7'}),
            'slogan_one_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'slogan_one_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'slogan_three_color': ('colorful.fields.RGBColorField', [], {'max_length': '7'}),
            'slogan_three_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'slogan_three_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'slogan_two_color': ('colorful.fields.RGBColorField', [], {'max_length': '7'}),
            'slogan_two_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'slogan_two_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'title_brands_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'title_brands_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'title_child_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'title_child_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'title_entertainments_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'title_entertainments_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'title_events_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'title_events_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'title_restaurants_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'title_restaurants_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'})
        }
    }

    complete_apps = ['site']