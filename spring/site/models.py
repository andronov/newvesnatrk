# -*- coding: utf-8 -*-
from colorful.fields import RGBColorField
from ckeditor.fields import RichTextField
from django.db import models
from django.utils.translation import ugettext_lazy as _

from easymode.i18n.decorators import I18n
from filebrowser.fields import FileBrowseField
from spring.shops.models import Brand


@I18n('slogan_one', 'slogan_two', 'slogan_three', 'slogan_four', 'slogan_five', 'slogan_cinema', 'slogan_entertainment',
      'slogan_children', 'children_cat_name_one', 'children_cat_name_two', 'feedback_form_text',
      'title_entertainments', 'title_child', 'title_events', 'title_restaurants', 'title_brands', 'renter_form_text',
      'adv_form_text', 'address_footer')
class Settings(models.Model):
    include = ['presentations']
    # Слоганы на главной
    slogan_one = models.CharField(_('Slogan on index page. 1'), max_length=255, default='')
    slogan_one_color = RGBColorField()
    slogan_two = models.CharField(_('Slogan on index page. 2'), max_length=255, default='')
    slogan_two_color = RGBColorField()
    slogan_three = models.CharField(_('Slogan on index page. 3'), max_length=255, default='')
    slogan_three_color = RGBColorField()
    slogan_four = models.CharField(_('Slogan on index page. 4'), max_length=255, default='')
    slogan_four_color = RGBColorField()
    slogan_five = models.CharField(_('Slogan on index page. 5'), max_length=255, default='')
    slogan_five_color = RGBColorField()

    #Заголовки страниц
    title_entertainments = models.CharField(_('Title entertainments'), max_length=255, default='')
    title_child = models.CharField(_('Title child'), max_length=255, default='')
    title_events = models.CharField(_('Title events'), max_length=255, default='')
    title_restaurants = models.CharField(_('Title restaurants'), max_length=255, default='')
    title_brands = models.CharField(_('Title brands'), max_length=255, default='')

    #Галерея
    enable_gallery = models.BooleanField(_('Enable gallery'), default=False)

    #Emails for forms
    email_renter_request = models.EmailField(_('Email for renter request'), max_length=255)
    email_advertisement = models.EmailField(_('Email for advertisement'), max_length=255)

    #forms_texts
    feedback_form_text = RichTextField(_('Feedback form text'), null=True)
    renter_form_text = RichTextField(_('Renter form text'), null=True)
    adv_form_text = RichTextField(_('Advertisement form text'), null=True)

    slogan_cinema = models.CharField(_('Slogan on cinema page.'), max_length=255, default='')
    slogan_entertainment = models.CharField(_('Slogan on entertainment page.'), max_length=255, default='')
    slogan_children = models.CharField(_('Slogan on children page.'), max_length=255, default='')

    children_cat_name_one = models.CharField(_('Children category name. 1'), max_length=255, default='')
    children_cat_name_two = models.CharField(_('Children category name. 1'), max_length=255, default='')

    children_shops_for_one = models.ManyToManyField(
        Brand,
        verbose_name=_('Shops in children page for first category.'),
        blank=True,
        null=True,
        related_name='children_shops_for_ones'
    )
    children_shops_for_two = models.ManyToManyField(
        Brand,
        verbose_name=_('Shops in children page for second category.'),
        blank=True,
        null=True,
        related_name='children_shops_for_twos'
    )

    address_footer = models.CharField(_('TRC address'), max_length=255)

    class Meta:
        verbose_name = _('Settings')
        verbose_name_plural = _('Settings')

    def __unicode__(self):
        return _('Settings')

    @property
    def get_shops_for_one(self):
        return [i for i in self.children_shops_for_one.all().order_by('?')[0:3]]

    @property
    def get_shops_for_two(self):
        return [i for i in self.children_shops_for_two.all().order_by('?')[0:3]]


class Presentation(models.Model):
    exclude = ['category']

    name = models.CharField(_('Name'), max_length=255, null=True)
    presentation = FileBrowseField(
        verbose_name=_('Presentation'),
        directory='about/presentation/',
        max_length=255,
        extensions=['.pdf', '.ppt', '.jpg', '.png']
    )
    category = models.ForeignKey(Settings, verbose_name=_('Settings module'), related_name='presentations')