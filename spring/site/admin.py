# -*- coding: utf-8 -*-
from django.contrib import admin
from spring.site.models import Settings, Presentation


class PresentationsInline(admin.TabularInline):
    model = Presentation


class SettingsAdmin(admin.ModelAdmin):
    filter_horizontal = ('children_shops_for_one', 'children_shops_for_two')
    inlines = (PresentationsInline,)

    def has_add_permission(self, request):
        return False

admin.site.register(Settings, SettingsAdmin)