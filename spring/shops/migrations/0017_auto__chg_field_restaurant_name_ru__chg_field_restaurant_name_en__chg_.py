# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Restaurant.name_ru'
        db.alter_column(u'shops_restaurant', 'name_ru', self.gf('django.db.models.fields.CharField')(default=1, max_length=254))

        # Changing field 'Restaurant.name_en'
        db.alter_column(u'shops_restaurant', 'name_en', self.gf('django.db.models.fields.CharField')(default=1, max_length=254))

        # Changing field 'Brand.name_ru'
        db.alter_column(u'shops_brand', 'name_ru', self.gf('django.db.models.fields.CharField')(default=1, max_length=254))

        # Changing field 'Brand.name_en'
        db.alter_column(u'shops_brand', 'name_en', self.gf('django.db.models.fields.CharField')(default=1, max_length=254))

    def backwards(self, orm):

        # Changing field 'Restaurant.name_ru'
        db.alter_column(u'shops_restaurant', 'name_ru', self.gf('django.db.models.fields.CharField')(max_length=254, null=True))

        # Changing field 'Restaurant.name_en'
        db.alter_column(u'shops_restaurant', 'name_en', self.gf('django.db.models.fields.CharField')(max_length=254, null=True))

        # Changing field 'Brand.name_ru'
        db.alter_column(u'shops_brand', 'name_ru', self.gf('django.db.models.fields.CharField')(max_length=254, null=True))

        # Changing field 'Brand.name_en'
        db.alter_column(u'shops_brand', 'name_en', self.gf('django.db.models.fields.CharField')(max_length=254, null=True))

    models = {
        u'shops.brand': {
            'Meta': {'ordering': "['place_number', '-pk']", 'object_name': 'Brand'},
            'categories': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'brands'", 'symmetrical': 'False', 'to': u"orm['shops.BrandCategory']"}),
            'color': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'brands'", 'null': 'True', 'to': u"orm['shops.Color']"}),
            'contacts': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'description_en': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'description_ru': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo_en': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'logo_ru': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'number': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '256'}),
            'open_soon': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'place_number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1000'}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'url': ('spring.shops.fields.NullableURLField', [], {'default': 'None', 'max_length': '128', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'views_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'work_time': ('django.db.models.fields.CharField', [], {'default': "'10:00 - 19:00'", 'max_length': '13'})
        },
        u'shops.brandcategory': {
            'Meta': {'ordering': "('position',)", 'object_name': 'BrandCategory'},
            'banner': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'banner_url': ('django.db.models.fields.URLField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'for_children': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'}),
            'position': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        },
        u'shops.brandimage': {
            'Meta': {'object_name': 'BrandImage'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'images'", 'to': u"orm['shops.Brand']"}),
            'source': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'})
        },
        u'shops.color': {
            'Meta': {'object_name': 'Color'},
            'circle_color': ('colorful.fields.RGBColorField', [], {'max_length': '7'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'promotion_color': ('colorful.fields.RGBColorField', [], {'max_length': '7'})
        },
        u'shops.restaurant': {
            'Meta': {'ordering': "['place_number', '-pk']", 'object_name': 'Restaurant'},
            'categories': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'restaurants'", 'symmetrical': 'False', 'to': u"orm['shops.RestaurantCategory']"}),
            'color': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'restaurants'", 'null': 'True', 'to': u"orm['shops.Color']"}),
            'contacts': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'description_en': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'description_ru': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo_en': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'logo_ru': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'number': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '256'}),
            'open_soon': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'place_number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1000'}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'url': ('spring.shops.fields.NullableURLField', [], {'default': 'None', 'max_length': '128', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'views_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'work_time': ('django.db.models.fields.CharField', [], {'default': "'10:00 - 19:00'", 'max_length': '13'})
        },
        u'shops.restaurantcategory': {
            'Meta': {'ordering': "('position',)", 'object_name': 'RestaurantCategory'},
            'banner': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'banner_url': ('django.db.models.fields.URLField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'}),
            'position': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        },
        u'shops.restaurantimage': {
            'Meta': {'object_name': 'RestaurantImage'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'images'", 'to': u"orm['shops.Restaurant']"}),
            'source': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['shops']