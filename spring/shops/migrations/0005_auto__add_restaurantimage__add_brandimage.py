# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'RestaurantImage'
        db.create_table(u'shops_restaurantimage', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('source', self.gf('filebrowser.fields.FileBrowseField')(default='', max_length=254, null=True, blank=True)),
            ('shop', self.gf('django.db.models.fields.related.ForeignKey')(related_name='images', to=orm['shops.Restaurant'])),
        ))
        db.send_create_signal(u'shops', ['RestaurantImage'])

        # Adding model 'BrandImage'
        db.create_table(u'shops_brandimage', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('source', self.gf('filebrowser.fields.FileBrowseField')(default='', max_length=254, null=True, blank=True)),
            ('shop', self.gf('django.db.models.fields.related.ForeignKey')(related_name='images', to=orm['shops.Brand'])),
        ))
        db.send_create_signal(u'shops', ['BrandImage'])


    def backwards(self, orm):
        # Deleting model 'RestaurantImage'
        db.delete_table(u'shops_restaurantimage')

        # Deleting model 'BrandImage'
        db.delete_table(u'shops_brandimage')


    models = {
        u'shops.brand': {
            'Meta': {'ordering': "['place_number']", 'object_name': 'Brand'},
            'category': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'brands'", 'symmetrical': 'False', 'to': u"orm['shops.BrandCategory']"}),
            'color': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'brands'", 'null': 'True', 'to': u"orm['shops.Color']"}),
            'contacts': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '128'}),
            'description_en': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'description_ru': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo_en': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'logo_ru': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'}),
            'number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'place_number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1000'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '128'}),
            'views_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        },
        u'shops.brandcategory': {
            'Meta': {'object_name': 'BrandCategory'},
            'for_children': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'})
        },
        u'shops.brandimage': {
            'Meta': {'object_name': 'BrandImage'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'images'", 'to': u"orm['shops.Brand']"}),
            'source': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'})
        },
        u'shops.color': {
            'Meta': {'object_name': 'Color'},
            'circle_color': ('colorful.fields.RGBColorField', [], {'max_length': '7'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'promotion_color': ('colorful.fields.RGBColorField', [], {'max_length': '7'})
        },
        u'shops.restaurant': {
            'Meta': {'ordering': "['place_number']", 'object_name': 'Restaurant'},
            'category': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'restaurants'", 'symmetrical': 'False', 'to': u"orm['shops.RestaurantCategory']"}),
            'color': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'restaurants'", 'null': 'True', 'to': u"orm['shops.Color']"}),
            'contacts': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '128'}),
            'description_en': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'description_ru': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo_en': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'logo_ru': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'}),
            'number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'place_number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1000'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '128'}),
            'views_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        },
        u'shops.restaurantcategory': {
            'Meta': {'object_name': 'RestaurantCategory'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'})
        },
        u'shops.restaurantimage': {
            'Meta': {'object_name': 'RestaurantImage'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'images'", 'to': u"orm['shops.Restaurant']"}),
            'source': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['shops']