# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'BrandCategory'
        db.create_table(u'shops_brandcategory', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=254, null=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=254, null=True, blank=True)),
        ))
        db.send_create_signal(u'shops', ['BrandCategory'])

        # Adding model 'RestaurantCategory'
        db.create_table(u'shops_restaurantcategory', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=254, null=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=254, null=True, blank=True)),
        ))
        db.send_create_signal(u'shops', ['RestaurantCategory'])

        # Adding model 'Restaurant'
        db.create_table(u'shops_restaurant', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=254, null=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=254, null=True, blank=True)),
            ('logo_ru', self.gf('filebrowser.fields.FileBrowseField')(default='', max_length=254, null=True, blank=True)),
            ('logo_en', self.gf('filebrowser.fields.FileBrowseField')(default='', max_length=254, null=True, blank=True)),
            ('contacts', self.gf('django.db.models.fields.CharField')(default='', max_length=128)),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=128)),
            ('description_ru', self.gf('ckeditor.fields.RichTextField')(max_length=1000, null=True)),
            ('description_en', self.gf('ckeditor.fields.RichTextField')(max_length=1000, null=True, blank=True)),
            ('views_count', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
            ('number', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
        ))
        db.send_create_signal(u'shops', ['Restaurant'])

        # Adding M2M table for field category on 'Restaurant'
        m2m_table_name = db.shorten_name(u'shops_restaurant_category')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('restaurant', models.ForeignKey(orm[u'shops.restaurant'], null=False)),
            ('restaurantcategory', models.ForeignKey(orm[u'shops.restaurantcategory'], null=False))
        ))
        db.create_unique(m2m_table_name, ['restaurant_id', 'restaurantcategory_id'])

        # Adding model 'Brand'
        db.create_table(u'shops_brand', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=254, null=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=254, null=True, blank=True)),
            ('logo_ru', self.gf('filebrowser.fields.FileBrowseField')(default='', max_length=254, null=True, blank=True)),
            ('logo_en', self.gf('filebrowser.fields.FileBrowseField')(default='', max_length=254, null=True, blank=True)),
            ('contacts', self.gf('django.db.models.fields.CharField')(default='', max_length=128)),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=128)),
            ('description_ru', self.gf('ckeditor.fields.RichTextField')(max_length=1000, null=True)),
            ('description_en', self.gf('ckeditor.fields.RichTextField')(max_length=1000, null=True, blank=True)),
            ('views_count', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
            ('number', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
        ))
        db.send_create_signal(u'shops', ['Brand'])

        # Adding M2M table for field category on 'Brand'
        m2m_table_name = db.shorten_name(u'shops_brand_category')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('brand', models.ForeignKey(orm[u'shops.brand'], null=False)),
            ('brandcategory', models.ForeignKey(orm[u'shops.brandcategory'], null=False))
        ))
        db.create_unique(m2m_table_name, ['brand_id', 'brandcategory_id'])


    def backwards(self, orm):
        # Deleting model 'BrandCategory'
        db.delete_table(u'shops_brandcategory')

        # Deleting model 'RestaurantCategory'
        db.delete_table(u'shops_restaurantcategory')

        # Deleting model 'Restaurant'
        db.delete_table(u'shops_restaurant')

        # Removing M2M table for field category on 'Restaurant'
        db.delete_table(db.shorten_name(u'shops_restaurant_category'))

        # Deleting model 'Brand'
        db.delete_table(u'shops_brand')

        # Removing M2M table for field category on 'Brand'
        db.delete_table(db.shorten_name(u'shops_brand_category'))


    models = {
        u'shops.brand': {
            'Meta': {'object_name': 'Brand'},
            'category': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'brands'", 'symmetrical': 'False', 'to': u"orm['shops.BrandCategory']"}),
            'contacts': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '128'}),
            'description_en': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'description_ru': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo_en': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'logo_ru': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'}),
            'number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '128'}),
            'views_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        },
        u'shops.brandcategory': {
            'Meta': {'object_name': 'BrandCategory'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'})
        },
        u'shops.restaurant': {
            'Meta': {'object_name': 'Restaurant'},
            'category': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'restaurants'", 'symmetrical': 'False', 'to': u"orm['shops.RestaurantCategory']"}),
            'contacts': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '128'}),
            'description_en': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'description_ru': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo_en': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'logo_ru': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'}),
            'number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '128'}),
            'views_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        },
        u'shops.restaurantcategory': {
            'Meta': {'object_name': 'RestaurantCategory'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'})
        }
    }

    complete_apps = ['shops']