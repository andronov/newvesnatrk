# -*- coding: utf-8 -*-
import datetime

from colorful.fields import RGBColorField
from django.forms.models import model_to_dict
from django.core.validators import MinLengthValidator
from django.db import models
from django.utils.functional import cached_property
from django.utils.translation import ugettext_lazy as _, get_language
from easymode.i18n.decorators import I18n
from filebrowser.fields import FileBrowseField
from ckeditor.fields import RichTextField

from spring.shops.fields import NullableURLField
from utilities.capable.models import Orderable


@I18n('name')
class Category(Orderable):
    name = models.CharField(_('Name'), max_length=254)
    banner = FileBrowseField(
        verbose_name=_('Banner (690x150)'),
        directory='brand/cat_banners/',
        max_length=254,
        null=True,
        default='',
        blank=True)
    iphone_banner = FileBrowseField(
        verbose_name=_('IPhone banner (310x150)'),
        directory='brand/cat_banners/iphone/',
        max_length=254,
        null=True,
        default='',
        blank=True)
    banner_url = models.URLField(_('Banner URL'), max_length=255, null=True, blank=True)

    def __unicode__(self):
        return self.name

    @property
    def banner_310x150(self):
        try:
            return unicode(self.iphone_banner.version_generate('310x150').url)
        except IOError:
            return ''

    @property
    def banner_690x150(self):
        try:
            return unicode(self.banner.version_generate('690x150').url)
        except IOError:
            return ''

    class Meta(Orderable.Meta):
        abstract = True


class BrandCategory(Category):
    for_children = models.BooleanField(_('For children'), default=False)

    class Meta(Category.Meta):
        verbose_name = _('Brand category')
        verbose_name_plural = _('Brand categories')


class RestaurantCategory(Category):
    class Meta(Category.Meta):
        verbose_name = _('Restaurant category')
        verbose_name_plural = _('Restaurant categories')


class Color(models.Model):
    name = models.CharField(_('Color name'), max_length=254)
    circle_color = RGBColorField()
    promotion_color = RGBColorField()

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('Color')
        verbose_name_plural = _('Colors')


class ModelDiffMixin(object):
    """
    A model mixin that tracks model fields' values and provide some useful api
    to know what fields have been changed.
    """

    def __init__(self, *args, **kwargs):
        super(ModelDiffMixin, self).__init__(*args, **kwargs)
        self.__initial = self._dict

    @property
    def diff(self):
        d1 = self.__initial
        d2 = self._dict
        diffs = [(k, (v, d2[k])) for k, v in d1.items() if v != d2[k] and getattr(v, 'path', '') != getattr(d2[k], 'path', '1')]
        return dict(diffs)

    @property
    def has_changed(self):
        return bool(self.diff)

    @property
    def changed_fields(self):
        return self.diff.keys()

    def get_field_diff(self, field_name):
        """
        Returns a diff for field if it's changed and None otherwise.
        """
        return self.diff.get(field_name, None)

    def save(self, *args, **kwargs):
        """
        Saves model and set initial state.
        """
        super(ModelDiffMixin, self).save(*args, **kwargs)
        self.__initial = self._dict

    @property
    def _dict(self):
        return model_to_dict(self, fields=[field.name for field in
                             self._meta.fields])


@I18n('logo', 'description')
class Shop(ModelDiffMixin, models.Model):
    name_ru = models.CharField(_('Name') + ' (ru)', max_length=254)
    name_en = models.CharField(_('Name') + ' (en)', max_length=254)
    contacts = models.CharField(_('Contacts'), max_length=128, default='', null=True, blank=True)
    url = NullableURLField(_('URL'), unique=True, max_length=128, null=True, blank=True, default=None)
    description = RichTextField(_('Description'), config_name='big', max_length=1000, validators=[MinLengthValidator(200)])
    views_count = models.PositiveIntegerField(_('Views count'), default=0)
    number = models.CharField(_('Pavilion number'), max_length=256, default='')
    place_number = models.PositiveSmallIntegerField(_('Place number'), default=1000, db_index=True)
    color = models.ForeignKey(Color, verbose_name=_('Shop color'), related_name='%(class)ss', null=True, blank=True)
    work_time = models.CharField(_('Work time'), max_length=13, default='10:00 - 19:00')
    published = models.BooleanField(_('Publish'), default=False, db_index=True)
    filter_apply = models.BooleanField(_('Apply filter'), default=True)
    open_soon = models.BooleanField(_('Opening soon'), default=False)
    show_on_main = models.BooleanField(_('Show on main'), default=False)
    logo = FileBrowseField(
        verbose_name=_('Logo'),
        directory='brand/logo/',
        max_length=254,
        format='image',
        extensions=['.jpg', '.jpeg', '.gif', '.png', '.tif', '.tiff'],
        null=True,
        default='',
        blank=True
    )

    @property
    def discount_promotion(self):
        today = datetime.date.today()
        try:
            return self.baseevents.filter(
                promotion__discount__gt=0,
                pub_date__lte=today,
                end_date__gte=today,
                is_published=True
            )[0]
        except IndexError:
            return None

    @property
    def logo130(self):
        try:
            logo = getattr(self, 'logo_{}'.format(get_language()))
            return logo.version_generate('130x130').url
        except Exception:
            return None

    @cached_property
    def is_spring(self):
        return self.name == u'Весна'

    def __unicode__(self):
        return getattr(self, 'name_{}'.format(get_language()))

    @staticmethod
    def autocomplete_search_fields():
        return ('id__iexact', 'name_ru__icontains', 'name_en__icontains',)

    class Meta:
        abstract = True


class Restaurant(Shop):
    categories = models.ManyToManyField(RestaurantCategory, verbose_name=_('Category'), related_name='restaurants')

    @property
    def type(self):
        return 'restaurant'

    class Meta:
        ordering = ['place_number', ]
        verbose_name = _('Restaurant')
        verbose_name_plural = _('Restaurants')


class Brand(Shop):
    categories = models.ManyToManyField(BrandCategory, verbose_name=_('Category'), related_name='brands')

    @property
    def type(self):
        return 'brand'

    class Meta:
        ordering = ['place_number', ]
        verbose_name = _('Brand')
        verbose_name_plural = _('Brands')


class BrandImage(models.Model):
    source = FileBrowseField(
        verbose_name=_('Image'),
        directory='brand/images/',
        max_length=254,
        null=True,
        default='',
        blank=True)
    shop = models.ForeignKey(Brand, verbose_name=_('Brand image'), related_name='images')

    def __unicode__(self):
        return self.source.name

    class Meta:
        verbose_name_plural = _('Brand image')
        verbose_name = _('Brand images')


class RestaurantImage(models.Model):
    source = FileBrowseField(
        verbose_name=_('Image'),
        directory='brand/images/',
        max_length=254,
        null=True,
        default='',
        blank=True)
    shop = models.ForeignKey(Restaurant, verbose_name=_('Restaurant image'), related_name='images')

    def __unicode__(self):
        return self.source.name

    class Meta:
        verbose_name_plural = _('Restaurant image')
        verbose_name = _('Restaurant images')


class BrandSynonym(models.Model):
    synonym = models.CharField(_('Synonym'), max_length=254)
    shop = models.ForeignKey(Brand, verbose_name=_('Brand synonym'), related_name='synonyms')

    def __unicode__(self):
        return self.synonym

    class Meta:
        verbose_name_plural = _('Brand search synonym')
        verbose_name = _('Brand search synonyms')


class RestaurantSynonym(models.Model):
    synonym = models.CharField(_('Synonym'), max_length=254)
    shop = models.ForeignKey(Restaurant, verbose_name=_('Restaurant synonym'), related_name='synonyms')

    def __unicode__(self):
        return self.synonym

    class Meta:
        verbose_name_plural = _('Restaurant search synonym')
        verbose_name = _('Restaurant search synonyms')