from django.conf.urls import patterns, url


urlpatterns = patterns(
    'spring.shops.views',
    url(r'^brands/$', 'brand_list'),
    url(r'^restaurants/$', 'restaurant_list'),
    url(r'^brand/(?P<pk>\d+)/$', 'brand_detail'),
    url(r'^restaurant/(?P<pk>\d+)/$', 'restaurant_detail'),
)
