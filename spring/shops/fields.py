from django.db import models
from south.modelsinspector import add_introspection_rules


class NullableURLField(models.URLField):
    description = "URLField that stores NULL but returns ''"
    __metaclass__ = models.SubfieldBase

    def to_python(self, value):
        if isinstance(value, models.URLField):
            return value
        return value or ''

    def get_prep_value(self, value):
        return value or None

add_introspection_rules([], ["^spring\.shops\.fields\.NullableURLField"])
