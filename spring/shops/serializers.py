from __future__ import absolute_import
from django.utils.translation import get_language
from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import ModelSerializer
from spring.shops.models import Brand, Restaurant, BrandCategory, RestaurantCategory, Color, BrandImage, RestaurantImage
from utilities.fields import SerializerLocaleField


class BrandImageSerializer(ModelSerializer):
    source = SerializerMethodField('get_source')

    def get_source(self, obj):
        try:
            return getattr(obj, 'source').url
        except (AttributeError, KeyError, ValueError):
            return ''

    class Meta:
        model = BrandImage
        fields = ('source', 'id')


class RestaurantImageSerializer(BrandImageSerializer):
    class Meta(BrandImageSerializer.Meta):
        model = RestaurantImage


class BrandCategorySerializer(ModelSerializer):
    name = SerializerLocaleField()
    banner = SerializerMethodField('get_banner')
    iphone_banner = SerializerMethodField('get_iphone_banner')

    def get_banner(self, obj):
        try:
            return obj.banner.url
        except AttributeError:
            return ''

    def get_iphone_banner(self, obj):
        try:
            return obj.iphone_banner.url
        except AttributeError:
            return ''

    class Meta:
        model = BrandCategory
        fields = ('banner', 'banner_url', 'id', 'iphone_banner', 'name', 'position')


class RestaurantCategorySerializer(BrandCategorySerializer):
    class Meta(BrandCategorySerializer.Meta):
        model = RestaurantCategory


class ColorSerializer(ModelSerializer):
    class Meta:
        model = Color


class BrandInEventsSerializer(ModelSerializer):
    description = SerializerLocaleField()
    logo = SerializerMethodField('get_logo')
    name = SerializerLocaleField()
    categories = BrandCategorySerializer()
    color = ColorSerializer()
    type = SerializerMethodField('get_type')

    def get_logo(self, obj):
        try:
            logo_image = getattr(obj, 'logo_{}'.format(get_language()))
            if logo_image.height <= logo_image.width > 100:
                return logo_image.version_generate('brand_logo_width').url
            elif logo_image.width <= logo_image.height > 100:
                return logo_image.version_generate('brand_logo_height').url
            else:
                return logo_image.url
        except (AttributeError, KeyError, ValueError, IOError):
            return ''

    def get_type(self, obj):
        return obj.type

    class Meta:
        model = Brand
        fields = (
            'id', 'description', 'filter_apply', 'contacts', 'logo', 'name', 'number', 'open_soon', 'place_number',
            'published', 'show_on_main', 'url', 'views_count', 'work_time', 'categories', 'type', 'color'
        )


class RestaurantInEventSerializer(BrandInEventsSerializer):
    categories = RestaurantCategorySerializer()

    class Meta(BrandInEventsSerializer.Meta):
        model = Restaurant


class BrandSerializer(BrandInEventsSerializer):
    discount_promotion = SerializerMethodField('get_discount_promotion')
    logo130 = SerializerMethodField('get_logo130')
    images = BrandImageSerializer(many=True)

    def get_discount_promotion(self, obj):
        from spring.events.serializers import EventInBrandsSerializer
        if obj.discount_promotion is not None:
            return EventInBrandsSerializer(obj.discount_promotion).data
        return None

    def get_logo130(self, obj):
        return getattr(obj, 'logo130')

    class Meta(BrandInEventsSerializer.Meta):
        fields = BrandInEventsSerializer.Meta.fields + ('logo130', 'images', 'discount_promotion')
        exclude = ('place_number', 'shop', 'pub_date')


class RestaurantSerializer(BrandSerializer):
    images = RestaurantImageSerializer(many=True)

    class Meta(BrandSerializer.Meta):
        model = Restaurant


class RestaurantToMenuSerializer(RestaurantSerializer):
    class Meta(RestaurantSerializer.Meta):
        fields = ('id', 'name', 'open_soon')


class BrandCategoryToMenuSerializer(BrandCategorySerializer):
    class Meta(BrandCategorySerializer.Meta):
        fields = ('id', 'name')


class BrandToMenuSerializer(BrandSerializer):
    categories = BrandCategoryToMenuSerializer()

    class Meta(BrandSerializer.Meta):
        fields = ('categories', 'id', 'name', 'open_soon', 'place_number')
        exclude = ()


class BrandToContentSerializer(BrandSerializer):
    class Meta(BrandSerializer.Meta):
        fields = ('color', 'logo130', 'id', 'name', 'open_soon')
