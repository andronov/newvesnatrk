# -*- coding: utf-8 -*-
from __future__ import division
import datetime

from django.utils.translation import get_language
from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.response import Response
from django.db.models import Q
from pytils.translit import slugify

from spring.shops import models as category_models
from spring.shops import serializers as model_serializers
from spring.events.models import BaseEvent
from spring.events.serializers import EventInBrandsSerializer
from spring.shops.models import Brand, Restaurant


class BrandSplitList(ListAPIView):
    model = Brand

    def __init__(self, *args, **kwargs):
        super(BrandSplitList, self).__init__(*args, **kwargs)
        self.object_list = None
        self.serializer_class = getattr(model_serializers, '%sSerializer' % self.model.__name__)

    def list(self, request, *args, **kwargs):
        self.object_list = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(self.object_list, many=True)

        #EVENT_LIST
        event_list = BaseEvent.objects.none()
        page = self.request.GET.get('page', None)
        if page:
            per_page = int(self.request.GET.get('per_page', '12'))
            filter_kwargs = {
                'pub_date__lte': datetime.date.today(),
                'end_date__gte': datetime.date.today(),
                'is_published': True,
            }

            offset = (int(page) - 1) * (abs(int(per_page / 4)) + 1)
            limit = offset + abs(int(per_page / 4))

            event_list = BaseEvent.objects.filter(
                Q(
                    Q(**filter_kwargs) & Q(springevent=None)
                ) | Q(
                    Q(**filter_kwargs) & Q(springevent__background='')
                )
            ).exclude(
                springevent__is_cycled=True
            )[offset:limit]
        #CATEGORY_LIST
        category_cls = getattr(category_models, '%sCategory' % self.model.__name__)
        category_serializer = getattr(model_serializers, '%sCategorySerializer' % self.model.__name__)
        #CHAR_LIST
        char_list = []
        for item in self.object_list:
            word = getattr(item, 'name_{}'.format(get_language()), None)
            if word and slugify(word):
                char_list.append(word[0].lower())

        return Response({
            'object_list': serializer.data,
            'event_list': [EventInBrandsSerializer(item).data for item in event_list],
            'category_list': [category_serializer(item).data for item in category_cls.objects.all()],
            'char_list': set(char_list)
        })

    def get_queryset(self):
        qs = super(BrandSplitList, self).get_queryset().filter(published=True)
        #BY_CATEGORY
        category = self.request.GET.get('cat_pk', None)
        if category:
            qs = qs.filter(categories__pk__in=[int(category)])

        #SEARCH_BLOCK
        search_string = self.request.GET.get('isearch', None)
        if search_string:
            if slugify(search_string):
                qs = qs.filter(
                    Q(name_ru__icontains=search_string) |
                    Q(name_en__icontains=search_string) |
                    Q(name_en__icontains=slugify(search_string)) |
                    Q(name_en__icontains=slugify(search_string))
                )
            else:
                qs = qs.filter(
                    Q(name_ru__icontains=search_string) |
                    Q(name_en__icontains=search_string)
                )

        #PAGINATE_BLOCK
        page = self.request.GET.get('page', None)
        if page:
            per_page = int(self.request.GET.get('per_page', '12'))
            offset = (int(page) - 1) * (per_page + 1)
            limit = offset + per_page
            qs = qs[offset:limit]
        return qs
brand_list = BrandSplitList.as_view()
restaurant_list = BrandSplitList.as_view(model=Restaurant)


class BrandDetail(RetrieveAPIView):
    model = Brand

    def __init__(self, *args, **kwargs):
        super(BrandDetail, self).__init__(*args, **kwargs)
        self.object = None
        self.serializer_class = getattr(model_serializers, '%sSerializer' % self.model.__name__)

    def retrieve(self, request, *args, **kwargs):
        self.object = self.get_object()
        serializer = self.get_serializer(self.object)
        return Response({
            'object': serializer.data,
            'event_list': [
                EventInBrandsSerializer(item).data for item in self.object.baseevents.filter(
                    is_published=True,
                    pub_date__lte=datetime.date.today(),
                    end_date__gte=datetime.date.today()
                )
            ]
        })
brand_detail = BrandDetail.as_view()
restaurant_detail = BrandDetail.as_view(model=Restaurant)
