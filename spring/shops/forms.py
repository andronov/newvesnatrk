# -*- coding: utf-8 -*-
from django import forms
from django.core.exceptions import ValidationError
from spring.shops.models import Brand, Restaurant
from django.utils.translation import ugettext_lazy as _, get_language


class BrandAdminForm(forms.ModelForm):

    def clean_number(self):
        shop = self.Meta.model.objects.filter(number=self.cleaned_data['number'])
        if shop.exists() and not self.instance == shop[0]:
            shop = shop[0]
            raise ValidationError(_('Pavilion number already exists by "' + getattr(shop, 'name_' + get_language()) + '"'))
        return self.cleaned_data['number']

    class Meta:
        model = Brand


class RestaurantAdminForm(BrandAdminForm):

    class Meta:
        model = Restaurant