# -*- coding: utf-8 -*-
from django.contrib import admin
from spring.shops.forms import RestaurantAdminForm, BrandAdminForm
from spring.shops.models import Brand, Restaurant, BrandCategory, RestaurantCategory, Color, BrandImage, RestaurantImage, BrandSynonym, RestaurantSynonym
from utilities.capable.admin import OrderableAdmin


class BrandImageInline(admin.TabularInline):
    model = BrandImage


class BrandSynonymInline(admin.TabularInline):
    model = BrandSynonym


class RestaurantSynonymInline(admin.TabularInline):
    model = RestaurantSynonym


class RestaurantImageInline(admin.TabularInline):
    model = RestaurantImage


class BrandAdmin(admin.ModelAdmin):
    form = BrandAdminForm
    inlines = (BrandImageInline, BrandSynonymInline)
    list_filter = ('name_ru', 'name_en', 'published', 'open_soon', 'categories', 'show_on_main')
    list_display = ('id', 'name_ru', 'name_en', 'published', 'open_soon', 'filter_apply', 'show_on_main')
    search_fields = ('name_ru', 'name_en')
    list_editable = ('published', 'open_soon', 'filter_apply', 'show_on_main')



class RestaurantAdmin(admin.ModelAdmin):
    form = RestaurantAdminForm
    inlines = (RestaurantImageInline, RestaurantSynonymInline)
    list_filter = ('name_ru', 'name_en', 'published', 'open_soon', 'categories')
    list_display = ('id', 'name_ru', 'name_en', 'published', 'open_soon', 'filter_apply')
    search_fields = ('name_ru', 'name_en')
    list_editable = ('published', 'open_soon', 'filter_apply')


class BrandCategoryAdmin(OrderableAdmin):
    list_display_links = ('name',)
    list_display = ('name',) + OrderableAdmin.list_display


class RestaurantCategoryAdmin(OrderableAdmin):
    list_display_links = ('name',)
    list_display = ('name',) + OrderableAdmin.list_display

admin.site.register(BrandCategory, BrandCategoryAdmin)
admin.site.register(RestaurantCategory, RestaurantCategoryAdmin)
admin.site.register(Brand, BrandAdmin)
admin.site.register(Restaurant, RestaurantAdmin)
admin.site.register(Color)
