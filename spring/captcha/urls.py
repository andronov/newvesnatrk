from django.conf.urls import patterns, url


urlpatterns = patterns(
    'spring.captcha.views',
    url(r'^image/$', 'captcha')
)
