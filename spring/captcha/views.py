from captcha.models import CaptchaStore
from django.core.urlresolvers import reverse
from rest_framework.response import Response
from rest_framework.views import APIView


class CaptchaImageView(APIView):
    def get(self, request):
        new_key = CaptchaStore.generate_key()
        return Response({
            'object': {
                'key': new_key,
                'image_url': reverse('captcha-image-2x', args=[new_key]),
            }
        })
captcha = CaptchaImageView.as_view()
