# -*- coding: utf-8 -*-

from datetime import datetime
from django.core.urlresolvers import reverse
from social.backends.vk import vk_api, VKOAuth2


class Vkontakte(object):
    def __init__(self, user):
        self.userqs = user.social_auth.filter(provider='vk-oauth2')

    def api(self, method, data):
        self.backend = self.userqs.get().get_backend_instance()
        return vk_api(self.backend, method, data)

    def is_auth(self):
        is_auth = self.userqs.exists()
        # if is_auth:
        #     installed = self.api('isAppUser', {'uid': self.userqs.get().uid})
        #     assert False
        #     if not installed:
        #         return False
        return is_auth

    def user_in_group(self):
        group_id = 'vesnatrk'
        uig = 0
        if self.userqs.exists():
            uig = self.api('groups.isMember', {'gid': group_id, 'uid': self.userqs.get().uid}).get('response', 0)
            # assert False
        return bool(uig)
