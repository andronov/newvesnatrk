# -*- coding: utf-8 -*-

import urllib2
from datetime import datetime
from json import loads
from django.core.urlresolvers import reverse


class Facebook(object):
    def __init__(self, user):
        self.userqs = user.social_auth.filter(provider='facebook')

    def is_auth(self):
        return self.userqs.exists()

    def user_in_group(self, fields=None):
        if not self.userqs.exists():
            return False
        self.user = self.userqs.get()
        self.backend = self.user.get_backend_instance()
        self.access_token = self.user.extra_data['access_token']
        try:
            result = loads(urllib2.urlopen('https://graph.facebook.com/{}/likes/523998781053048/?access_token={}'.format(
                self.user.uid,
                self.access_token,
            )).read())
        except urllib2.HTTPError as e:
            return False

        if 'data' in result and result['data']:
            return True

        return False

