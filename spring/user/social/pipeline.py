from django.contrib.auth import logout


def admin_check(strategy, user=None, *args, **kwargs):
    from django.conf import settings
    if user is not None and not settings.SOCIAL_AUTH_MERGE_ACCOUNTS:
        from django.http import HttpResponseRedirect
        from django.contrib.auth.views import logout
        from django.core.urlresolvers import reverse

        login_page = reverse('social:begin', kwargs={'backend': strategy.backend.name})
        next_parameter = strategy.session_get('next')
        if next_parameter:
            response = logout(kwargs['request'], next_page='{}?next={}'.format(login_page, next_parameter))
        else:
            response = logout(kwargs['request'], next_page=login_page)

        if isinstance(response, HttpResponseRedirect):
            return response

    return None


def social_user(strategy, uid, user=None, *args, **kwargs):
    provider = strategy.backend.name
    social = strategy.storage.user.get_social_auth(provider, uid)
    if social:
        if user and social.user != user:
            logout(strategy.request)
            user = social.user
        elif not user:
            user = social.user
    return {'social': social,
            'user': user,
            'is_new': user is None,
            'new_association': False}
