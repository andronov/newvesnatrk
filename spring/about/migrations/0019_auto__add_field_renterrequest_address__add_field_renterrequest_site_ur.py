# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'RenterRequest.address'
        db.add_column(u'about_renterrequest', 'address',
                      self.gf('django.db.models.fields.CharField')(default=None, max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'RenterRequest.site_url'
        db.add_column(u'about_renterrequest', 'site_url',
                      self.gf('django.db.models.fields.URLField')(default=None, max_length=200, null=True, blank=True),
                      keep_default=False)

        # Adding field 'RenterRequest.position_person'
        db.add_column(u'about_renterrequest', 'position_person',
                      self.gf('django.db.models.fields.CharField')(default=None, max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'RenterRequest.fax'
        db.add_column(u'about_renterrequest', 'fax',
                      self.gf('django.db.models.fields.CharField')(default=None, max_length=12, null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'RenterRequest.address'
        db.delete_column(u'about_renterrequest', 'address')

        # Deleting field 'RenterRequest.site_url'
        db.delete_column(u'about_renterrequest', 'site_url')

        # Deleting field 'RenterRequest.position_person'
        db.delete_column(u'about_renterrequest', 'position_person')

        # Deleting field 'RenterRequest.fax'
        db.delete_column(u'about_renterrequest', 'fax')


    models = {
        u'about.adchannel': {
            'Meta': {'object_name': 'AdChannel'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'})
        },
        u'about.adrequest': {
            'Meta': {'object_name': 'AdRequest'},
            'channels': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'ad_requests'", 'symmetrical': 'False', 'to': u"orm['about.AdChannel']"}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '254'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '254', 'blank': 'True'}),
            'message': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '1000', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '12', 'blank': 'True'})
        },
        u'about.banner': {
            'Meta': {'object_name': 'Banner'},
            'banner_en': ('filebrowser.fields.FileBrowseField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'banner_ru': ('filebrowser.fields.FileBrowseField', [], {'max_length': '255', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'about.contactuscategory': {
            'Meta': {'object_name': 'ContactUsCategory'},
            'email': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'})
        },
        u'about.contactusrequest': {
            'Meta': {'object_name': 'ContactUsRequest'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'contact_us_request'", 'to': u"orm['about.ContactUsCategory']"}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '254'}),
            'fio': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '1000'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '12'}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '254'})
        },
        u'about.goodsprofile': {
            'Meta': {'object_name': 'GoodsProfile'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'})
        },
        u'about.messagetemplate': {
            'Meta': {'object_name': 'MessageTemplate'},
            'accost': ('django.db.models.fields.CharField', [], {'default': "u'\\u0417\\u0434\\u0440\\u0430\\u0432\\u0441\\u0442\\u0432\\u0443\\u0439\\u0442\\u0435, '", 'max_length': '255'}),
            'footer': ('ckeditor.fields.RichTextField', [], {'max_length': '2000', 'null': 'True', 'blank': 'True'}),
            'header': ('ckeditor.fields.RichTextField', [], {'max_length': '2000'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '254'}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '254'})
        },
        u'about.renterrequest': {
            'Meta': {'object_name': 'RenterRequest'},
            'address': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'brands': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '254'}),
            'fax': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '12', 'null': 'True', 'blank': 'True'}),
            'goods_profile': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'renter_requests'", 'null': 'True', 'to': u"orm['about.GoodsProfile']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'in_malls': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '254', 'blank': 'True'}),
            'info_about_opened_shops': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '2000', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'opened_count': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '12'}),
            'position_person': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'presentation': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'site_url': ('django.db.models.fields.URLField', [], {'default': 'None', 'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'square_from': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'square_optimal': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'square_to': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'trade_profile': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'renter_requests'", 'null': 'True', 'to': u"orm['about.TradeProfile']"})
        },
        u'about.tradeprofile': {
            'Meta': {'object_name': 'TradeProfile'},
            'block_goods_profile': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'})
        }
    }

    complete_apps = ['about']