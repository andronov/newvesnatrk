# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'MessageTemplate'
        db.create_table(u'about_messagetemplate', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=254)),
            ('header_ru', self.gf('ckeditor.fields.RichTextField')(max_length=2000, null=True)),
            ('header_en', self.gf('ckeditor.fields.RichTextField')(max_length=2000, null=True, blank=True)),
            ('footer_ru', self.gf('ckeditor.fields.RichTextField')(max_length=2000, null=True)),
            ('footer_en', self.gf('ckeditor.fields.RichTextField')(max_length=2000, null=True, blank=True)),
            ('subject_ru', self.gf('django.db.models.fields.CharField')(max_length=254, null=True)),
            ('subject_en', self.gf('django.db.models.fields.CharField')(max_length=254, null=True, blank=True)),
        ))
        db.send_create_signal(u'about', ['MessageTemplate'])


    def backwards(self, orm):
        # Deleting model 'MessageTemplate'
        db.delete_table(u'about_messagetemplate')


    models = {
        u'about.goodsprofile': {
            'Meta': {'object_name': 'GoodsProfile'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'})
        },
        u'about.messagetemplate': {
            'Meta': {'object_name': 'MessageTemplate'},
            'footer_en': ('ckeditor.fields.RichTextField', [], {'max_length': '2000', 'null': 'True', 'blank': 'True'}),
            'footer_ru': ('ckeditor.fields.RichTextField', [], {'max_length': '2000', 'null': 'True'}),
            'header_en': ('ckeditor.fields.RichTextField', [], {'max_length': '2000', 'null': 'True', 'blank': 'True'}),
            'header_ru': ('ckeditor.fields.RichTextField', [], {'max_length': '2000', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '254'}),
            'subject_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'subject_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'})
        },
        u'about.renterrequest': {
            'Meta': {'object_name': 'RenterRequest'},
            'brands': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '254'}),
            'goods_profile': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['about.GoodsProfile']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'in_malls': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '254', 'blank': 'True'}),
            'info_about_opened_shops': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '2000', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'opened_count': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '12'}),
            'presentation': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'square': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'trade_profile': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['about.TradeProfile']", 'null': 'True', 'blank': 'True'})
        },
        u'about.tradeprofile': {
            'Meta': {'object_name': 'TradeProfile'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'})
        }
    }

    complete_apps = ['about']