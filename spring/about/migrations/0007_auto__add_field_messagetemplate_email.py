# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'MessageTemplate.email'
        db.add_column(u'about_messagetemplate', 'email',
                      self.gf('django.db.models.fields.CharField')(default=1, max_length=254),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'MessageTemplate.email'
        db.delete_column(u'about_messagetemplate', 'email')


    models = {
        u'about.adchannel': {
            'Meta': {'object_name': 'AdChannel'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'})
        },
        u'about.adrequest': {
            'Meta': {'object_name': 'AdRequest'},
            'channels': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'ad_requests'", 'symmetrical': 'False', 'to': u"orm['about.AdChannel']"}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '254'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'message': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '1000', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '12'})
        },
        u'about.contactuscategory': {
            'Meta': {'object_name': 'ContactUsCategory'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'})
        },
        u'about.contactusrequest': {
            'Meta': {'object_name': 'ContactUsRequest'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'contact_us_request'", 'to': u"orm['about.ContactUsCategory']"}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '254'}),
            'fio': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '1000'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '12'}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '254'})
        },
        u'about.goodsprofile': {
            'Meta': {'object_name': 'GoodsProfile'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'})
        },
        u'about.messagetemplate': {
            'Meta': {'object_name': 'MessageTemplate'},
            'email': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'footer': ('ckeditor.fields.RichTextField', [], {'max_length': '2000'}),
            'header': ('ckeditor.fields.RichTextField', [], {'max_length': '2000'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '254'}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '254'})
        },
        u'about.renterrequest': {
            'Meta': {'object_name': 'RenterRequest'},
            'brands': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '254'}),
            'goods_profile': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'renter_requests'", 'to': u"orm['about.GoodsProfile']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'in_malls': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '254', 'blank': 'True'}),
            'info_about_opened_shops': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '2000', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'opened_count': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '12'}),
            'presentation': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'square_from': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'square_optimal': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'square_to': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'trade_profile': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'renter_requests'", 'null': 'True', 'to': u"orm['about.TradeProfile']"})
        },
        u'about.tradeprofile': {
            'Meta': {'object_name': 'TradeProfile'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'})
        }
    }

    complete_apps = ['about']