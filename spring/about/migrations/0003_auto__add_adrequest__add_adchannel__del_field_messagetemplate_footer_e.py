# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'AdRequest'
        db.create_table(u'about_adrequest', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=254)),
            ('message', self.gf('ckeditor.fields.RichTextField')(default='', max_length=1000, blank=True)),
        ))
        db.send_create_signal(u'about', ['AdRequest'])

        # Adding M2M table for field channels on 'AdRequest'
        m2m_table_name = db.shorten_name(u'about_adrequest_channels')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('adrequest', models.ForeignKey(orm[u'about.adrequest'], null=False)),
            ('adchannel', models.ForeignKey(orm[u'about.adchannel'], null=False))
        ))
        db.create_unique(m2m_table_name, ['adrequest_id', 'adchannel_id'])

        # Adding model 'AdChannel'
        db.create_table(u'about_adchannel', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=254, null=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=254, null=True, blank=True)),
        ))
        db.send_create_signal(u'about', ['AdChannel'])

        # Deleting field 'MessageTemplate.footer_en'
        db.delete_column(u'about_messagetemplate', 'footer_en')

        # Deleting field 'MessageTemplate.header_en'
        db.delete_column(u'about_messagetemplate', 'header_en')

        # Deleting field 'MessageTemplate.header_ru'
        db.delete_column(u'about_messagetemplate', 'header_ru')

        # Deleting field 'MessageTemplate.subject_en'
        db.delete_column(u'about_messagetemplate', 'subject_en')

        # Deleting field 'MessageTemplate.subject_ru'
        db.delete_column(u'about_messagetemplate', 'subject_ru')

        # Deleting field 'MessageTemplate.footer_ru'
        db.delete_column(u'about_messagetemplate', 'footer_ru')

        # Adding field 'MessageTemplate.header'
        db.add_column(u'about_messagetemplate', 'header',
                      self.gf('ckeditor.fields.RichTextField')(default='', max_length=2000),
                      keep_default=False)

        # Adding field 'MessageTemplate.footer'
        db.add_column(u'about_messagetemplate', 'footer',
                      self.gf('ckeditor.fields.RichTextField')(default='', max_length=2000),
                      keep_default=False)

        # Adding field 'MessageTemplate.subject'
        db.add_column(u'about_messagetemplate', 'subject',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=254),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting model 'AdRequest'
        db.delete_table(u'about_adrequest')

        # Removing M2M table for field channels on 'AdRequest'
        db.delete_table(db.shorten_name(u'about_adrequest_channels'))

        # Deleting model 'AdChannel'
        db.delete_table(u'about_adchannel')

        # Adding field 'MessageTemplate.footer_en'
        db.add_column(u'about_messagetemplate', 'footer_en',
                      self.gf('ckeditor.fields.RichTextField')(max_length=2000, null=True, blank=True),
                      keep_default=False)

        # Adding field 'MessageTemplate.header_en'
        db.add_column(u'about_messagetemplate', 'header_en',
                      self.gf('ckeditor.fields.RichTextField')(max_length=2000, null=True, blank=True),
                      keep_default=False)

        # Adding field 'MessageTemplate.header_ru'
        db.add_column(u'about_messagetemplate', 'header_ru',
                      self.gf('ckeditor.fields.RichTextField')(max_length=2000, null=True),
                      keep_default=False)

        # Adding field 'MessageTemplate.subject_en'
        db.add_column(u'about_messagetemplate', 'subject_en',
                      self.gf('django.db.models.fields.CharField')(max_length=254, null=True, blank=True),
                      keep_default=False)

        # Adding field 'MessageTemplate.subject_ru'
        db.add_column(u'about_messagetemplate', 'subject_ru',
                      self.gf('django.db.models.fields.CharField')(max_length=254, null=True),
                      keep_default=False)

        # Adding field 'MessageTemplate.footer_ru'
        db.add_column(u'about_messagetemplate', 'footer_ru',
                      self.gf('ckeditor.fields.RichTextField')(max_length=2000, null=True),
                      keep_default=False)

        # Deleting field 'MessageTemplate.header'
        db.delete_column(u'about_messagetemplate', 'header')

        # Deleting field 'MessageTemplate.footer'
        db.delete_column(u'about_messagetemplate', 'footer')

        # Deleting field 'MessageTemplate.subject'
        db.delete_column(u'about_messagetemplate', 'subject')


    models = {
        u'about.adchannel': {
            'Meta': {'object_name': 'AdChannel'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'})
        },
        u'about.adrequest': {
            'Meta': {'object_name': 'AdRequest'},
            'channels': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'ad_requests'", 'symmetrical': 'False', 'to': u"orm['about.AdChannel']"}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '254'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '1000', 'blank': 'True'})
        },
        u'about.goodsprofile': {
            'Meta': {'object_name': 'GoodsProfile'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'})
        },
        u'about.messagetemplate': {
            'Meta': {'object_name': 'MessageTemplate'},
            'footer': ('ckeditor.fields.RichTextField', [], {'max_length': '2000'}),
            'header': ('ckeditor.fields.RichTextField', [], {'max_length': '2000'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '254'}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '254'})
        },
        u'about.renterrequest': {
            'Meta': {'object_name': 'RenterRequest'},
            'brands': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '254'}),
            'goods_profile': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'renter_requests'", 'to': u"orm['about.GoodsProfile']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'in_malls': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '254', 'blank': 'True'}),
            'info_about_opened_shops': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '2000', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'opened_count': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '12'}),
            'presentation': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'square': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'trade_profile': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'renter_requests'", 'null': 'True', 'to': u"orm['about.TradeProfile']"})
        },
        u'about.tradeprofile': {
            'Meta': {'object_name': 'TradeProfile'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'})
        }
    }

    complete_apps = ['about']