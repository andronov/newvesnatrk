# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'GoodsProfile'
        db.create_table(u'about_goodsprofile', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=254, null=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=254, null=True, blank=True)),
        ))
        db.send_create_signal(u'about', ['GoodsProfile'])

        # Adding model 'TradeProfile'
        db.create_table(u'about_tradeprofile', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=254, null=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=254, null=True, blank=True)),
        ))
        db.send_create_signal(u'about', ['TradeProfile'])

        # Adding model 'RenterRequest'
        db.create_table(u'about_renterrequest', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=254)),
            ('brands', self.gf('django.db.models.fields.CharField')(max_length=254)),
            ('goods_profile', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['about.GoodsProfile'])),
            ('square', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=12)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=254)),
            ('info_about_opened_shops', self.gf('ckeditor.fields.RichTextField')(default='', max_length=2000, blank=True)),
            ('opened_count', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0, blank=True)),
            ('trade_profile', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['about.TradeProfile'], null=True, blank=True)),
            ('in_malls', self.gf('django.db.models.fields.CharField')(default='', max_length=254, blank=True)),
            ('presentation', self.gf('filebrowser.fields.FileBrowseField')(default='', max_length=254, null=True, blank=True)),
        ))
        db.send_create_signal(u'about', ['RenterRequest'])


    def backwards(self, orm):
        # Deleting model 'GoodsProfile'
        db.delete_table(u'about_goodsprofile')

        # Deleting model 'TradeProfile'
        db.delete_table(u'about_tradeprofile')

        # Deleting model 'RenterRequest'
        db.delete_table(u'about_renterrequest')


    models = {
        u'about.goodsprofile': {
            'Meta': {'object_name': 'GoodsProfile'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'})
        },
        u'about.renterrequest': {
            'Meta': {'object_name': 'RenterRequest'},
            'brands': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '254'}),
            'goods_profile': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['about.GoodsProfile']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'in_malls': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '254', 'blank': 'True'}),
            'info_about_opened_shops': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '2000', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'opened_count': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '12'}),
            'presentation': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'square': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'trade_profile': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['about.TradeProfile']", 'null': 'True', 'blank': 'True'})
        },
        u'about.tradeprofile': {
            'Meta': {'object_name': 'TradeProfile'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'})
        }
    }

    complete_apps = ['about']