# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import datetime
from urllib2 import unquote

from django.template.loader import render_to_string
from django.core.mail import EmailMessage
from django.core.files.storage import default_storage
from rest_framework.generics import ListAPIView, CreateAPIView
from rest_framework.response import Response
from rest_framework.status import is_success
from spring.about.serializers import (
    BannerSerializer, ContactUsCategorySerializer, GoodsProfileSerializer, TradeProfileSerializer,
    AdChannelSerializer, RenterRequestSerializer, AdRequestSerializer, ContactUsSerializer
)
from spring.site.models import Settings
from spring.about.models import (
    GoodsProfile, TradeProfile, MessageTemplate, AdChannel, ContactUsCategory, Banner
)


class RenterRequestCreate(CreateAPIView):
    serializer_class = RenterRequestSerializer

    def create(self, request, *args, **kwargs):
        response = super(RenterRequestCreate, self).create(request, *args, **kwargs)
        if not is_success(response.status_code):
            response.data = {
                'form_errors': response.data,
            }
        response.data.update({
            'form_accepted': True if is_success(response.status_code) else False
        })
        return response

    def post_save(self, obj, created=False):
        message_template = MessageTemplate.objects.get(name='renter_request')
        email = EmailMessage(
            subject=message_template.subject.replace('{{ today }}', datetime.date.today().strftime("%d.%m.%Y")),
            body=render_to_string('contact_us.html', {
                'header': message_template.header,
                'description': get_message(obj),
                'footer': message_template.footer,
                'server_name': 'http://' + self.request.META['SERVER_NAME']
            }
            ),
            to=[Settings.objects.get().email_renter_request],
        )
        if obj.presentation:
            file_object = default_storage.open(
                unquote(obj.presentation.path)
            )
            email.attach_file(default_storage.path(file_object))
        email.content_subtype = 'html'
        email.send(True)
renter_request = RenterRequestCreate.as_view()


class BannerList(ListAPIView):
    model = Banner
    serializer_class = BannerSerializer

    def list(self, request, *args, **kwargs):
        return Response({
            'object_list': self.get_serializer(self.get_queryset(), many=True).data
        })
banner_list = BannerList.as_view()


class GoodsAndTradeProfileList(ListAPIView):
    model = GoodsProfile
    serializer_class = GoodsProfileSerializer

    def list(self, request, *args, **kwargs):
        return Response({
            'object_list': self.get_serializer(self.get_queryset(), many=True).data,
            'trade_profile_list': [TradeProfileSerializer(item).data for item in TradeProfile.objects.all()]
        })
goods_and_trade_profile_list = GoodsAndTradeProfileList.as_view()


class AdRequestCreate(CreateAPIView):
    serializer_class = AdRequestSerializer

    def create(self, request, *args, **kwargs):
        response = super(AdRequestCreate, self).create(request, *args, **kwargs)
        if not is_success(response.status_code):
            response.data = {
                'form_errors': response.data,
            }
        response.data.update({
            'form_accepted': True if is_success(response.status_code) else False
        })
        return response

    def post_save(self, obj, created=False):
        message_template = MessageTemplate.objects.get(name='ad')
        email = EmailMessage(
            subject=message_template.subject.replace('{{ today }}', datetime.date.today().strftime("%d.%m.%Y")),
            body=render_to_string('contact_us.html', {
                'header': message_template.header,
                'description': get_message(obj),
                'footer': message_template.footer,
                'server_name': 'http://' + self.request.META['SERVER_NAME']
            }
            ),
            to=[Settings.objects.get().email_advertisement],
        )

        email.content_subtype = 'html'
        email.send(True)
ad_request_create = AdRequestCreate.as_view()


class AdChannelList(ListAPIView):
    model = AdChannel
    serializer_class = AdChannelSerializer

    def list(self, request, *args, **kwargs):
        return Response({
            'object_list': self.get_serializer(self.get_queryset(), many=True).data
        })
ad_channel_list = AdChannelList.as_view()


class ContactUsRequestCreate(CreateAPIView):
    serializer_class = ContactUsSerializer

    def create(self, request, *args, **kwargs):
        response = super(ContactUsRequestCreate, self).create(request, *args, **kwargs)
        if not is_success(response.status_code):
            response.data = {
                'form_errors': response.data,
            }
        response.data.update({
            'form_accepted': True if is_success(response.status_code) else False
        })
        return response

    def post_save(self, obj, created=False):
        category = ContactUsCategory.objects.get(pk=obj.category.pk)
        message_template = MessageTemplate.objects.get(name='contact_us')
        email = EmailMessage(
            subject=message_template.subject.replace('{{ today }}', datetime.date.today().strftime("%d.%m.%Y")),
            body=render_to_string('contact_us.html', {
                'header': message_template.header,
                'description': get_message(obj),
                'footer': message_template.footer,
                'server_name': 'http://' + self.request.META['SERVER_NAME']
            }),
            to=[category.email]
        )

        email.content_subtype = 'html'
        email.send(True)
contact_us_request_create = ContactUsRequestCreate.as_view()


class ContactUsCategoryList(ListAPIView):
    model = ContactUsCategory
    serializer_class = ContactUsCategorySerializer

    def list(self, request, *args, **kwargs):
        return Response({
            'object_list': self.get_serializer(self.get_queryset(), many=True).data
        })
contact_us_category_list = ContactUsCategoryList.as_view()


def clean_data(data, field_name):
    if data is None:
        return ''
    value = list()
    if isinstance(data, basestring):
        data = [data]
    for item in [data]:
        if field_name == 'goods_profile':
            model = GoodsProfile
        elif field_name == 'trade_profile':
            model = TradeProfile
        else:
            model = AdChannel
        val = model.objects.get(pk=item.pk)
        value.append(val.name)
    return ', '.join(value)


def get_message(obj):
        message = ''
        for field in obj._meta.fields:
            if field.name in ['id', 'presentation', 'captcha', 'category'] or getattr(obj, field.name) == '':
                continue
            if field.name in ['channels', 'trade_profile', 'goods_profile']:
                value = clean_data(getattr(obj, field.name), field.name)
            else:
                value = unicode(getattr(obj, field.name))
            message += """
                {}: {}<br \>
            """.format(unicode(field.verbose_name), value)
        return message
