# -*- coding: utf-8 -*-
from django.contrib import admin
from spring.about.models import (
    GoodsProfile,
    TradeProfile,
    RenterRequest,
    MessageTemplate,
    ContactUsRequest,
    ContactUsCategory,
    AdRequest,
    AdChannel,
    Banner
)


class MessageTemplateAdmin(admin.ModelAdmin):
    actions = None

    def has_add_permission(self, request):
        return False


class RenterRequestAdmin(admin.ModelAdmin):
    list_display = ('name', 'square_from', 'square_to', 'square_optimal', 'phone', 'fax', 'email', 'created_at', 'address', 'site_url', 'position_person')
    list_filter = ('name', 'square_from', 'square_to', 'square_optimal', 'phone', 'email', 'created_at')
    search_fields = ('name', 'square_from', 'square_to', 'square_optimal', 'phone', 'email', 'created_at')


class ContactUsRequestAdmin(admin.ModelAdmin):
    list_display = ('subject', 'category', 'email', 'fio', 'message', 'created_at')
    list_filter = ('subject', 'category', 'email', 'fio', 'message', 'created_at')
    search_fields = ('subject', 'category', 'email', 'fio', 'message', 'created_at')


class AdRequestAdmin(admin.ModelAdmin):
    list_display = ('email', 'created_at', 'first_name', 'last_name', 'phone', 'message', 'created_at')
    filter_horizontal = ('channels', )

admin.site.register(GoodsProfile)
admin.site.register(TradeProfile)
admin.site.register(RenterRequest, RenterRequestAdmin)
admin.site.register(ContactUsRequest, ContactUsRequestAdmin)
admin.site.register(ContactUsCategory)
admin.site.register(AdRequest, AdRequestAdmin)
admin.site.register(AdChannel)
admin.site.register(MessageTemplate, MessageTemplateAdmin)
admin.site.register(Banner)
