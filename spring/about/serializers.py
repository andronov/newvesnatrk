from django.utils.translation import get_language
from rest_framework.fields import SerializerMethodField, WritableField
from rest_framework.serializers import ModelSerializer
from spring.about.models import (
    Banner, ContactUsCategory, GoodsProfile, TradeProfile, AdChannel, RenterRequest, AdRequest, ContactUsRequest
)
from spring.subscription.serializers import CaptchaSerializer
from utilities.fields import SerializerLocaleField, FileBrowseField


class RenterRequestSerializer(CaptchaSerializer, ModelSerializer):
    presentation = FileBrowseField(source='presentation', required=False)

    class Meta:
        model = RenterRequest


class AdRequestSerializer(CaptchaSerializer, ModelSerializer):
    channels = WritableField(source='channels', required=True)

    class Meta:
        model = AdRequest


class ContactUsSerializer(CaptchaSerializer, ModelSerializer):
    class Meta:
        model = ContactUsRequest


class BannerSerializer(ModelSerializer):
    banner = SerializerMethodField('get_banner')

    def get_banner(self, obj):
        try:
            return getattr(obj, 'banner_{}'.format(get_language())).url
        except (AttributeError, KeyError, ValueError):
            return ''

    class Meta:
        model = Banner
        fields = ('banner', 'is_published')


class ContactUsCategorySerializer(ModelSerializer):
    name = SerializerLocaleField()

    class Meta:
        model = ContactUsCategory
        fields = ('name', 'email', 'id')


class GoodsProfileSerializer(ModelSerializer):
    name = SerializerLocaleField()

    class Meta:
        model = GoodsProfile
        fields = ('name', 'id')


class TradeProfileSerializer(GoodsProfileSerializer):
    class Meta(GoodsProfileSerializer.Meta):
        model = TradeProfile
        fields = GoodsProfileSerializer.Meta.fields + ('block_goods_profile',)


class AdChannelSerializer(GoodsProfileSerializer):
    class Meta(GoodsProfileSerializer.Meta):
        model = AdChannel
