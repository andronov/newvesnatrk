# -*- coding: utf-8 -*-
from ckeditor.fields import RichTextField
from django.core.validators import MaxValueValidator, MinValueValidator

from django.db import models
from django.utils.translation import ugettext_lazy as _, get_language

from easymode.i18n.decorators import I18n
from filebrowser.fields import FileBrowseField


@I18n('name')
class GoodsProfile(models.Model):
    name = models.CharField(_('Name'), max_length=254)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('Goods profile')
        verbose_name_plural = _('Goods profiles')


@I18n('name')
class TradeProfile(models.Model):
    name = models.CharField(_('Name'), max_length=254)
    block_goods_profile = models.BooleanField(_('Block goods profile'), default=False)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('Trade profile')
        verbose_name_plural = _('Trade profiles')


class RenterRequest(models.Model):
    name = models.CharField(_('Name'), max_length=254)
    brands = models.CharField(_('Brands'), max_length=254)
    goods_profile = models.ForeignKey(
        GoodsProfile,
        verbose_name=_('Goods profile'),
        related_name='renter_requests',
        null=True,
        blank=True
    )
    square_from = models.PositiveSmallIntegerField(
        _('Square from'),
        validators=[
            MaxValueValidator(1000),
            MinValueValidator(1)
        ]
    )
    square_to = models.PositiveSmallIntegerField(
        _('Square to'),
        validators=[
            MaxValueValidator(1000),
            MinValueValidator(1)
        ]
    )
    square_optimal = models.PositiveSmallIntegerField(
        _('Square optimal'),
        validators=[
            MaxValueValidator(1000),
            MinValueValidator(1)
        ]
    )
    phone = models.CharField(_('Phone number'), max_length=12)
    email = models.EmailField(_('Email'), max_length=254)

    info_about_opened_shops = RichTextField(_('Comments'), config_name='big', max_length=2000, default='', blank=True)
    opened_count = models.PositiveSmallIntegerField(_('Opened shops count'), default=0, blank=True, null=True)
    trade_profile = models.ForeignKey(TradeProfile, verbose_name=_('Trade profile'), null=True, blank=True, related_name='renter_requests')
    in_malls = models.CharField(_('In which malls'), max_length=254, default='', blank=True)
    presentation = FileBrowseField(
        verbose_name=_('Presentation'),
        directory='about/presentation/',
        max_length=254,
        extensions=['.pdf', '.ppt', '.key', '.doc', '.docx', '.jpg', '.rar', '.zip', '.png'],
        null=True,
        default='',
        blank=True)
    created_at = models.DateTimeField(_('Date add'), auto_now_add=True)
    address = models.CharField(_('Fact address'), max_length=255, null=True, blank=True, default=None)
    site_url = models.URLField(_('Site address'), blank=True, null=True, default=None)
    position_person = models.CharField(_('Person position'), max_length=100, blank=True, null=True, default=None)
    fax = models.CharField(_('Fax'), max_length=12, blank=True, null=True, default=None)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('Renter request')
        verbose_name_plural = _('Renter requests')


@I18n('name')
class AdChannel(models.Model):
    name = models.CharField(_('Channel name'), max_length=254)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('Advertisement channel')
        verbose_name_plural = _('Advertisement channels')


class AdRequest(models.Model):
    channels = models.ManyToManyField(AdChannel, verbose_name=_('Advertisement channel'), related_name='ad_requests')
    email = models.EmailField(_('Email'), max_length=254)
    first_name = models.CharField(_('First name'), max_length=254)
    last_name = models.CharField(_('Last name'), max_length=254, default='', blank=True)
    phone = models.CharField(_('Phone number'), max_length=12, default='', blank=True)
    message = RichTextField(_('Message'), config_name='big', max_length=1000, default='', blank=True)
    created_at = models.DateTimeField(_('Date add'), auto_now_add=True)

    def __unicode__(self):
        return self.email

    class Meta:
        verbose_name = _('Advertisement request')
        verbose_name_plural = _('Advertisement requests')

@I18n('name')
class ContactUsCategory(models.Model):
    name = models.CharField(_('Category mame'), max_length=254)
    email = models.CharField(_('Recipient email'), max_length=254)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('Contact us category')
        verbose_name_plural = _('Contact us categories')


class ContactUsRequest(models.Model):
    category = models.ForeignKey(ContactUsCategory, verbose_name=_('Contact us category'), related_name='contact_us_request')
    email = models.EmailField(_('Email'), max_length=254)
    subject = models.CharField(_('Subject'), max_length=254)
    phone = models.CharField(_('Phone number'), max_length=12)
    fio = models.CharField(_('FIO'), max_length=254)
    message = RichTextField(_('Message'), config_name='big', max_length=1000, default='')
    created_at = models.DateTimeField(_('Date add'), auto_now_add=True)

    def __unicode__(self):
        return self.email

    class Meta:
        verbose_name = _('Contact us request')
        verbose_name_plural = _('Contact us requests')


class MessageTemplate(models.Model):
    SYSTEM_NAMES = (
        ('renter_request', _('Renter request')),
        ('ad', _('Advertisement')),
        ('contact_us', _('Contact us')),
        ('after_subscribe', _('After subscribe')),
    )
    name = models.CharField(_('Message name'), max_length=254, choices=SYSTEM_NAMES, unique=True)
    header = RichTextField(_('Mesage template'), config_name='big', max_length=2000)
    footer = RichTextField(_('Message footer'), config_name='big', max_length=2000, blank=True, null=True)
    subject = models.CharField(_('Message subject'), max_length=254)
    accost = models.CharField(_('Accost'), default=u'Здравствуйте, ', max_length=255)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('Message template')
        verbose_name_plural = _('Message templates')


@I18n('banner')
class Banner(models.Model):
    banner = FileBrowseField(verbose_name=_('Banner'), directory='about/banners/', format='image', max_length=255)
    is_published = models.BooleanField(_('Is published'), default=False)

    def __unicode__(self):
        banner = getattr(self, 'banner_{}'.format(get_language()), None)
        return banner.url if banner else 'None'
