from django.conf.urls import patterns, url


urlpatterns = patterns(
    'spring.about.views',
    url(r'^banners/$', 'banner_list'),
    url(r'^renter_request/$', 'renter_request'),
    url(r'^goods_and_trade/$', 'goods_and_trade_profile_list'),
    url(r'^ad_request/$', 'ad_request_create'),
    url(r'^ad_channels/$', 'ad_channel_list'),
    url(r'^contact_us/$', 'contact_us_request_create'),
    url(r'^contact_us_cats/$', 'contact_us_category_list'),
)
