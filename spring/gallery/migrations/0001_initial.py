# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'GalleryCategory'
        db.create_table(u'gallery_gallerycategory', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=254, null=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=254, null=True, blank=True)),
            ('parent', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='children', null=True, to=orm['gallery.GalleryCategory'])),
        ))
        db.send_create_signal(u'gallery', ['GalleryCategory'])

        # Adding model 'GalleryImage'
        db.create_table(u'gallery_galleryimage', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('image', self.gf('filebrowser.fields.FileBrowseField')(default='', max_length=254, null=True, blank=True)),
        ))
        db.send_create_signal(u'gallery', ['GalleryImage'])

        # Adding M2M table for field category on 'GalleryImage'
        m2m_table_name = db.shorten_name(u'gallery_galleryimage_category')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('galleryimage', models.ForeignKey(orm[u'gallery.galleryimage'], null=False)),
            ('gallerycategory', models.ForeignKey(orm[u'gallery.gallerycategory'], null=False))
        ))
        db.create_unique(m2m_table_name, ['galleryimage_id', 'gallerycategory_id'])


    def backwards(self, orm):
        # Deleting model 'GalleryCategory'
        db.delete_table(u'gallery_gallerycategory')

        # Deleting model 'GalleryImage'
        db.delete_table(u'gallery_galleryimage')

        # Removing M2M table for field category on 'GalleryImage'
        db.delete_table(db.shorten_name(u'gallery_galleryimage_category'))


    models = {
        u'gallery.gallerycategory': {
            'Meta': {'object_name': 'GalleryCategory'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': u"orm['gallery.GalleryCategory']"})
        },
        u'gallery.galleryimage': {
            'Meta': {'object_name': 'GalleryImage'},
            'category': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'images'", 'symmetrical': 'False', 'to': u"orm['gallery.GalleryCategory']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['gallery']