# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _
from ckeditor.fields import RichTextField
from easymode.i18n.decorators import I18n
from filebrowser.fields import FileBrowseField
from utilities.capable.models import Orderable


@I18n('name')
class GalleryCategory(Orderable):
    name = models.CharField(_('Name'), max_length=254)
    parent = models.ForeignKey('self', verbose_name=_('Parent'), related_name='children', null=True, blank=True)
    description = RichTextField(_('Description'), null=True, blank=True)
    last_update = models.DateField(_('Last update'), auto_now_add=True)

    def __unicode__(self):
        return self.name

    class Meta(Orderable.Meta):
        verbose_name = _('Gallery category')
        verbose_name_plural = _('Gallery categories')


class GalleryImage(models.Model):
    image = FileBrowseField(
        verbose_name=_('Image'),
        directory='gallery/',
        max_length=254,
        null=True,
        default='',
        blank=True)
    category = models.ManyToManyField(GalleryCategory, verbose_name=_('Gallery category'), related_name='images')

    def __unicode__(self):
        return self.image.name

    class Meta:
        verbose_name = _('Gallery image')
        verbose_name_plural = _('Gallery images')
