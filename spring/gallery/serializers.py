from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import ModelSerializer
from spring.gallery.models import GalleryCategory, GalleryImage
from utilities.fields import SerializerLocaleField


class GalleryImageSerializer(ModelSerializer):
    image_180x130 = SerializerMethodField('get_image_180x130')
    image_369x268 = SerializerMethodField('get_image_369x268')
    image_692x500 = SerializerMethodField('get_image_692x500')

    def get_image_field(self, obj, field_name):
        try:
            return obj.image.version_generate(field_name).url
        except (AttributeError, KeyError, ValueError, IOError):
            return ''

    def get_image_180x130(self, obj):
        return self.get_image_field(obj, '180x130')

    def get_image_369x268(self, obj):
        return self.get_image_field(obj, '369x268')

    def get_image_692x500(self, obj):
        return self.get_image_field(obj, '692x500')

    class Meta:
        model = GalleryImage
        fields = ('id', 'image', 'image_180x130', 'image_369x268', 'image_692x500')


class GallerySerializer(ModelSerializer):
    name = SerializerLocaleField()
    images = GalleryImageSerializer(many=True)
    children = SerializerMethodField('get_children')

    def get_children(self, obj):
        return GallerySerializer(obj.children.all(), many=True).data

    class Meta:
        model = GalleryCategory
        fields = ('children', 'description', 'id', 'images', 'last_update', 'name', 'position')