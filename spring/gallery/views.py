# -*- coding: utf-8 -*-
from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.response import Response
from spring.gallery.models import GalleryCategory
from spring.gallery.serializers import GallerySerializer


class GalleryCategoryList(ListAPIView):
    model = GalleryCategory
    serializer_class = GallerySerializer

    def get_queryset(self):
        return super(GalleryCategoryList, self).get_queryset().filter(parent=None)

    def list(self, request, *args, **kwargs):
        return Response({
            'object_list': self.get_serializer(self.get_queryset(), many=True).data
        })
gallery_list = GalleryCategoryList.as_view()


class GalleryCategoryDetail(RetrieveAPIView):
    model = GalleryCategory
    serializer_class = GallerySerializer

    def retrieve(self, request, *args, **kwargs):
        serializer = self.serializer_class(self.get_object())

        return Response({
            'object': serializer.data
        })
gallery_detail = GalleryCategoryDetail.as_view()
