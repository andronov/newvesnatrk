from django.conf.urls import patterns, url


urlpatterns = patterns(
    'spring.gallery.views',
    url(r'^$', 'gallery_list'),
    url(r'^(?P<pk>\d+)/$', 'gallery_detail'),
)
