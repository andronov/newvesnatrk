# -*- coding: utf-8 -*-
import datetime

from django.contrib import admin
from spring.gallery.models import GalleryCategory, GalleryImage
from utilities.capable.admin import OrderableAdmin


class GalleryImageAdmin(admin.ModelAdmin):
    filter_horizontal = ('category', )

    def save_model(self, request, obj, form, change):
        obj.save()
        albums = obj.category.all()
        for album in albums:
            album.last_update = datetime.date.today()
            album.save(update_fields=['last_update', ])


class GalleryCategoryAdmin(OrderableAdmin):
    list_display = ('name', 'last_update') + OrderableAdmin.list_display

    def render_change_form(self, request, context, *args, **kwargs):
        obj = kwargs.get('obj', None)
        pks = []
        have_children = False
        if obj:
            pks = [obj.pk]
            have_children = obj.children.all().exists()
        if not have_children:
            context['adminform'].form.fields['parent'].queryset = GalleryCategory.objects.filter(parent=None).exclude(pk__in=pks)
        else:
            context['adminform'].form.fields['parent'].queryset = GalleryCategory.objects.none()
        return super(GalleryCategoryAdmin, self).render_change_form(request, context, args, kwargs)

admin.site.register(GalleryCategory, GalleryCategoryAdmin)
admin.site.register(GalleryImage, GalleryImageAdmin)

