# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os
from StringIO import StringIO
from urllib import quote
from datetime import datetime, timedelta
import requests
from PIL import Image
from django.conf import settings
from django.core.management.base import NoArgsCommand
from spring.cinema.models import Movie, MovieSchedule
from spring.cinema.utilities import KinohodAPI, RottenTomatoesAPI, LuxorAPI


class Command(NoArgsCommand):
    help = 'Loads information about schedule and movies for until next Wednesday'

    def handle_noargs(self, **options):
        luxor = LuxorAPI()
        tomatoes = RottenTomatoesAPI()
        today = datetime.today()
        next_wednesday = today + timedelta(days=5)
        agent = 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0'

        luxor.get_schedules(today)

        return
        while today != next_wednesday:
            luxor.get_schedules(today)

            today += timedelta(days=1)
            # for item in :
            #     pass
                # mov = Movie.objects.filter(kinohod_id=item['movie']['id'])
                # if not mov.exists():
                #     movie_ru = kinohod.get_movie(item['movie']['id'])
                #
                #     image_ru = Image.open(
                #         StringIO(requests.get(kinohod.get_movie_poster(movie_ru['poster'], 1625, 2544)).content))
                #
                #     image_ru_path = 'cinema/{}/{}'.format(
                #         datetime.now().strftime('%Y/%m'),
                #         os.path.basename(movie_ru['poster'])
                #     )
                #
                #     image_ru.save(os.path.join(settings.MEDIA_ROOT, image_ru_path))
                #
                #     try:
                #         rating = requests.get(
                #             os.path.join(settings.KINOPOISK_RATING_URL, quote(item['movie']['title'].encode('utf-8'))),
                #             headers={'User-Agent': agent}
                #         ).json().get('rating', '0.0')
                #     except (ValueError, KeyError):
                #         rating = '0.0'
                #     if rating == '0.0':
                #         try:
                #             rating = requests.get(
                #                 os.path.join(settings.KINOPOISK_RATING_URL, quote(item['movie']['originalTitle'].encode('utf-8'))),
                #                 headers={'User-Agent': agent}
                #             ).json().get('rating', '0.0')
                #         except (ValueError, KeyError, AttributeError):
                #             rating = '0.0'
                #
                #     params = {
                #         'kinohod_id': item['movie']['id'],
                #         'title_ru': movie_ru['title'],
                #         'title_en': movie_ru['originalTitle'],
                #         'duration': str(timedelta(seconds=int(movie_ru['duration'] or 0) * 60)),
                #         'short_desc_ru': movie_ru['annotationShort'],
                #         'long_desc_ru': movie_ru['annotationFull'],
                #         'age_restriction': movie_ru['ageRestriction'],
                #         'rating': rating,
                #         'poster_ru': image_ru_path,
                #         'countries_ru': ', '.join(movie_ru['countries']),
                #         'actors_ru': ', '.join(movie_ru['actors']),
                #         'producers_ru': ', '.join(movie_ru['producers']),
                #         'directors_ru': ', '.join(movie_ru['directors']),
                #     }
                #
                #     try:
                #         movie_en = tomatoes.get_movie(unicode(movie_ru['originalTitle']))['movies'][0]
                #
                #         if (movie_ru['originalTitle'] is not None and movie_en['posters']['original'] and
                #            'poster_default' not in movie_en['posters']['original']):
                #             image_en = Image.open(StringIO(requests.get(movie_en['posters']['original']).content))
                #
                #             image_en_path = 'cinema/{}/{}'.format(
                #                 datetime.now().strftime('%Y/%m'),
                #                 os.path.basename(movie_en['posters']['original']).replace('.jpg', '.jpeg')
                #             )
                #             try:
                #
                #                 os.path.join(image_en.save(settings.MEDIA_ROOT, image_en_path))
                #             except IOError:
                #                 image_en_path = ''
                #         else:
                #             image_en_path = ''
                #
                #         params.update({
                #             'long_desc_en': movie_en['synopsis'],
                #             'poster_en': image_en_path,
                #             'actors_en': ', '.join([actor['name'] for actor in movie_en['abridged_cast']]),
                #         })
                #     except (KeyError, IndexError):
                #         pass
                #
                #     Movie.objects.create(**params)
                # elif mov[0].rating == '0.0':
                #     mov = mov[0]
                #     try:
                #         rating = requests.get(
                #             os.path.join(settings.KINOPOISK_RATING_URL, mov.title_ru),
                #             headers={'User-Agent': agent}
                #         ).json().get('rating', '0.0')
                #     except (ValueError, KeyError):
                #         rating = '0.0'
                #     if rating == '0.0':
                #         try:
                #             rating = requests.get(
                #                 os.path.join(settings.KINOPOISK_RATING_URL, mov.title_en),
                #                 headers={'User-Agent': agent}
                #             ).json().get('rating', '0.0')
                #         except (ValueError, KeyError, AttributeError):
                #             rating = '0.0'
                #     mov.rating = rating
                #     mov.save(update_fields=['rating', ])
                # bulk = []
                #
                # for schedule in item['schedules']:
                #     if not MovieSchedule.objects.filter(
                #             movie=Movie.objects.get(kinohod_id=item['movie']['id']),
                #             time=schedule['time'],
                #             date=today
                #     ).exists():
                #         bulk.append(
                #             MovieSchedule(
                #                 movie=Movie.objects.get(kinohod_id=item['movie']['id']),
                #                 time=schedule['time'],
                #                 date=today,
                #                 kinohod_id=schedule['id']
                #             )
                #         )
                # if bulk:
                #     MovieSchedule.objects.bulk_create(bulk)
                # print 'now: %s, schedule day: %s, schedule bulk length: %s' % (datetime.now(), today, len(bulk))

            # today += timedelta(days=1)
