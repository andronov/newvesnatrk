from django.conf.urls import patterns, url


urlpatterns = patterns(
    'spring.cinema.views',
    url(r'^$', 'cinema_list'),
    url(r'^(?P<pk>\d+)/$', 'cinema_detail'),
)
