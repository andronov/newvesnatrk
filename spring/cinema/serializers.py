from __future__ import absolute_import
from django.utils.translation import get_language
from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import ModelSerializer
from spring.cinema.models import Movie
from utilities.fields import SerializerLocaleField


class CinemaSerializer(ModelSerializer):
    poster = SerializerMethodField('get_poster')
    circle_image = SerializerMethodField('get_circle_image')
    title = SerializerLocaleField()
    short_desc = SerializerLocaleField()
    long_desc = SerializerLocaleField()
    genre = SerializerLocaleField()
    super_poster = SerializerLocaleField()
    countries = SerializerLocaleField()
    actors = SerializerLocaleField()
    producers = SerializerLocaleField()
    directors = SerializerLocaleField()
    scenario = SerializerLocaleField()
    now = SerializerMethodField('get_now')
    sorted_schedule = SerializerMethodField('get_sorted_schedule')
    small_poster = SerializerMethodField('get_small_poster')
    big_poster = SerializerMethodField('get_big_poster')

    def get_image_field(self, obj, field_name):
        try:
            return getattr(obj, field_name).url
        except (AttributeError, KeyError, ValueError):
            return ''

    def get_small_poster(self, obj):
        return self.get_image_field(obj, 'small_poster')

    def get_big_poster(self, obj):
        return self.get_image_field(obj, 'big_poster')

    def get_now(self, obj):
        return obj.now

    def get_sorted_schedule(self, obj):
        return obj.sorted_schedule

    def get_poster(self, obj):
        return self.get_image_field(obj, 'poster_{}'.format(get_language()))

    def get_circle_image(self, obj):
        return self.get_image_field(obj, 'circle_image')

    class Meta:
        model = Movie
        fields = (
            'title', 'short_desc', 'long_desc', 'genre', 'super_poster', 'poster', 'countries', 'actors', 'producers',
            'directors', 'scenario', 'circle_image', 'duration', 'age_restriction', 'id', 'is_premiere',
            'now', 'rating', 'sorted_schedule'
        )


class CinemaToContentSerializer(CinemaSerializer):
    main_page_poster = SerializerMethodField('get_main_page_poster')

    def get_main_page_poster(self, obj):
        return self.get_image_field(obj, 'main_page_poster')

    class Meta(CinemaSerializer.Meta):
        fields = CinemaSerializer.Meta.fields + ('kinohod_id', 'main_page_poster', 'small_poster', 'big_poster')


class CinemaToSearchSerializer(CinemaSerializer):
    type = SerializerMethodField('get_type')

    def get_type(self, obj):
        return obj.type

    class Meta(CinemaSerializer.Meta):
        fields = CinemaSerializer.Meta.fields + ('kinohod_id', 'type', 'small_poster', 'big_poster')
