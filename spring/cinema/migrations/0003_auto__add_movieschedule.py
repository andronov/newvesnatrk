# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'MovieSchedule'
        db.create_table(u'cinema_movieschedule', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('movie', self.gf('django.db.models.fields.related.ForeignKey')(related_name='schedule', to=orm['cinema.Movie'])),
            ('time', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('date', self.gf('django.db.models.fields.DateField')()),
        ))
        db.send_create_signal(u'cinema', ['MovieSchedule'])


    def backwards(self, orm):
        # Deleting model 'MovieSchedule'
        db.delete_table(u'cinema_movieschedule')


    models = {
        u'cinema.movie': {
            'Meta': {'object_name': 'Movie'},
            'actors_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'actors_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'age_restriction': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'countries_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'countries_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'directors_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'directors_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'duration': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'genre_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'genre_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_premiere': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'kinohod_id': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'long_desc_en': ('ckeditor.fields.RichTextField', [], {'max_length': '2000', 'null': 'True', 'blank': 'True'}),
            'long_desc_ru': ('ckeditor.fields.RichTextField', [], {'max_length': '2000', 'null': 'True'}),
            'poster_en': ('filebrowser.fields.FileBrowseField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'poster_ru': ('filebrowser.fields.FileBrowseField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'producers_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'producers_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'rating': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'scenario_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'scenario_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'short_desc_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'short_desc_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'super_poster_en': ('filebrowser.fields.FileBrowseField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'super_poster_ru': ('filebrowser.fields.FileBrowseField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'})
        },
        u'cinema.movieschedule': {
            'Meta': {'object_name': 'MovieSchedule'},
            'date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'movie': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'schedule'", 'to': u"orm['cinema.Movie']"}),
            'time': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['cinema']