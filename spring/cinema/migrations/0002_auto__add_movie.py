# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Movie'
        db.create_table(u'cinema_movie', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('kinohod_id', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('title_ru', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('title_en', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('duration', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('short_desc_ru', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('short_desc_en', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('long_desc_ru', self.gf('ckeditor.fields.RichTextField')(max_length=2000, null=True)),
            ('long_desc_en', self.gf('ckeditor.fields.RichTextField')(max_length=2000, null=True, blank=True)),
            ('age_restriction', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('rating', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('genre_ru', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('genre_en', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('super_poster_ru', self.gf('filebrowser.fields.FileBrowseField')(max_length=255, null=True, blank=True)),
            ('super_poster_en', self.gf('filebrowser.fields.FileBrowseField')(max_length=255, null=True, blank=True)),
            ('poster_ru', self.gf('filebrowser.fields.FileBrowseField')(max_length=255, null=True, blank=True)),
            ('poster_en', self.gf('filebrowser.fields.FileBrowseField')(max_length=255, null=True, blank=True)),
            ('countries_ru', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('countries_en', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('actors_ru', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('actors_en', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('producers_ru', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('producers_en', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('directors_ru', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('directors_en', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('scenario_ru', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('scenario_en', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('is_premiere', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'cinema', ['Movie'])


    def backwards(self, orm):
        # Deleting model 'Movie'
        db.delete_table(u'cinema_movie')


    models = {
        u'cinema.movie': {
            'Meta': {'object_name': 'Movie'},
            'actors_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'actors_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'age_restriction': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'countries_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'countries_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'directors_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'directors_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'duration': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'genre_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'genre_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_premiere': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'kinohod_id': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'long_desc_en': ('ckeditor.fields.RichTextField', [], {'max_length': '2000', 'null': 'True', 'blank': 'True'}),
            'long_desc_ru': ('ckeditor.fields.RichTextField', [], {'max_length': '2000', 'null': 'True'}),
            'poster_en': ('filebrowser.fields.FileBrowseField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'poster_ru': ('filebrowser.fields.FileBrowseField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'producers_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'producers_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'rating': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'scenario_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'scenario_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'short_desc_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'short_desc_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'super_poster_en': ('filebrowser.fields.FileBrowseField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'super_poster_ru': ('filebrowser.fields.FileBrowseField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'})
        }
    }

    complete_apps = ['cinema']