# -*- coding: utf-8 -*-
from __future__ import absolute_import
from datetime import datetime
from django.template.defaultfilters import date as date_format
from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.response import Response
from spring.cinema.models import Movie
from spring.cinema.serializers import CinemaToContentSerializer
from spring.cinema.utilities import get_human_title


class CinemaList(ListAPIView):
    model = Movie
    serializer_class = CinemaToContentSerializer

    def __init__(self, *args, **kwargs):
        super(CinemaList, self).__init__(*args, **kwargs)
        self.common_schedule = None
        self.object_list = None

    def get_queryset(self):
        queryset = super(CinemaList, self).get_queryset().filter(schedule__date__gte=datetime.today()).distinct()
        for item in queryset:
            self.common_schedule = [shed.date for shed in item.schedule.filter(
                date__gte=datetime.today()
            ).order_by('date')]
        return queryset

    def list(self, request, *args, **kwargs):
        self.object_list = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(self.object_list, many=True)

        if self.common_schedule is not None:
            schedule_days = [
                {
                    'human_title': get_human_title(item),
                    'date_title': date_format(item, 'j E'),
                    'string_format': str(item)
                }
                for item in sorted(set(self.common_schedule))
            ]
        else:
            schedule_days = []

        return Response({
            'object_list': serializer.data,
            'schedule_days': schedule_days
        })
cinema_list = CinemaList.as_view()


class CinemaDetail(RetrieveAPIView):
    model = Movie
    serializer_class = CinemaToContentSerializer

    def __init__(self, *args, **kwargs):
        super(CinemaDetail, self).__init__(*args, **kwargs)
        self.object = None

    def retrieve(self, request, *args, **kwargs):
        self.object = self.get_object()
        serializer = self.get_serializer(self.object)
        return Response({
            'object': serializer.data
        })
cinema_detail = CinemaDetail.as_view()
