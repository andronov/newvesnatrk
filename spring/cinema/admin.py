# -*- coding: utf-8 -*-
from django.contrib import admin
from spring.cinema.models import Movie, MovieSchedule


class MovieScheduleInline(admin.TabularInline):
    model = MovieSchedule


class MovieAdmin(admin.ModelAdmin):
    inlines = (MovieScheduleInline,)
    list_filter = ('is_premiere',)

admin.site.register(Movie, MovieAdmin)
