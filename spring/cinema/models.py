# -*- coding: utf-8 -*-

import os
from datetime import datetime, date
from django.db import models
from django.conf import settings
from django.template.defaultfilters import date as date_format
from django.utils.translation import get_language, ugettext_lazy as _
from easymode.i18n.decorators import I18n
from ckeditor.fields import RichTextField
from filebrowser.fields import FileBrowseField
from spring.cinema.utilities import get_human_title
from spring.shops.models import ModelDiffMixin


MOVIE_DIR = 'cinema/{}'.format(datetime.now().strftime('%Y/%m'))

if not os.path.exists(settings.MEDIA_ROOT + MOVIE_DIR):
    os.makedirs(settings.MEDIA_ROOT + MOVIE_DIR)


@I18n('title', 'short_desc', 'long_desc', 'genre', 'super_poster', 'poster', 'countries', 'actors', 'producers', 'directors', 'scenario')
class Movie(ModelDiffMixin, models.Model):
    kinohod_id = models.CharField(_('Kinohod ID'), max_length=255)
    title = models.CharField(_('Title'), max_length=255)
    duration = models.CharField(_('Duration'), max_length=255)
    short_desc = models.CharField(_('Short Desc'), max_length=255)
    long_desc = RichTextField(_('Description'), config_name='big', max_length=2000)
    age_restriction = models.CharField(_('Age Restriction'), max_length=255)
    rating = models.CharField(_('Rating'), max_length=255)
    genre = models.CharField(_('Genre'), max_length=255, blank=True)
    super_poster = FileBrowseField(verbose_name=_('Super Poster'), directory=MOVIE_DIR, format='image', max_length=255, blank=True)
    poster = FileBrowseField(verbose_name=_('Poster'), directory=MOVIE_DIR, format='image', max_length=255, blank=True)
    circle_image = FileBrowseField(verbose_name=_('Circle image (225x225)'), directory=MOVIE_DIR, format='image', max_length=255, blank=True, null=True)
    countries = models.CharField(_('Countries'), max_length=255)
    actors = models.CharField(_('Actors'), max_length=255)
    producers = models.CharField(_('Producers'), max_length=255)
    directors = models.CharField(_('Directors'), max_length=255)
    scenario = models.CharField(_('Scenario'), max_length=255, blank=True)
    is_premiere = models.BooleanField(default=False)

    @property
    def type(self):
        return 'movie'

    @property
    def now(self):
        return {
            'date': datetime.now().strftime('%Y-%m-%d'),
            'time': datetime.now().strftime('%H:%M:%S'),
        }

    @property
    def big_poster(self):
        poster = getattr(self, 'poster_{}'.format(get_language()))
        if poster is None or isinstance(poster, basestring):
            if get_language() == 'en':
                poster = getattr(self, 'poster_ru')
                if poster is None or isinstance(poster, basestring):
                    return
                else:
                    return poster.version_generate('big_poster') if poster.exists() else None
            return
        return poster.version_generate('big_poster') if poster.exists() else None

    @property
    def small_poster(self):
        poster = getattr(self, 'poster_{}'.format(get_language()))
        if poster is None or isinstance(poster, basestring):
            if get_language() == 'en':
                poster = getattr(self, 'poster_ru')
                if poster is None or isinstance(poster, basestring):
                    return
                else:
                    return poster.version_generate('small_poster') if poster.exists() else None
            return
        return poster.version_generate('small_poster') if poster.exists() else None

    @property
    def main_page_poster(self):
        poster = getattr(self, 'poster_{}'.format(get_language()))
        if poster is None or isinstance(poster, basestring):
            if get_language() == 'en':
                poster = getattr(self, 'poster_ru')
                if poster is None or isinstance(poster, basestring):
                    return
                else:
                    return poster.version_generate('main_page_poster') if poster.exists() else None
            return
        return poster.version_generate('main_page_poster') if poster.exists() else None

    @property
    def entertainment_page_poster(self):
        poster = getattr(self, 'poster_{}'.format(get_language()))
        if poster is None or isinstance(poster, basestring):
            if get_language() == 'en':
                poster = getattr(self, 'poster_ru')
                if poster is None or isinstance(poster, basestring):
                    return
                else:
                    return poster.version_generate('entertainment_page_poster') if poster.exists() else None
            return
        return poster.version_generate('entertainment_page_poster') if poster.exists() else None

    @property
    def mail_image(self):
        poster = self.circle_image
        if poster is None or isinstance(poster, basestring):
            if get_language() == 'en':
                poster = getattr(self, 'poster_ru')
                if poster is None or isinstance(poster, basestring):
                    return
                else:
                    return poster.version_generate('mail_poster') if poster.exists() else None
            return
        return poster.version_generate('mail_poster') if poster.exists() else None

    @property
    def sorted_schedule(self):
        sorted_schedule = {}

        for item in self.schedule.filter(date__gte=date.today()).order_by('time'):

            if date.today() > item.date:
                continue

            if not str(item.date) in sorted_schedule:
                sorted_schedule[str(item.date)] = {}

            if not 'date_title' in sorted_schedule[str(item.date)]:
                sorted_schedule[str(item.date)]['date_title'] = date_format(item.date, 'j E')

            if not 'human_title' in sorted_schedule[str(item.date)]:
                sorted_schedule[str(item.date)]['human_title'] = get_human_title(item.date)

            if not 'time' in sorted_schedule[str(item.date)]:
                sorted_schedule[str(item.date)]['time'] = []

            sorted_schedule[str(item.date)]['time'].append({'schedule_id': item.kinohod_id, 'time': item.time})

        for key, value in sorted_schedule.items():
            if u'00:' in sorted_schedule[key]['time'][0]['time']:
                item = sorted_schedule[key]['time'][0]
                del sorted_schedule[key]['time'][0]
                sorted_schedule[key]['time'].append(item)
        return sorted_schedule

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = _('Movie')
        verbose_name_plural = _('Movies')


class MovieSchedule(models.Model):
    movie = models.ForeignKey(Movie, verbose_name=_('Movie'), related_name='schedule')
    time = models.CharField(_('Time'), max_length=255, db_index=True)
    date = models.DateField(_('Date'), db_index=True)
    kinohod_id = models.CharField(_('Kinohod ID'), max_length=255, default='')

    def __unicode__(self):
        return '{} {}'.format(self.date, self.time)

    class Meta:
        verbose_name = _('Schedule')
        verbose_name_plural = _('Schedules')
