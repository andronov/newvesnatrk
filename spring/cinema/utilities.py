# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.template.defaultfilters import date as date_format
from django.utils.translation import ugettext as _
from datetime import timedelta
import requests
from django.conf import settings


class KinohodAPI(object):
    cinema_id = settings.KINOHOD_CINEMA_ID
    server = settings.KINOHOD_SERVER_URL
    api_key = settings.KINOHOD_API_KEY
    api_url = '{server}/api/rest/partner/v1/{method}?apikey={api_key}&{modifiers}'

    def request(self, method, modifiers=''):
        print (self.api_url.format(
            server=self.server,
            method=method,
            api_key=self.api_key,
            modifiers=modifiers))
        response = requests.get(self.api_url.format(
            server=self.server,
            method=method,
            api_key=self.api_key,
            modifiers=modifiers)
        )

        if response.status_code == 200:
            return response.json()

        return response

    def get_schedules(self, date):
        return self.request(
            'cinemas/{cinema_id}/schedules'.format(cinema_id=self.cinema_id),
            modifiers='date={}'.format(date.strftime('%d%m%Y'))
        )

    def get_movie(self, movie_id):
        return self.request('movies/{movie_id}'.format(movie_id=movie_id))

    def get_movie_poster(self, filename, width, height):
        return '{server}/p/{width}x{height}/{ab}/{cd}/{filename}'.format(
            server=self.server,
            width=width,
            height=height,
            ab=filename[:2],
            cd=filename[2:4],
            filename=filename
        )

class LuxorAPI(object):
    service_id = '116433711'
    server = '212.248.42.242'
    server_port = 9195
    api_key = settings.KINOHOD_API_KEY
    api_url = 'Version=3&QueryCode={query_code}&ServiceID={service_id}&{modifiers}'

    def request(self, method, modifiers=''):
        import socket

        params = self.api_url.format(
            query_code=method,
            service_id=self.service_id,
            modifiers=modifiers)

        length = str(len(params)).zfill(10)

        params = length + '&' + params



        client_socket = socket.socket()
        client_socket.connect((self.server, self.server_port))
        client_socket.send(params)




        data_list = []
        while True:
            data = client_socket.recv(1024)
            if not data:
                break
            else:
                data_list.append(data.decode('windows-1251'))

        client_socket.close()

        # print(data_list)
        xml_data = "".join(data_list)

        f = open(method + '.xml', 'w')
        f.write(xml_data.encode('windows-1251'))
        f.close()

        # response = requests.get()



        # if response.status_code == 200:
        #     return response.json()

        # return response

        return data

    def get_schedules(self, date):
        return self.request(
            'GetMovies',
            modifiers='DateList={}&ListType=PropertiesShow'.format(date.strftime('%d.%m.%Y')),
        )

    def get_movie(self, movie_id):
        return self.request('movies/{movie_id}'.format(movie_id=movie_id))

    def get_movie_poster(self, filename, width, height):
        return '{server}/p/{width}x{height}/{ab}/{cd}/{filename}'.format(
            server=self.server,
            width=width,
            height=height,
            ab=filename[:2],
            cd=filename[2:4],
            filename=filename
        )


class RottenTomatoesAPI(object):
    server = settings.TOMATOES_SERVER_URL
    api_key = settings.TOMATOES_API_KEY
    api_url = '{server}/api/public/v1.0/{method}.json?apikey={api_key}&{modifiers}'

    def request(self, method, modifiers=''):
        response = requests.get(self.api_url.format(
            server=self.server,
            method=method,
            api_key=self.api_key,
            modifiers=modifiers)
        )

        if response.status_code == 200:
            return response.json()

        return response

    def get_movie(self, title):
        return self.request('movies', modifiers='q={}'.format(title))


def get_human_title(date):
    if date.today() == date:
        return _('Today')
    elif date.today() + timedelta(days=+1) == date:
        return _('Tomorrow')
    elif date.today() + timedelta(days=+2) == date:
        return _('The day after tomorrow')
    else:
        return date_format(date, 'l')
