# -*- coding: utf-8 -*-

from django.contrib import admin
from spring.mainpage.models import Content, MainBanners, CinemaContent, EntertainmentsContent


admin.site.register(Content)
admin.site.register(CinemaContent)
admin.site.register(EntertainmentsContent)
admin.site.register(MainBanners)
