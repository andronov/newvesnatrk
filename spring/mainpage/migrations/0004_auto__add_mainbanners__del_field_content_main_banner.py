# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'MainBanners'
        db.create_table(u'mainpage_mainbanners', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('main_banner', self.gf('filebrowser.fields.FileBrowseField')(max_length=255, null=True, blank=True)),
        ))
        db.send_create_signal(u'mainpage', ['MainBanners'])

        # Deleting field 'Content.main_banner'
        db.delete_column(u'mainpage_content', 'main_banner')

        # Adding M2M table for field main_banner on 'Content'
        m2m_table_name = db.shorten_name(u'mainpage_content_main_banner')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('content', models.ForeignKey(orm[u'mainpage.content'], null=False)),
            ('mainbanners', models.ForeignKey(orm[u'mainpage.mainbanners'], null=False))
        ))
        db.create_unique(m2m_table_name, ['content_id', 'mainbanners_id'])


    def backwards(self, orm):
        # Deleting model 'MainBanners'
        db.delete_table(u'mainpage_mainbanners')

        # Adding field 'Content.main_banner'
        db.add_column(u'mainpage_content', 'main_banner',
                      self.gf('filebrowser.fields.FileBrowseField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Removing M2M table for field main_banner on 'Content'
        db.delete_table(db.shorten_name(u'mainpage_content_main_banner'))


    models = {
        u'mainpage.content': {
            'Meta': {'object_name': 'Content'},
            'how_to_get_text_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'how_to_get_text_ru': ('ckeditor.fields.RichTextField', [], {'null': 'True'}),
            'how_to_get_title_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'how_to_get_title_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'main_banner': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'welcome_banners'", 'symmetrical': 'False', 'to': u"orm['mainpage.MainBanners']"}),
            'ok_banner': ('filebrowser.fields.FileBrowseField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'stage_map_banner': ('filebrowser.fields.FileBrowseField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'working_time_begin': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'working_time_end': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        u'mainpage.mainbanners': {
            'Meta': {'object_name': 'MainBanners'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'main_banner': ('filebrowser.fields.FileBrowseField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['mainpage']