# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'EntertainmentsContent.main_title_ru'
        db.add_column(u'mainpage_entertainmentscontent', 'main_title_ru',
                      self.gf('django.db.models.fields.CharField')(max_length=256, null=True),
                      keep_default=False)

        # Adding field 'EntertainmentsContent.main_title_en'
        db.add_column(u'mainpage_entertainmentscontent', 'main_title_en',
                      self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'EntertainmentsContent.main_title_ru'
        db.delete_column(u'mainpage_entertainmentscontent', 'main_title_ru')

        # Deleting field 'EntertainmentsContent.main_title_en'
        db.delete_column(u'mainpage_entertainmentscontent', 'main_title_en')


    models = {
        u'mainpage.cinemacontent': {
            'Meta': {'object_name': 'CinemaContent'},
            'bottom_text_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'bottom_text_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'middle_text_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'middle_text_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'top_text_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'top_text_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True'})
        },
        u'mainpage.content': {
            'Meta': {'object_name': 'Content'},
            'how_to_get_text_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'how_to_get_text_ru': ('ckeditor.fields.RichTextField', [], {'null': 'True'}),
            'how_to_get_title_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'how_to_get_title_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'main_banner': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'welcome_banners'", 'symmetrical': 'False', 'to': u"orm['mainpage.MainBanners']"}),
            'ok_banner': ('filebrowser.fields.FileBrowseField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'stage_map_banner': ('filebrowser.fields.FileBrowseField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'working_time_begin': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'working_time_end': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        u'mainpage.entertainmentscontent': {
            'Meta': {'object_name': 'EntertainmentsContent'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'main_text_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'main_text_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True'}),
            'main_title_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'main_title_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True'})
        },
        u'mainpage.mainbanners': {
            'Meta': {'object_name': 'MainBanners'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'main_banner': ('filebrowser.fields.FileBrowseField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['mainpage']