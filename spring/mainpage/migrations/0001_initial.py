# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Content'
        db.create_table(u'mainpage_content', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('main_banner', self.gf('filebrowser.fields.FileBrowseField')(max_length=255, null=True, blank=True)),
            ('working_time', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('stage_map_banner', self.gf('filebrowser.fields.FileBrowseField')(max_length=255, null=True, blank=True)),
            ('ok_banner', self.gf('filebrowser.fields.FileBrowseField')(max_length=255, null=True, blank=True)),
            ('how_to_get_title_ru', self.gf('django.db.models.fields.CharField')(max_length=256, null=True)),
            ('how_to_get_title_en', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('how_to_get_text_ru', self.gf('ckeditor.fields.RichTextField')(null=True)),
            ('how_to_get_text_en', self.gf('ckeditor.fields.RichTextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'mainpage', ['Content'])


    def backwards(self, orm):
        # Deleting model 'Content'
        db.delete_table(u'mainpage_content')


    models = {
        u'mainpage.content': {
            'Meta': {'object_name': 'Content'},
            'how_to_get_text_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'how_to_get_text_ru': ('ckeditor.fields.RichTextField', [], {'null': 'True'}),
            'how_to_get_title_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'how_to_get_title_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'main_banner': ('filebrowser.fields.FileBrowseField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'ok_banner': ('filebrowser.fields.FileBrowseField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'stage_map_banner': ('filebrowser.fields.FileBrowseField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'working_time': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        }
    }

    complete_apps = ['mainpage']