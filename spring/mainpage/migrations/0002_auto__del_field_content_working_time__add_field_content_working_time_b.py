# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Content.working_time'
        db.delete_column(u'mainpage_content', 'working_time')

        # Adding field 'Content.working_time_begin'
        db.add_column(u'mainpage_content', 'working_time_begin',
                      self.gf('django.db.models.fields.CharField')(default=1, max_length=256),
                      keep_default=False)

        # Adding field 'Content.working_time_end'
        db.add_column(u'mainpage_content', 'working_time_end',
                      self.gf('django.db.models.fields.CharField')(default=1, max_length=256),
                      keep_default=False)


    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'Content.working_time'
        raise RuntimeError("Cannot reverse this migration. 'Content.working_time' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Content.working_time'
        db.add_column(u'mainpage_content', 'working_time',
                      self.gf('django.db.models.fields.CharField')(max_length=256),
                      keep_default=False)

        # Deleting field 'Content.working_time_begin'
        db.delete_column(u'mainpage_content', 'working_time_begin')

        # Deleting field 'Content.working_time_end'
        db.delete_column(u'mainpage_content', 'working_time_end')


    models = {
        u'mainpage.content': {
            'Meta': {'object_name': 'Content'},
            'how_to_get_text_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'how_to_get_text_ru': ('ckeditor.fields.RichTextField', [], {'null': 'True'}),
            'how_to_get_title_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'how_to_get_title_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'main_banner': ('filebrowser.fields.FileBrowseField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'ok_banner': ('filebrowser.fields.FileBrowseField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'stage_map_banner': ('filebrowser.fields.FileBrowseField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'working_time_begin': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'working_time_end': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        }
    }

    complete_apps = ['mainpage']