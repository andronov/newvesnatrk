# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Content.hypermarkt_working_time_begin'
        db.add_column(u'mainpage_content', 'hypermarkt_working_time_begin',
                      self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Content.hypermarkt_working_time_end'
        db.add_column(u'mainpage_content', 'hypermarkt_working_time_end',
                      self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Content.restaurants_working_time_begin'
        db.add_column(u'mainpage_content', 'restaurants_working_time_begin',
                      self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Content.restaurants_working_time_end'
        db.add_column(u'mainpage_content', 'restaurants_working_time_end',
                      self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Content.cinema_working_time_begin'
        db.add_column(u'mainpage_content', 'cinema_working_time_begin',
                      self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Content.cinema_working_time_end'
        db.add_column(u'mainpage_content', 'cinema_working_time_end',
                      self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Content.hypermarkt_working_time_begin'
        db.delete_column(u'mainpage_content', 'hypermarkt_working_time_begin')

        # Deleting field 'Content.hypermarkt_working_time_end'
        db.delete_column(u'mainpage_content', 'hypermarkt_working_time_end')

        # Deleting field 'Content.restaurants_working_time_begin'
        db.delete_column(u'mainpage_content', 'restaurants_working_time_begin')

        # Deleting field 'Content.restaurants_working_time_end'
        db.delete_column(u'mainpage_content', 'restaurants_working_time_end')

        # Deleting field 'Content.cinema_working_time_begin'
        db.delete_column(u'mainpage_content', 'cinema_working_time_begin')

        # Deleting field 'Content.cinema_working_time_end'
        db.delete_column(u'mainpage_content', 'cinema_working_time_end')


    models = {
        u'mainpage.cinemacontent': {
            'Meta': {'object_name': 'CinemaContent'},
            'bottom_text_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'bottom_text_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'middle_text_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'middle_text_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'top_text_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'top_text_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True'})
        },
        u'mainpage.content': {
            'Meta': {'object_name': 'Content'},
            'cinema_working_time_begin': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'cinema_working_time_end': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'how_to_get_text_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'how_to_get_text_ru': ('ckeditor.fields.RichTextField', [], {'null': 'True'}),
            'how_to_get_title_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'how_to_get_title_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True'}),
            'hypermarkt_working_time_begin': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'hypermarkt_working_time_end': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ok_banner_en': ('filebrowser.fields.FileBrowseField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'ok_banner_ru': ('filebrowser.fields.FileBrowseField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'ok_url': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['shops.Brand']", 'to_field': "'url'", 'null': 'True', 'blank': 'True'}),
            'restaurants_working_time_begin': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'restaurants_working_time_end': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'stage_map_banner': ('filebrowser.fields.FileBrowseField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'working_time_begin': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'working_time_end': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        u'mainpage.entertainmentscontent': {
            'Meta': {'object_name': 'EntertainmentsContent'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'main_text_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'main_text_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True'}),
            'main_title_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'main_title_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True'}),
            'today_in_film_text_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'today_in_film_text_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True'})
        },
        u'mainpage.mainbanners': {
            'Meta': {'object_name': 'MainBanners'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'main_banner_en': ('filebrowser.fields.FileBrowseField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'main_banner_ru': ('filebrowser.fields.FileBrowseField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'shops.brand': {
            'Meta': {'ordering': "['place_number', '-pk']", 'object_name': 'Brand'},
            'categories': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'brands'", 'symmetrical': 'False', 'to': u"orm['shops.BrandCategory']"}),
            'color': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'brands'", 'null': 'True', 'to': u"orm['shops.Color']"}),
            'contacts': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'description_en': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'description_ru': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo_en': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'logo_ru': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'}),
            'number': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '256'}),
            'open_soon': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'place_number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1000'}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'url': ('spring.shops.fields.NullableURLField', [], {'default': 'None', 'max_length': '128', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'views_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'work_time': ('django.db.models.fields.CharField', [], {'default': "'10:00 - 19:00'", 'max_length': '13'})
        },
        u'shops.brandcategory': {
            'Meta': {'ordering': "('position',)", 'object_name': 'BrandCategory'},
            'banner': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'banner_url': ('django.db.models.fields.URLField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'for_children': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'}),
            'position': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        },
        u'shops.color': {
            'Meta': {'object_name': 'Color'},
            'circle_color': ('colorful.fields.RGBColorField', [], {'max_length': '7'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'promotion_color': ('colorful.fields.RGBColorField', [], {'max_length': '7'})
        }
    }

    complete_apps = ['mainpage']