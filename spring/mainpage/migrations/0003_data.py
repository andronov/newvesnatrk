# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import DataMigration
from django.db import models

class Migration(DataMigration):

    def forwards(self, orm):
        "Write your forwards methods here."
        item = orm['mainpage.Content']()
        item.how_to_get_text = 'ewq'
        item.how_to_get_title = 'tre'
        item.main_banner = 'gdf'
        item.ok_banner = 'bcv'
        item.stage_map_banner = 'vnet'
        item.working_time_begin = 'vqfe'
        item.working_time_end = 'nyver'
        item.save()

    def backwards(self, orm):
        "Write your backwards methods here."
        orm['mainpage.Content'].objects.all().delete()

    models = {
        u'mainpage.content': {
            'Meta': {'object_name': 'Content'},
            'how_to_get_text_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'how_to_get_text_ru': ('ckeditor.fields.RichTextField', [], {'null': 'True'}),
            'how_to_get_title_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'how_to_get_title_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'main_banner': ('filebrowser.fields.FileBrowseField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'ok_banner': ('filebrowser.fields.FileBrowseField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'stage_map_banner': ('filebrowser.fields.FileBrowseField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'working_time_begin': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'working_time_end': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        }
    }

    complete_apps = ['mainpage']
    symmetrical = True
