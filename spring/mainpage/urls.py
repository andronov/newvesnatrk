from django.conf.urls import patterns, url


urlpatterns = patterns(
    'spring.mainpage.views',
    url(r'^main/$', 'main_page_content'),
    url(r'^cinema/$', 'main_page_cinema_content')
)
