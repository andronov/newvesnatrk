# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _, get_language
from django.db import models
from ckeditor.fields import RichTextField
from easymode.i18n.decorators import I18n
from filebrowser.fields import FileBrowseField
from spring.shops.models import Brand
from spring.user import rename


@I18n('main_banner')
class MainBanners(models.Model):
    main_banner = FileBrowseField(verbose_name=_('Welcome banner'), directory='mainpage/', format='image', max_length=255, blank=True, null=True)
    url = models.URLField(_('URL'), max_length=512, default='', blank=True)
    is_published = models.BooleanField(_('Is published'), default=False)
    show_on_second_screen = models.BooleanField(_('Show this banner on second screen'), default=False)

    def __unicode__(self):
        banner = getattr(self, 'main_banner_{}'.format(get_language()), None)
        return banner.url if banner else 'None'


@I18n('how_to_get_title', 'how_to_get_text', 'ok_banner')
class Content(models.Model):
    working_time_begin = models.CharField(max_length=256, verbose_name=_('Working time begin'))
    working_time_end = models.CharField(max_length=256, verbose_name=_('Working time end'))
    stage_map_banner = FileBrowseField(verbose_name=_('Stage map banner'), directory='mainpage/', format='image', max_length=255, blank=True, null=True)
    ok_banner = FileBrowseField(verbose_name=_('OK banner'), directory='mainpage/', format='image', max_length=255, blank=True, null=True)
    ok_url = models.ForeignKey(Brand, verbose_name=_('OK url'), to_field='url', blank=True, null=True)
    how_to_get_title = models.CharField(max_length=256, verbose_name=_('How to get'))
    how_to_get_text = RichTextField(verbose_name=_('How to get text'))
    hypermarkt_working_time_begin = models.CharField(
        max_length=256, verbose_name=_('Hypermarket working time begin'), blank=True, null=True
    )
    hypermarkt_working_time_end = models.CharField(
        max_length=256, verbose_name=_('Hypermarket working time end'), blank=True, null=True
    )
    restaurants_working_time_begin = models.CharField(
        max_length=256, verbose_name=_('Restaurants working time begin'), blank=True, null=True
    )
    restaurants_working_time_end = models.CharField(
        max_length=256, verbose_name=_('Restaurants working time end'), blank=True, null=True
    )
    cinema_working_time_begin = models.CharField(
        max_length=256, verbose_name=_('Cinema working time start'), blank=True, null=True
    )
    cinema_working_time_end = models.CharField(
        max_length=256, verbose_name=_('Cinema working time end'), blank=True, null=True
    )


@I18n('top_text', 'middle_text', 'bottom_text')
class CinemaContent(models.Model):
    top_text = models.CharField(max_length=256, verbose_name=_('Text on the top'))
    phone = models.CharField(max_length=256, verbose_name=_('Telephone'))
    middle_text = models.CharField(max_length=256, verbose_name=_('Text at the middle'))
    bottom_text = models.CharField(max_length=256, verbose_name=_('Text at the bottom'))


@I18n('main_text', 'main_title', 'today_in_film_text')
class EntertainmentsContent(models.Model):
    today_in_film_text = models.CharField(max_length=256, verbose_name=_('Today in cinema text'))
    main_text = models.CharField(max_length=256, verbose_name=_('Main text'))
    main_title = models.CharField(max_length=256, verbose_name=_('Main title'))