from django.utils.translation import get_language
from rest_framework.fields import SerializerMethodField, Field
from rest_framework.serializers import ModelSerializer
from spring.mainpage.models import EntertainmentsContent, Content, MainBanners, CinemaContent
from utilities.fields import SerializerLocaleField


class MainPageCinemaSerializer(ModelSerializer):
    top_text = SerializerLocaleField()
    bottom_text = SerializerLocaleField()
    middle_text = SerializerLocaleField()

    class Meta:
        model = CinemaContent
        fields = ('top_text', 'bottom_text', 'middle_text', 'phone')


class MainBannerSerializer(ModelSerializer):
    main_banner = SerializerMethodField('get_main_banner')

    def get_main_banner(self, obj):
        try:
            return getattr(obj, 'main_banner_{}'.format(get_language())).url
        except (AttributeError, KeyError, ValueError):
            return ''

    class Meta:
        model = MainBanners
        fields = ('is_published', 'main_banner', 'show_on_second_screen', 'url')


class EntertainmentsContentSerializer(ModelSerializer):
    today_in_film_text = SerializerLocaleField()
    main_text = SerializerLocaleField()
    main_title = SerializerLocaleField()

    class Meta:
        model = EntertainmentsContent
        fields = ('today_in_film_text', 'main_text', 'main_title', 'id')


class MainPageContentSerializer(ModelSerializer):
    how_to_get_title = SerializerLocaleField()
    how_to_get_text = SerializerLocaleField()
    ok_banner = SerializerMethodField('get_ok_banner')
    stage_map_banner = SerializerMethodField('get_stage_map_banner')
    url = Field(source='ok_url.url')

    def get_image_field(self, obj, field_name):
        try:
            return getattr(obj, field_name).url
        except (AttributeError, KeyError, ValueError):
            return ''

    def get_stage_map_banner(self, obj):
        return self.get_image_field(obj, 'stage_map_banner')

    def get_ok_banner(self, obj):
        return self.get_image_field(obj, 'ok_banner_{}'.format(get_language()))

    class Meta:
        model = Content
        fields = (
            'how_to_get_title', 'how_to_get_text', 'ok_banner', 'cinema_working_time_begin', 'cinema_working_time_end',
            'hypermarkt_working_time_begin', 'hypermarkt_working_time_end', 'id', 'restaurants_working_time_begin',
            'restaurants_working_time_end', 'stage_map_banner', 'url', 'working_time_begin', 'working_time_end'
        )
