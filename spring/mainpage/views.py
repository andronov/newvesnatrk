from datetime import datetime

from django.db.models import Q
from rest_framework.generics import RetrieveAPIView
from rest_framework.response import Response
from django.contrib.admin.models import LogEntry, ADDITION, CHANGE
from django.contrib.contenttypes.models import ContentType

from spring.cinema.models import Movie
from spring.cinema.serializers import CinemaToContentSerializer
from spring.entertainments.models import Entertainment
from spring.entertainments.serializers import EntertainmentToContentSerializer
from spring.events.models import BaseEvent
from spring.events.serializers import EventToContentSerializer
from spring.mainpage.models import Content, CinemaContent, MainBanners
from spring.mainpage.serializers import MainPageContentSerializer, MainBannerSerializer, MainPageCinemaSerializer
from spring.shops.models import Brand
from spring.shops.serializers import BrandToContentSerializer


class MainPageCinemaContent(RetrieveAPIView):
    model = CinemaContent
    serializer_class = MainPageCinemaSerializer

    def get_object(self, queryset=None):
        return self.model.objects.get()

    def retrieve(self, request, *args, **kwargs):
        serializer = self.serializer_class(self.get_object())

        return Response({
            'object': serializer.data
        })
main_page_cinema_content = MainPageCinemaContent.as_view()


class MainPageContent(RetrieveAPIView):
    model = Content
    serializer_class = MainPageContentSerializer

    def get_object(self, queryset=None):
        return self.model.objects.get()

    def retrieve(self, request, *args, **kwargs):
        serializer = self.serializer_class(self.get_object())
        #LOGIC FILTERS FOR MAIN PAGE
        brands = Brand.objects.filter(published=True)
        events = BaseEvent.objects.filter(is_published=True)
        entertainments = Entertainment.objects.filter(published=True)
        cinema = Movie.objects.exclude(
            ~Q(schedule__date=datetime.today())
        )

        show_on_main_brand_changes = LogEntry.objects.filter(
            content_type_id=ContentType.objects.get_for_model(Brand).pk,
            action_flag__in=[ADDITION, CHANGE],
            object_id__in=[item.pk for item in brands.filter(show_on_main=True)],
            change_message__icontains='show_on_main'
        ).order_by('-action_time')#[:2]

        show_on_main_brand_changes_custom = LogEntry.objects.filter(
            content_type_id=ContentType.objects.get_for_model(Brand).pk,
            action_flag__in=[ADDITION, CHANGE],
            object_id__in=[item.pk for item in brands.filter(show_on_main=True, logo_ru__isnull=False)],
            change_message__icontains='show_on_main'
        ).order_by('-action_time')

        if show_on_main_brand_changes_custom:
            main_page_brands_queryset_custom = brands.filter(show_on_main=True, logo_ru__isnull=False).order_by('place_number')
            #main_page_brands_queryset_custom = brands.filter(id__in=[item.object_id for item in show_on_main_brand_changes])
        else:
            main_page_brands_queryset_custom = brands.filter(show_on_main=True, logo_ru__isnull=False).order_by('-action_time')#[:10]

        if show_on_main_brand_changes:
            main_page_brands_queryset = brands.filter(id__in=[item.object_id for item in show_on_main_brand_changes])
        else:
            main_page_brands_queryset = brands.filter(show_on_main=True).order_by('-pk')#[:10]

        premiere_cinema_changes = LogEntry.objects.filter(
            content_type_id=ContentType.objects.get_for_model(Movie).pk,
            action_flag__in=[ADDITION, CHANGE],
            object_id__in=[item.pk for item in cinema.filter(is_premiere=True)],
            change_message__icontains='is_premiere'
        ).order_by('-action_time')[:1]

        if premiere_cinema_changes:
            cinema_premiere_queryset = cinema.filter(id__in=[item.object_id for item in premiere_cinema_changes])
        else:
            cinema_premiere_queryset = cinema.filter(is_premiere=True).order_by('-pk')[:1]

        return Response({
            'object': serializer.data,
            'banners': [MainBannerSerializer(item).data for item in MainBanners.objects.filter(is_published=True)],
            'main_tab': {
                'yellow': {
                    'brands': [BrandToContentSerializer(item).data for item in main_page_brands_queryset_custom]
                },
                'blue': {
                    'random_brands': [BrandToContentSerializer(item).data for item in brands.order_by('?')[:1]],
                    'event_brands': [
                        EventToContentSerializer(item).data for item in events.filter(
                            brand__isnull=False
                        ).distinct()[:4]
                    ],
                    'spring_events': [
                        EventToContentSerializer(item).data for item in events.filter(
                            springevent__isnull=False
                        ).order_by('?')[:1]
                    ]
                },
                'green': {
                    'random_brands': [BrandToContentSerializer(item).data for item in brands.order_by('?')[:4]],
                    'spring_events': [
                        EventToContentSerializer(item).data for item in events.filter(
                            springevent__isnull=False
                        ).order_by('?')[:3]
                    ],
                    'event_brands': [
                        EventToContentSerializer(item).data for item in events.filter(
                            brand__isnull=False
                        ).distinct()[:2]
                    ],
                }
            },
            'brands_tab': {
                'brands': [BrandToContentSerializer(item).data for item in brands.order_by('place_number')[:12]]
            },
            'children_tab': {
                'entertainments': [
                    EntertainmentToContentSerializer(item).data for item in entertainments.all()
                ]
            },
            'cinema_tab': {
                'premiere': [CinemaToContentSerializer(item).data for item in cinema_premiere_queryset],
                'random_cinemas': [
                    CinemaToContentSerializer(item).data for item in cinema.filter(
                        is_premiere=False
                    ).order_by('?')[:4]
                ]
            }
        })
main_page_content = MainPageContent.as_view()
