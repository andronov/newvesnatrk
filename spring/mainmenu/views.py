from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from spring.mainmenu.models import Menu, RightMenu
from spring.mainmenu.serializers import MenuSerializer, RightMenuSerializer


class MainMenuContent(ListAPIView):
    model = Menu
    serializer_class = MenuSerializer

    def list(self, request, *args, **kwargs):
        serializer = self.get_serializer(self.get_queryset(), many=True)

        return Response({
            'object_list': serializer.data
        })
main_menu_content = MainMenuContent.as_view()


class RightMainMenuContent(ListAPIView):
    model = RightMenu
    serializer_class = RightMenuSerializer

    def get_queryset(self):
        return self.model.objects.filter(parent__isnull=True)

    def list(self, request, *args, **kwargs):
        serializer = self.get_serializer(self.get_queryset(), many=True)

        return Response({
            'object_list': serializer.data
        })
main_right_menu_content = RightMainMenuContent.as_view()
