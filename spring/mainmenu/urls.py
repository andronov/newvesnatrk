from django.conf.urls import patterns, url


urlpatterns = patterns(
    'spring.mainmenu.views',
    url(r'^$', 'main_menu_content'),
    url(r'^right/$', 'main_right_menu_content'),
)
