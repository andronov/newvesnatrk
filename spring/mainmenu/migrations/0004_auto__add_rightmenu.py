# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'RightMenu'
        db.create_table(u'mainmenu_rightmenu', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('position', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
            ('title_ru', self.gf('django.db.models.fields.CharField')(max_length=256, null=True)),
            ('title_en', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
        ))
        db.send_create_signal(u'mainmenu', ['RightMenu'])

        # Adding M2M table for field sub_menus on 'RightMenu'
        m2m_table_name = db.shorten_name(u'mainmenu_rightmenu_sub_menus')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('from_rightmenu', models.ForeignKey(orm[u'mainmenu.rightmenu'], null=False)),
            ('to_rightmenu', models.ForeignKey(orm[u'mainmenu.rightmenu'], null=False))
        ))
        db.create_unique(m2m_table_name, ['from_rightmenu_id', 'to_rightmenu_id'])


    def backwards(self, orm):
        # Deleting model 'RightMenu'
        db.delete_table(u'mainmenu_rightmenu')

        # Removing M2M table for field sub_menus on 'RightMenu'
        db.delete_table(db.shorten_name(u'mainmenu_rightmenu_sub_menus'))


    models = {
        u'mainmenu.menu': {
            'Meta': {'ordering': "('position',)", 'object_name': 'Menu'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True'})
        },
        u'mainmenu.rightmenu': {
            'Meta': {'ordering': "('position',)", 'object_name': 'RightMenu'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'sub_menus': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'sub_menus_rel_+'", 'to': u"orm['mainmenu.RightMenu']"}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True'})
        }
    }

    complete_apps = ['mainmenu']