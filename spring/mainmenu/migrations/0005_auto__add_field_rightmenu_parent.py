# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'RightMenu.parent'
        db.add_column(u'mainmenu_rightmenu', 'parent',
                      self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='sub_menus', null=True, to=orm['mainmenu.RightMenu']),
                      keep_default=False)

        # Removing M2M table for field sub_menus on 'RightMenu'
        db.delete_table(db.shorten_name(u'mainmenu_rightmenu_sub_menus'))


    def backwards(self, orm):
        # Deleting field 'RightMenu.parent'
        db.delete_column(u'mainmenu_rightmenu', 'parent_id')

        # Adding M2M table for field sub_menus on 'RightMenu'
        m2m_table_name = db.shorten_name(u'mainmenu_rightmenu_sub_menus')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('from_rightmenu', models.ForeignKey(orm[u'mainmenu.rightmenu'], null=False)),
            ('to_rightmenu', models.ForeignKey(orm[u'mainmenu.rightmenu'], null=False))
        ))
        db.create_unique(m2m_table_name, ['from_rightmenu_id', 'to_rightmenu_id'])


    models = {
        u'mainmenu.menu': {
            'Meta': {'ordering': "('position',)", 'object_name': 'Menu'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True'})
        },
        u'mainmenu.rightmenu': {
            'Meta': {'ordering': "('position',)", 'object_name': 'RightMenu'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'sub_menus'", 'null': 'True', 'to': u"orm['mainmenu.RightMenu']"}),
            'position': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True'})
        }
    }

    complete_apps = ['mainmenu']