# -*- coding: utf-8 -*-

from django.contrib import admin
from spring.mainmenu.models import Menu, RightMenu
from utilities.capable.admin import OrderableAdmin


class MenuAdmin(OrderableAdmin):
    list_display_links = ('title',)
    list_display = ('title',) + OrderableAdmin.list_display


class RightMenuAdmin(OrderableAdmin):
    list_display_links = ('title',)
    list_display = ('title',) + OrderableAdmin.list_display


admin.site.register(Menu, MenuAdmin)
admin.site.register(RightMenu, RightMenuAdmin)
