# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.db import models
from easymode.i18n.decorators import I18n
from utilities.capable.models import Orderable
from spring.user import rename


@I18n('title')
class Menu(Orderable):
    title = models.CharField(max_length=256, verbose_name=_('Menu title'))
    tag = models.CharField(max_length=256, verbose_name=_('Menu tag (DO NOT MENYAT)'), blank=True, null=True)


@I18n('title')
class RightMenu(Orderable):
    title = models.CharField(max_length=256, verbose_name=_('Menu title'))
    parent = models.ForeignKey('self', verbose_name=_('Parent'), related_name='child', null=True, blank=True)
    tag = models.CharField(max_length=256, verbose_name=_('Menu tag (DO NOT MENYAT)'), blank=True, null=True)

    def __unicode__(self):
        return self.title