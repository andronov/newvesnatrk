from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import ModelSerializer
from spring.mainmenu.models import Menu, RightMenu
from spring.shops.models import Restaurant, Brand, BrandCategory
from spring.shops.serializers import RestaurantToMenuSerializer, BrandToMenuSerializer, BrandCategoryToMenuSerializer
from utilities.fields import SerializerLocaleField


class MenuSerializer(ModelSerializer):
    title = SerializerLocaleField()
    category_list = SerializerMethodField('get_category_list')
    rest_list = SerializerMethodField('get_rest_list')
    shop_list = SerializerMethodField('get_shop_list')

    def get_category_list(self, obj):
        if obj.tag == 'shops':
            return [BrandCategoryToMenuSerializer(item).data for item in BrandCategory.objects.filter(
                brands__isnull=False
            ).distinct().order_by('id')]
        return None

    def get_rest_list(self, obj):
        if obj.tag == 'restaurants':
            return [RestaurantToMenuSerializer(item).data for item in Restaurant.objects.filter(published=True)]
        return None

    def get_shop_list(self, obj):
        if obj.tag == 'shops':
            return [BrandToMenuSerializer(item).data for item in Brand.objects.filter(published=True)]
        return None

    class Meta:
        model = Menu
        fields = ('id', 'position', 'title', 'category_list', 'rest_list', 'shop_list', 'tag')


class RightMenuSerializer(ModelSerializer):
    title = SerializerLocaleField()
    child = SerializerMethodField('get_child')

    def get_child(self, obj):
        return [RightMenuSerializer(item).data for item in obj.child.all()]

    class Meta:
        model = RightMenu
        fields = ('tag', 'child', 'title')
