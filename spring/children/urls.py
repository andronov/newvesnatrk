from django.conf.urls import patterns, url


urlpatterns = patterns(
    'spring.children.views',
    url(r'^$', 'child_list'),
)
