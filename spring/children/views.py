# -*- coding: utf-8 -*-
from datetime import date
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from spring.entertainments.models import Entertainment
from spring.entertainments.serializers import EntertainmentSerializer
from spring.events.models import BaseEvent
from spring.events.serializers import EventInEntertainmentSerializer


class ChildrenList(ListAPIView):
    model = BaseEvent
    serializer_class = EventInEntertainmentSerializer

    def __init__(self, *args, **kwargs):
        super(ChildrenList, self).__init__(*args, **kwargs)
        self.object_list = None

    def list(self, request, *args, **kwargs):
        self.object_list = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(self.object_list, many=True)

        return Response({
            'object_list': serializer.data,
            'special_events': [
                EventInEntertainmentSerializer(item).data for item in BaseEvent.objects.filter(
                    springevent__is_special=True
                )[:2]
            ],
            'entertainment_list': [
                EntertainmentSerializer(item).data for item in Entertainment.objects.filter(for_children=True)[:2]
            ]
        })

    def get_queryset(self):
        return super(ChildrenList, self).get_queryset().filter(
            for_children=True,
            is_published=True,
            end_date__gte=date.today(),
            pub_date__lte=date.today()
        ).exclude(springevent__is_special=True)
child_list = ChildrenList.as_view()
