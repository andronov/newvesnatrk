# -*- coding: utf-8 -*-

from django.contrib import admin
from spring.seopages.models import Pages, PagesMetadata


class PagesMetadataAdmin(admin.ModelAdmin):
    list_display = ('tag',)


admin.site.register(Pages)
admin.site.register(PagesMetadata, PagesMetadataAdmin)
