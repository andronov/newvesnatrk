from django.db import models


class PagesMetadataManager(models.Manager):
    def get_or_none(self, *args, **kwargs):
        try:
            return super(PagesMetadataManager, self).get(*args, **kwargs)
        except (self.model.DoesNotExist,):
            return None
