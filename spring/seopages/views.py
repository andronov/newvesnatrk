from rest_framework.generics import RetrieveAPIView

from spring.seopages.models import Pages as FlatPage
from spring.seopages.serializers import FlatPageSerializer


class FlatPageDetailView(RetrieveAPIView):
    model = FlatPage
    serializer_class = FlatPageSerializer
    slug_field = 'url'
    slug_url_kwarg = 'url'

    def retrieve(self, request, *args, **kwargs):
        response = super(FlatPageDetailView, self).retrieve(request, *args, **kwargs)

        response.data = {
            'object': response.data
        }
        return response

    def get_object(self, queryset=None):
        try:
            return self.model.objects.get(url=self.slashes_url())
        except self.model.DoesNotExist:
            return None

    def slashes_url(self):
        url = self.kwargs[self.slug_field]
        if not url.startswith('/'):
            url = '/' + url
        if not url.endswith('/'):
            url += '/'
        return url
flat_page = FlatPageDetailView.as_view()