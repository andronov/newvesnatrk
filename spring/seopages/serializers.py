from rest_framework.serializers import ModelSerializer
from spring.seopages.models import PagesMetadata, Pages
from utilities.fields import SerializerLocaleField


class PagesMetadataSerializer(ModelSerializer):
    class Meta:
        model = PagesMetadata


class FlatPageSerializer(ModelSerializer):
    meta_title = SerializerLocaleField()
    title = SerializerLocaleField()
    description = SerializerLocaleField()
    keywords = SerializerLocaleField()
    text = SerializerLocaleField()

    class Meta:
        model = Pages
        fields = ('meta_title', 'title', 'url', 'description', 'keywords', 'text', 'id')
