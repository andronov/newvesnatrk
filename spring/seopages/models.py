# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.db import models
from ckeditor.fields import RichTextField
from easymode.i18n.decorators import I18n
from spring.seopages.managers import PagesMetadataManager
from spring.seopages.validators import validate_system_name
from spring.user import rename


@I18n('meta_title', 'title', 'keywords', 'description', 'text')
class Pages(models.Model):
    meta_title = models.CharField(max_length=255, verbose_name=_('Meta title'))
    title = models.CharField(max_length=255, verbose_name=_('Title'))
    url = models.CharField(max_length=255, verbose_name=_('Url'), unique=True)
    description = models.CharField(max_length=255, verbose_name=_('Description'))
    keywords = models.CharField(max_length=255, verbose_name=_('Keywords'))
    text = RichTextField(verbose_name=_('Text'))

    class Meta:
        verbose_name = _('Seo page')
        verbose_name_plural = _('Seo pages')


class PagesMetadata(models.Model):
    title = models.CharField(max_length=255, verbose_name=_('Title'))
    description = models.CharField(max_length=255, verbose_name=_('Description'))
    tag = models.CharField(
        max_length=255,
        verbose_name=_('Link to page'),
        validators=[validate_system_name],
        unique=True
    )

    class Meta:
        verbose_name = _('Metadata content for built-in pages')
        verbose_name_plural = _('Metadata contents for built-in pages')

    objects = PagesMetadataManager()


def slashes(sender, instance, **kwargs):
    if not instance.url.startswith('/'):
        instance.url = '/' + instance.url
    if not instance.url.endswith('/'):
        instance.url += '/'

models.signals.pre_save.connect(slashes, sender=Pages)