# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import re
from django.core.exceptions import ValidationError


def validate_system_name(value):
    if not re.match(r'^[A-Za-z0-9_]*$', value):
        raise ValidationError(
            'Системное имя может содержать только латинские буквы, цифры и знак подчеркивания',
            code='invalid'
        )