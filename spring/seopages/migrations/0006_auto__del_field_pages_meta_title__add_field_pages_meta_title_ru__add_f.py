# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Pages.meta_title'
        db.delete_column(u'seopages_pages', 'meta_title')

        # Adding field 'Pages.meta_title_ru'
        db.add_column(u'seopages_pages', 'meta_title_ru',
                      self.gf('django.db.models.fields.CharField')(max_length=256, null=True),
                      keep_default=False)

        # Adding field 'Pages.meta_title_en'
        db.add_column(u'seopages_pages', 'meta_title_en',
                      self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'Pages.meta_title'
        raise RuntimeError("Cannot reverse this migration. 'Pages.meta_title' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Pages.meta_title'
        db.add_column(u'seopages_pages', 'meta_title',
                      self.gf('django.db.models.fields.CharField')(max_length=256),
                      keep_default=False)

        # Deleting field 'Pages.meta_title_ru'
        db.delete_column(u'seopages_pages', 'meta_title_ru')

        # Deleting field 'Pages.meta_title_en'
        db.delete_column(u'seopages_pages', 'meta_title_en')


    models = {
        u'seopages.pages': {
            'Meta': {'object_name': 'Pages'},
            'description_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'description_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'keywords_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'keywords_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True'}),
            'meta_title_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'meta_title_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True'}),
            'text_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'text_ru': ('ckeditor.fields.RichTextField', [], {'null': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'})
        }
    }

    complete_apps = ['seopages']