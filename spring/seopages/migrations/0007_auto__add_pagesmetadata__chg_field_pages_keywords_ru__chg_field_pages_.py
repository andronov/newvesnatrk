# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'PagesMetadata'
        db.create_table(u'seopages_pagesmetadata', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('tag', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'seopages', ['PagesMetadata'])


        # Changing field 'Pages.keywords_ru'
        db.alter_column(u'seopages_pages', 'keywords_ru', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

        # Changing field 'Pages.url'
        db.alter_column(u'seopages_pages', 'url', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255))

        # Changing field 'Pages.description_ru'
        db.alter_column(u'seopages_pages', 'description_ru', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

        # Changing field 'Pages.title_en'
        db.alter_column(u'seopages_pages', 'title_en', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

        # Changing field 'Pages.meta_title_ru'
        db.alter_column(u'seopages_pages', 'meta_title_ru', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

        # Changing field 'Pages.description_en'
        db.alter_column(u'seopages_pages', 'description_en', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

        # Changing field 'Pages.meta_title_en'
        db.alter_column(u'seopages_pages', 'meta_title_en', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

        # Changing field 'Pages.title_ru'
        db.alter_column(u'seopages_pages', 'title_ru', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

        # Changing field 'Pages.keywords_en'
        db.alter_column(u'seopages_pages', 'keywords_en', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

    def backwards(self, orm):
        # Deleting model 'PagesMetadata'
        db.delete_table(u'seopages_pagesmetadata')


        # Changing field 'Pages.keywords_ru'
        db.alter_column(u'seopages_pages', 'keywords_ru', self.gf('django.db.models.fields.CharField')(max_length=256, null=True))

        # Changing field 'Pages.url'
        db.alter_column(u'seopages_pages', 'url', self.gf('django.db.models.fields.CharField')(max_length=256, unique=True))

        # Changing field 'Pages.description_ru'
        db.alter_column(u'seopages_pages', 'description_ru', self.gf('django.db.models.fields.CharField')(max_length=256, null=True))

        # Changing field 'Pages.title_en'
        db.alter_column(u'seopages_pages', 'title_en', self.gf('django.db.models.fields.CharField')(max_length=256, null=True))

        # Changing field 'Pages.meta_title_ru'
        db.alter_column(u'seopages_pages', 'meta_title_ru', self.gf('django.db.models.fields.CharField')(max_length=256, null=True))

        # Changing field 'Pages.description_en'
        db.alter_column(u'seopages_pages', 'description_en', self.gf('django.db.models.fields.CharField')(max_length=256, null=True))

        # Changing field 'Pages.meta_title_en'
        db.alter_column(u'seopages_pages', 'meta_title_en', self.gf('django.db.models.fields.CharField')(max_length=256, null=True))

        # Changing field 'Pages.title_ru'
        db.alter_column(u'seopages_pages', 'title_ru', self.gf('django.db.models.fields.CharField')(max_length=256, null=True))

        # Changing field 'Pages.keywords_en'
        db.alter_column(u'seopages_pages', 'keywords_en', self.gf('django.db.models.fields.CharField')(max_length=256, null=True))

    models = {
        u'seopages.pages': {
            'Meta': {'object_name': 'Pages'},
            'description_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'description_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'keywords_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'keywords_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'meta_title_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'meta_title_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'text_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'text_ru': ('ckeditor.fields.RichTextField', [], {'null': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        },
        u'seopages.pagesmetadata': {
            'Meta': {'object_name': 'PagesMetadata'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tag': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['seopages']