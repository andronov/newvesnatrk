# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Pages.description'
        db.delete_column(u'seopages_pages', 'description')

        # Deleting field 'Pages.title'
        db.delete_column(u'seopages_pages', 'title')

        # Deleting field 'Pages.text'
        db.delete_column(u'seopages_pages', 'text')

        # Adding field 'Pages.title_ru'
        db.add_column(u'seopages_pages', 'title_ru',
                      self.gf('django.db.models.fields.CharField')(max_length=256, null=True),
                      keep_default=False)

        # Adding field 'Pages.title_en'
        db.add_column(u'seopages_pages', 'title_en',
                      self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Pages.description_ru'
        db.add_column(u'seopages_pages', 'description_ru',
                      self.gf('django.db.models.fields.CharField')(max_length=256, null=True),
                      keep_default=False)

        # Adding field 'Pages.description_en'
        db.add_column(u'seopages_pages', 'description_en',
                      self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Pages.text_ru'
        db.add_column(u'seopages_pages', 'text_ru',
                      self.gf('ckeditor.fields.RichTextField')(null=True),
                      keep_default=False)

        # Adding field 'Pages.text_en'
        db.add_column(u'seopages_pages', 'text_en',
                      self.gf('ckeditor.fields.RichTextField')(null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'Pages.description'
        raise RuntimeError("Cannot reverse this migration. 'Pages.description' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Pages.description'
        db.add_column(u'seopages_pages', 'description',
                      self.gf('django.db.models.fields.CharField')(max_length=256),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'Pages.title'
        raise RuntimeError("Cannot reverse this migration. 'Pages.title' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Pages.title'
        db.add_column(u'seopages_pages', 'title',
                      self.gf('django.db.models.fields.CharField')(max_length=256),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'Pages.text'
        raise RuntimeError("Cannot reverse this migration. 'Pages.text' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Pages.text'
        db.add_column(u'seopages_pages', 'text',
                      self.gf('ckeditor.fields.RichTextField')(),
                      keep_default=False)

        # Deleting field 'Pages.title_ru'
        db.delete_column(u'seopages_pages', 'title_ru')

        # Deleting field 'Pages.title_en'
        db.delete_column(u'seopages_pages', 'title_en')

        # Deleting field 'Pages.description_ru'
        db.delete_column(u'seopages_pages', 'description_ru')

        # Deleting field 'Pages.description_en'
        db.delete_column(u'seopages_pages', 'description_en')

        # Deleting field 'Pages.text_ru'
        db.delete_column(u'seopages_pages', 'text_ru')

        # Deleting field 'Pages.text_en'
        db.delete_column(u'seopages_pages', 'text_en')


    models = {
        u'seopages.pages': {
            'Meta': {'object_name': 'Pages'},
            'description_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'description_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'text_ru': ('ckeditor.fields.RichTextField', [], {'null': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        }
    }

    complete_apps = ['seopages']