# -*- coding: utf-8 -*-
import datetime
from django.core.validators import MaxValueValidator

from django.db import models
from django.utils.functional import cached_property
from django.utils.translation import ugettext_lazy as _, get_language

from easymode.i18n.decorators import I18n
from filebrowser.fields import FileBrowseField
from ckeditor.fields import RichTextField
from spring.entertainments.models import Entertainment
from spring.gallery.models import GalleryCategory
from spring.shops.models import Brand, Restaurant, Color, ModelDiffMixin


@I18n('title', 'image', 'description', 'short_description')
class BaseEvent(ModelDiffMixin, models.Model):
    title = models.CharField(_('Title'), max_length=254)
    image = FileBrowseField(
        verbose_name=_('Image'),
        directory='event/images/',
        max_length=254,
        null=True,
        default='',
        blank=True)
    description = RichTextField(_('Description'), config_name='big', max_length=1000)
    short_description = RichTextField(_('Short description'), config_name='big', max_length=100)
    pub_date = models.DateField(_('Publication date'), default=datetime.date.today(), db_index=True)
    start_date = models.DateField(_('Start date'), default=datetime.date.today())
    end_date = models.DateField(_('End date'), default=datetime.date.today())
    brand = models.ForeignKey(Brand, verbose_name=_('Brand'), related_name='%(class)ss', null=True, blank=True)
    restaurant = models.ForeignKey(
        Restaurant,
        verbose_name=_('Restaurant'),
        related_name='%(class)ss',
        null=True,
        blank=True
    )
    entertainment = models.ForeignKey(
        Entertainment,
        verbose_name=_('Entertainment'),
        related_name='%(class)ss',
        null=True,
        blank=True
    )
    for_children = models.BooleanField(_('For children'), default=False)
    is_published = models.BooleanField(_('Is published'), default=False)
    created_at = models.DateTimeField(
        _('Created at'),
        default=datetime.datetime.now(),
        auto_now_add=True,
        db_index=True
    )
    is_entertainment = models.BooleanField(_('Is entertainment'), default=False)
    main_event = models.BooleanField(_('Is main event'), default=False)

    @property
    def type(self):
        if hasattr(self, 'promotion'):
            return 'promotion'
        if hasattr(self, 'springevent'):
            return 'spring_event'
        if hasattr(self, 'cinemaevent'):
            return 'cinema_event'
        return 'event'

    @cached_property
    def is_event(self):
        try:
            return bool(self.springevent)
        except SpringEvent.DoesNotExist:
            return False

    @cached_property
    def shop(self):
        return getattr(self, 'brand', None) or getattr(self, 'restaurant', None)

    def get_site_url(self, server_name):
        return 'http://{}/{}/events/{}/'.format(
            server_name,
            get_language(),
            self.pk
        )

    def __unicode__(self):
        return u'{} - {} - {}'.format(
            unicode(self.shop or self.entertainment), self.title, str(self.start_date)
        )

    class Meta:
        ordering = ['main_event', '-start_date']


class CinemaEvent(BaseEvent):
    color = models.ForeignKey(Color, verbose_name=_('Color'), null=True, blank=True)
    
    class Meta:
        verbose_name = _('Cinema event')
        verbose_name_plural = _('Cinema events')


class Promotion(BaseEvent):
    color = models.ForeignKey(Color, verbose_name=_('Color'), related_name='%(class)ss', null=True)
    discount = models.PositiveSmallIntegerField(_('Discount'), validators=[MaxValueValidator(100)], default=0)

    class Meta:
        verbose_name = _('Promotion')
        verbose_name_plural = _('Promotions')

Promotion._meta.get_field('start_date').null = True
Promotion._meta.get_field('start_date').blank = True
Promotion._meta.get_field('end_date').blank = True
Promotion._meta.get_field('end_date').null = True


class SpringEvent(BaseEvent):
    is_cycled = models.BooleanField(_('Cycling event'), default=False)
    every_day_ru = models.CharField(verbose_name=_('Every day') + ' (ru)', default='', blank=True, max_length=255)
    every_day_en = models.CharField(verbose_name=_('Every day') + ' (en)', default='', blank=True, max_length=255)
    background = FileBrowseField(
        verbose_name=_('Background image'),
        directory='event/backgrounds/',
        max_length=254,
        null=True,
        default='',
        blank=True)
    color = models.ForeignKey(Color, verbose_name=_('Color'), null=True, blank=True)
    is_special = models.BooleanField(_('Is special'), default=False)
    gallery = models.ForeignKey(GalleryCategory, verbose_name=_('Gallery'), null=True, blank=True)

    @property
    def show_on_site(self):
        today = datetime.date.today()
        return self.is_published and ((self.end_date >= today and self.pub_date <= today) or self.is_cycled)

    @property
    def every_day(self):
        return getattr(self, 'every_day_{}'.format(get_language()))

    class Meta:
        verbose_name = _('Spring event')
        verbose_name_plural = _('Spring events')

    def __unicode__(self):
        return _('Spring event') + ' - ' + self.title + ' - ' + str(self.start_date)
