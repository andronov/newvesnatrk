import datetime
from django.utils.translation import get_language
from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import ModelSerializer
from spring.events.models import BaseEvent, SpringEvent
from spring.entertainments.serializers import EntertainmentInEventsSerializer
from spring.shops.models import Brand, Restaurant
from spring.shops.serializers import BrandInEventsSerializer, RestaurantInEventSerializer, ColorSerializer
from utilities.fields import SerializerLocaleField


class EventSerializer(ModelSerializer):
    description = SerializerLocaleField()
    type = SerializerMethodField('get_type')
    title = SerializerLocaleField()
    image = SerializerMethodField('get_image')
    short_description = SerializerLocaleField()
    discount = SerializerMethodField('get_discount')
    is_event = SerializerMethodField('get_is_event')
    name = SerializerMethodField('get_name')
    shop = SerializerMethodField('get_shop')
    calendar_image = SerializerMethodField('get_calendar_image')
    logo = SerializerMethodField('get_logo_100')
    is_expired = SerializerMethodField('get_is_expired')
    color = SerializerMethodField('get_color')
    gallery = SerializerMethodField('get_gallery')
    entertainment = EntertainmentInEventsSerializer()

    def get_discount(self, obj):
        if hasattr(obj, 'promotion'):
            return getattr(obj.promotion, 'discount', 0)
        return 0

    def get_gallery(self, obj):
        try:
            if hasattr(obj, 'springevent'):
                return '/about/galleries/{}'.format(
                    obj.springevent.gallery.pk
                )
            return None
        except (AttributeError, ValueError):
            return None

    def get_is_expired(self, obj):
        try:
            return True if obj.end_date < datetime.date.today() else False
        except TypeError:
            return False

    def get_color(self, obj):
        event_type = obj.type.replace('_', '')
        if hasattr(obj, event_type) and getattr(getattr(obj, event_type), 'color', None) is not None:
            return ColorSerializer(getattr(getattr(obj, event_type), 'color')).data
        return None

    def get_logo_100(self, obj):
        try:
            if getattr(obj, 'shop', None) is not None:
                logo = getattr(obj.shop, 'logo_{}'.format(get_language()), None)
            elif getattr(obj, 'entertainment', None) is not None:
                logo = getattr(obj.entertainment, 'internal_page_image', None)
            else:
                return None
            if logo.width >= logo.height:
                return logo.version_generate('entertainment_100_width').url
            elif logo.height > logo.width:
                return logo.version_generate('entertainment_100_height').url
            return logo.url
        except (AttributeError, KeyError, ValueError, IOError):
            return None

    def get_image(self, obj):
        try:
            main_image = getattr(obj, 'image_{}'.format(get_language()))
            if main_image.height > 500:
                return main_image.version_generate('event_list_image_500_height').url
            else:
                return main_image.url
        except (AttributeError, KeyError, ValueError, IOError):
            return ''

    def get_calendar_image(self, obj):
        try:
            calendar_image = getattr(obj, 'image_{}'.format(get_language()))
            if calendar_image.width > 160:
                calendar_image = calendar_image.version_generate('calendar_image_160')
            if calendar_image.height > 90:
                calendar_image = calendar_image.version_generate('calendar_image_90')
            return calendar_image.url
        except (AttributeError, KeyError, ValueError, IOError):
            return ''

    def get_is_event(self, obj):
        return getattr(obj, 'is_event', False)

    def get_type(self, obj):
        return getattr(obj, 'type', 'event')

    def get_shop(self, obj):
        if isinstance(obj.shop, Brand):
            return BrandInEventsSerializer(obj.shop).data
        elif isinstance(obj.shop, Restaurant):
            return RestaurantInEventSerializer(obj.shop).data
        return None

    def get_name(self, obj):
        try:
            return getattr(obj.shop, 'name_{}'.format(get_language()))
        except (AttributeError, KeyError, ValueError):
            return None

    class Meta:
        model = BaseEvent
        fields = (
            'description', 'discount', 'end_date', 'entertainment', 'id', 'image', 'calendar_image', 'is_event', 'name',
            'pub_date', 'shop', 'short_description', 'start_date', 'title', 'type', 'logo', 'is_expired', 'color',
            'gallery'
        )


class EventInEntertainmentSerializer(EventSerializer):
    background = SerializerMethodField('get_background')
    every_day = SerializerMethodField('get_every_day')
    is_cycled = SerializerMethodField('get_is_cycled')

    def get_every_day(self, obj):
        try:
            return obj.springevent.every_day
        except SpringEvent.DoesNotExist:
            return None

    def get_is_cycled(self, obj):
        try:
            return obj.springevent.is_cycled
        except SpringEvent.DoesNotExist:
            return None

    def get_background(self, obj):
        try:
            return obj.springevent.background.url
        except (AttributeError, KeyError, ValueError, SpringEvent.DoesNotExist):
            return ''

    class Meta(EventSerializer.Meta):
        fields = EventSerializer.Meta.fields + ('background', 'every_day', 'is_cycled')
        exclude = ('discount', 'name', 'shop', 'calendar_image', 'color', 'logo', 'is_expired', 'gallery')


class EventInBrandsSerializer(EventSerializer):
    class Meta(EventSerializer.Meta):
        exclude = ('discount', 'pub_date', 'shop', 'calendar_image', 'color', 'logo', 'is_expired', 'gallery')


class EventToContentSerializer(EventSerializer):
    class Meta(EventSerializer.Meta):
        exclude = ('discount', 'calendar_image', 'color', 'logo', 'is_expired', 'gallery')
