# -*- coding: utf-8 -*-
import datetime
from dateutil.relativedelta import relativedelta
from django.db.models import Q
from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.response import Response
from spring.cinema.utilities import get_human_title
from spring.events.models import BaseEvent
from spring.events.serializers import EventSerializer


class EventList(ListAPIView):
    model = BaseEvent
    serializer_class = EventSerializer

    def list(self, request, *args, **kwargs):
        serializer = self.get_serializer(self.get_queryset(), many=True)

        return Response({
            'now_date': {
                'human_title': get_human_title(datetime.date.today()),
                'string_format': str(datetime.date.today())
            },
            'object_list': serializer.data
        })

    def get_queryset(self):
        base_qs = super(EventList, self).get_queryset().filter(is_published=True)
        event_date = self.request.GET.get('date', None)
        search_string = self.request.GET.get('isearch', None)
        month = None
        day = None

        if not event_date:
            day = datetime.date.today()
        else:
            try:
                day = datetime.datetime.strptime(event_date, "%Y-%m-%d")
            except ValueError:
                month = datetime.datetime.strptime(event_date, "%Y-%m")

        filter_dictionary = {}

        if month:
            filter_dictionary.update({
                'end_date__gte': month
            })
            if not event_date:
                filter_dictionary.update({
                    'pub_date__lt': month + relativedelta(months=1)
                })
            else:
                filter_dictionary.update({
                    'start_date__lt': month + relativedelta(months=1)
                })
        elif day:
            filter_dictionary.update({
                'end_date__gte': day
            })
            if not event_date:
                filter_dictionary.update({
                    'pub_date__lte': day
                })
            else:
                filter_dictionary.update({
                    'start_date__lte': day
                })
        qs = base_qs.filter(**filter_dictionary).filter(Q(promotion__isnull=False) | Q(cinemaevent__isnull=False))

        loop_events_qs = base_qs.filter(
            Q(start_date__isnull=True) | Q(end_date__isnull=True)
        )

        if month:
            loop_events_qs = loop_events_qs.filter(
                pub_date__lte=month + relativedelta(months=1)
            )
        else:
            loop_events_qs = loop_events_qs.filter(
                pub_date__lte=day
            )

        spring_events_qs = base_qs.filter(springevent__isnull=False).order_by('-main_event', 'start_date')

        qs = qs | loop_events_qs | spring_events_qs

        if search_string:
            qs = qs.filter(
                Q(promotion__brand__name_ru__icontains=search_string) |
                Q(promotion__brand__name_en__icontains=search_string) |
                Q(promotion__restaurant__name_ru__icontains=search_string) |
                Q(promotion__restaurant__name_en__icontains=search_string)
            )
        return qs
event_list = EventList.as_view()


class EventDetail(RetrieveAPIView):
    model = BaseEvent
    serializer_class = EventSerializer
    
    def retrieve(self, request, *args, **kwargs):
        response = super(EventDetail, self).retrieve(request, *args, **kwargs)
        #ADDITIONAL DATA
        brand = getattr(self.object, 'brand', None)
        restaurant = getattr(self.object, 'restaurant', None)
        is_event = self.object.is_event
        shop = brand or restaurant

        if shop:
            brand_another_events = shop.baseevents.filter(
                is_published=True,
                pub_date__lte=datetime.datetime.today(),
                end_date__gte=datetime.datetime.today()
            ).exclude(pk=self.object.pk)
        else:
            brand_another_events = self.model.objects.filter(
                Q(is_published=True) & Q(
                    Q(
                        pub_date__lte=datetime.datetime.today(),
                        end_date__gte=datetime.datetime.today()
                    ) | Q(
                        springevent__is_cycled=True
                    )
                )
            ).exclude(
                Q(pk=self.object.pk) | Q(springevent=None)
            )

        if is_event:
            brand_another_events = brand_another_events.filter(promotion=None)
        else:
            brand_another_events = brand_another_events.filter(springevent=None)

        another_events = self.model.objects.filter(
            is_published=True,
            pub_date__lte=datetime.datetime.today(),
            end_date__gte=datetime.datetime.today()
        ).exclude(restaurant=restaurant, brand=brand)

        if is_event:
            another_events = another_events.filter(promotion=None)
        else:
            another_events = another_events.filter(springevent=None)

        response.data = {
            'object': response.data,
            'events': [EventSerializer(item).data for item in another_events],
            'brand_events': [EventSerializer(item).data for item in brand_another_events]
        }
        return response
event_detail = EventDetail.as_view()
