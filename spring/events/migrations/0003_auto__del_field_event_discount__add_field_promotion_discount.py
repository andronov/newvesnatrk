# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Event.discount'
        db.delete_column(u'events_event', 'discount')

        # Adding field 'Promotion.discount'
        db.add_column(u'events_promotion', 'discount',
                      self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'Event.discount'
        db.add_column(u'events_event', 'discount',
                      self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0),
                      keep_default=False)

        # Deleting field 'Promotion.discount'
        db.delete_column(u'events_promotion', 'discount')


    models = {
        u'events.baseevent': {
            'Meta': {'object_name': 'BaseEvent'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'baseevents'", 'null': 'True', 'to': u"orm['shops.Brand']"}),
            'description_en': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'description_ru': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True'}),
            'end_date': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2013, 12, 16, 0, 0)'}),
            'for_children': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_en': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'image_ru': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'pub_date': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2013, 12, 16, 0, 0)'}),
            'restaurant': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'baseevents'", 'null': 'True', 'to': u"orm['shops.Restaurant']"}),
            'start_date': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2013, 12, 16, 0, 0)'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'})
        },
        u'events.event': {
            'Meta': {'object_name': 'Event', '_ormbases': [u'events.BaseEvent']},
            u'baseevent_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['events.BaseEvent']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'events.promotion': {
            'Meta': {'object_name': 'Promotion', '_ormbases': [u'events.BaseEvent']},
            u'baseevent_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['events.BaseEvent']", 'unique': 'True', 'primary_key': 'True'}),
            'discount': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'shops.brand': {
            'Meta': {'object_name': 'Brand'},
            'category': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'brands'", 'symmetrical': 'False', 'to': u"orm['shops.BrandCategory']"}),
            'contacts': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '128'}),
            'description_en': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'description_ru': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo_en': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'logo_ru': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'}),
            'number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '128'}),
            'views_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        },
        u'shops.brandcategory': {
            'Meta': {'object_name': 'BrandCategory'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'})
        },
        u'shops.restaurant': {
            'Meta': {'object_name': 'Restaurant'},
            'category': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'restaurants'", 'symmetrical': 'False', 'to': u"orm['shops.RestaurantCategory']"}),
            'contacts': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '128'}),
            'description_en': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'description_ru': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo_en': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'logo_ru': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'}),
            'number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '128'}),
            'views_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        },
        u'shops.restaurantcategory': {
            'Meta': {'object_name': 'RestaurantCategory'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'})
        }
    }

    complete_apps = ['events']