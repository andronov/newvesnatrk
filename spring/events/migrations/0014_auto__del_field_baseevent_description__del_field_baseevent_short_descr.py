# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'BaseEvent.description'
        db.delete_column(u'events_baseevent', 'description')

        # Deleting field 'BaseEvent.short_description'
        db.delete_column(u'events_baseevent', 'short_description')

        # Deleting field 'BaseEvent.title'
        db.delete_column(u'events_baseevent', 'title')

        # Deleting field 'BaseEvent.image'
        db.delete_column(u'events_baseevent', 'image')

        # Adding field 'BaseEvent.title_ru'
        db.add_column(u'events_baseevent', 'title_ru',
                      self.gf('django.db.models.fields.CharField')(max_length=254, null=True),
                      keep_default=False)

        # Adding field 'BaseEvent.title_en'
        db.add_column(u'events_baseevent', 'title_en',
                      self.gf('django.db.models.fields.CharField')(max_length=254, null=True, blank=True),
                      keep_default=False)

        # Adding field 'BaseEvent.image_ru'
        db.add_column(u'events_baseevent', 'image_ru',
                      self.gf('filebrowser.fields.FileBrowseField')(default='', max_length=254, null=True, blank=True),
                      keep_default=False)

        # Adding field 'BaseEvent.image_en'
        db.add_column(u'events_baseevent', 'image_en',
                      self.gf('filebrowser.fields.FileBrowseField')(default='', max_length=254, null=True, blank=True),
                      keep_default=False)

        # Adding field 'BaseEvent.description_ru'
        db.add_column(u'events_baseevent', 'description_ru',
                      self.gf('ckeditor.fields.RichTextField')(max_length=1000, null=True),
                      keep_default=False)

        # Adding field 'BaseEvent.description_en'
        db.add_column(u'events_baseevent', 'description_en',
                      self.gf('ckeditor.fields.RichTextField')(max_length=1000, null=True, blank=True),
                      keep_default=False)

        # Adding field 'BaseEvent.short_description_ru'
        db.add_column(u'events_baseevent', 'short_description_ru',
                      self.gf('ckeditor.fields.RichTextField')(max_length=100, null=True),
                      keep_default=False)

        # Adding field 'BaseEvent.short_description_en'
        db.add_column(u'events_baseevent', 'short_description_en',
                      self.gf('ckeditor.fields.RichTextField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Deleting field 'Event.short_description_en'
        db.delete_column(u'events_event', 'short_description_en')

        # Deleting field 'Event.title_en'
        db.delete_column(u'events_event', 'title_en')

        # Deleting field 'Event.description_en'
        db.delete_column(u'events_event', 'description_en')

        # Deleting field 'Event.image_en'
        db.delete_column(u'events_event', 'image_en')

        # Deleting field 'SpringEvent.every_day_en'
        db.delete_column(u'events_springevent', 'every_day_en')

        # Deleting field 'SpringEvent.title_en'
        db.delete_column(u'events_springevent', 'title_en')

        # Deleting field 'SpringEvent.description_en'
        db.delete_column(u'events_springevent', 'description_en')

        # Deleting field 'SpringEvent.every_day_ru'
        db.delete_column(u'events_springevent', 'every_day_ru')

        # Deleting field 'SpringEvent.short_description_en'
        db.delete_column(u'events_springevent', 'short_description_en')

        # Deleting field 'SpringEvent.image_en'
        db.delete_column(u'events_springevent', 'image_en')

        # Adding field 'SpringEvent.every_day'
        db.add_column(u'events_springevent', 'every_day',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True),
                      keep_default=False)


    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'BaseEvent.description'
        raise RuntimeError("Cannot reverse this migration. 'BaseEvent.description' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'BaseEvent.description'
        db.add_column(u'events_baseevent', 'description',
                      self.gf('ckeditor.fields.RichTextField')(max_length=1000),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'BaseEvent.short_description'
        raise RuntimeError("Cannot reverse this migration. 'BaseEvent.short_description' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'BaseEvent.short_description'
        db.add_column(u'events_baseevent', 'short_description',
                      self.gf('ckeditor.fields.RichTextField')(max_length=100),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'BaseEvent.title'
        raise RuntimeError("Cannot reverse this migration. 'BaseEvent.title' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'BaseEvent.title'
        db.add_column(u'events_baseevent', 'title',
                      self.gf('django.db.models.fields.CharField')(max_length=254),
                      keep_default=False)

        # Adding field 'BaseEvent.image'
        db.add_column(u'events_baseevent', 'image',
                      self.gf('filebrowser.fields.FileBrowseField')(default='', max_length=254, null=True, blank=True),
                      keep_default=False)

        # Deleting field 'BaseEvent.title_ru'
        db.delete_column(u'events_baseevent', 'title_ru')

        # Deleting field 'BaseEvent.title_en'
        db.delete_column(u'events_baseevent', 'title_en')

        # Deleting field 'BaseEvent.image_ru'
        db.delete_column(u'events_baseevent', 'image_ru')

        # Deleting field 'BaseEvent.image_en'
        db.delete_column(u'events_baseevent', 'image_en')

        # Deleting field 'BaseEvent.description_ru'
        db.delete_column(u'events_baseevent', 'description_ru')

        # Deleting field 'BaseEvent.description_en'
        db.delete_column(u'events_baseevent', 'description_en')

        # Deleting field 'BaseEvent.short_description_ru'
        db.delete_column(u'events_baseevent', 'short_description_ru')

        # Deleting field 'BaseEvent.short_description_en'
        db.delete_column(u'events_baseevent', 'short_description_en')

        # Adding field 'Event.short_description_en'
        db.add_column(u'events_event', 'short_description_en',
                      self.gf('ckeditor.fields.RichTextField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.title_en'
        db.add_column(u'events_event', 'title_en',
                      self.gf('django.db.models.fields.CharField')(max_length=254, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.description_en'
        db.add_column(u'events_event', 'description_en',
                      self.gf('ckeditor.fields.RichTextField')(max_length=1000, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.image_en'
        db.add_column(u'events_event', 'image_en',
                      self.gf('filebrowser.fields.FileBrowseField')(default='', max_length=254, null=True, blank=True),
                      keep_default=False)

        # Adding field 'SpringEvent.every_day_en'
        db.add_column(u'events_springevent', 'every_day_en',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True),
                      keep_default=False)

        # Adding field 'SpringEvent.title_en'
        db.add_column(u'events_springevent', 'title_en',
                      self.gf('django.db.models.fields.CharField')(max_length=254, null=True, blank=True),
                      keep_default=False)

        # Adding field 'SpringEvent.description_en'
        db.add_column(u'events_springevent', 'description_en',
                      self.gf('ckeditor.fields.RichTextField')(max_length=1000, null=True, blank=True),
                      keep_default=False)

        # Adding field 'SpringEvent.every_day_ru'
        db.add_column(u'events_springevent', 'every_day_ru',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True),
                      keep_default=False)

        # Adding field 'SpringEvent.short_description_en'
        db.add_column(u'events_springevent', 'short_description_en',
                      self.gf('ckeditor.fields.RichTextField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'SpringEvent.image_en'
        db.add_column(u'events_springevent', 'image_en',
                      self.gf('filebrowser.fields.FileBrowseField')(default='', max_length=254, null=True, blank=True),
                      keep_default=False)

        # Deleting field 'SpringEvent.every_day'
        db.delete_column(u'events_springevent', 'every_day')


    models = {
        u'events.baseevent': {
            'Meta': {'ordering': "['-created_at']", 'object_name': 'BaseEvent'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'baseevents'", 'null': 'True', 'to': u"orm['shops.Brand']"}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 2, 19, 0, 0)', 'auto_now_add': 'True', 'blank': 'True'}),
            'description_en': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'description_ru': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True'}),
            'end_date': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2014, 2, 19, 0, 0)'}),
            'for_children': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_en': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'image_ru': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'is_entertainment': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'pub_date': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2014, 2, 19, 0, 0)'}),
            'restaurant': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'baseevents'", 'null': 'True', 'to': u"orm['shops.Restaurant']"}),
            'short_description_en': ('ckeditor.fields.RichTextField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'short_description_ru': ('ckeditor.fields.RichTextField', [], {'max_length': '100', 'null': 'True'}),
            'start_date': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2014, 2, 19, 0, 0)'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'})
        },
        u'events.event': {
            'Meta': {'ordering': "['-created_at']", 'object_name': 'Event', '_ormbases': [u'events.BaseEvent']},
            u'baseevent_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['events.BaseEvent']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'events.promotion': {
            'Meta': {'ordering': "['-created_at']", 'object_name': 'Promotion', '_ormbases': [u'events.BaseEvent']},
            u'baseevent_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['events.BaseEvent']", 'unique': 'True', 'primary_key': 'True'}),
            'discount': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'events.springevent': {
            'Meta': {'ordering': "['-created_at']", 'object_name': 'SpringEvent', '_ormbases': [u'events.BaseEvent']},
            'background': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            u'baseevent_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['events.BaseEvent']", 'unique': 'True', 'primary_key': 'True'}),
            'color': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['shops.Color']", 'null': 'True', 'blank': 'True'}),
            'every_day': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'is_cycled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_special': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'shops.brand': {
            'Meta': {'ordering': "['place_number', '-pk']", 'object_name': 'Brand'},
            'categories': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'brands'", 'symmetrical': 'False', 'to': u"orm['shops.BrandCategory']"}),
            'color': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'brands'", 'null': 'True', 'to': u"orm['shops.Color']"}),
            'contacts': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'description_en': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'description_ru': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo_en': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'logo_ru': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'}),
            'number': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '256'}),
            'place_number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1000'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '128', 'blank': 'True'}),
            'views_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'work_time': ('django.db.models.fields.CharField', [], {'default': "'10:00 - 19:00'", 'max_length': '13'})
        },
        u'shops.brandcategory': {
            'Meta': {'ordering': "('position',)", 'object_name': 'BrandCategory'},
            'banner': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'banner_url': ('django.db.models.fields.URLField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'for_children': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'}),
            'position': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        },
        u'shops.color': {
            'Meta': {'object_name': 'Color'},
            'circle_color': ('colorful.fields.RGBColorField', [], {'max_length': '7'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'promotion_color': ('colorful.fields.RGBColorField', [], {'max_length': '7'})
        },
        u'shops.restaurant': {
            'Meta': {'ordering': "['place_number', '-pk']", 'object_name': 'Restaurant'},
            'categories': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'restaurants'", 'symmetrical': 'False', 'to': u"orm['shops.RestaurantCategory']"}),
            'color': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'restaurants'", 'null': 'True', 'to': u"orm['shops.Color']"}),
            'contacts': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'description_en': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'description_ru': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo_en': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'logo_ru': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'}),
            'number': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '256'}),
            'place_number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1000'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '128', 'blank': 'True'}),
            'views_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'work_time': ('django.db.models.fields.CharField', [], {'default': "'10:00 - 19:00'", 'max_length': '13'})
        },
        u'shops.restaurantcategory': {
            'Meta': {'ordering': "('position',)", 'object_name': 'RestaurantCategory'},
            'banner': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'banner_url': ('django.db.models.fields.URLField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'}),
            'position': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['events']