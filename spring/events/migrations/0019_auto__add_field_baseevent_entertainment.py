# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'BaseEvent.entertainment'
        db.add_column(u'events_baseevent', 'entertainment',
                      self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='baseevents', null=True, to=orm['entertainments.Entertainment']),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'BaseEvent.entertainment'
        db.delete_column(u'events_baseevent', 'entertainment_id')


    models = {
        u'entertainments.entertainment': {
            'Meta': {'object_name': 'Entertainment'},
            'contacts': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'description_en': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '2000', 'blank': 'True'}),
            'description_ru': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '2000'}),
            'description_short_en': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '500', 'blank': 'True'}),
            'description_short_ru': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '500'}),
            'description_title_en': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '200', 'blank': 'True'}),
            'description_title_ru': ('ckeditor.fields.RichTextField', [], {'default': "''", 'max_length': '200'}),
            'for_children': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'internal_page_image': ('filebrowser.fields.FileBrowseField', [], {'max_length': '254', 'blank': 'True'}),
            'internal_page_image_child': ('filebrowser.fields.FileBrowseField', [], {'max_length': '254', 'blank': 'True'}),
            'place_on_map': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'title_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '128', 'blank': 'True'}),
            'work_time': ('django.db.models.fields.CharField', [], {'default': "'10:00 - 19:00'", 'max_length': '13'})
        },
        u'events.baseevent': {
            'Meta': {'ordering': "['-created_at']", 'object_name': 'BaseEvent'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'baseevents'", 'null': 'True', 'to': u"orm['shops.Brand']"}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 5, 20, 0, 0)', 'auto_now_add': 'True', 'db_index': 'True', 'blank': 'True'}),
            'description_en': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'description_ru': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True'}),
            'end_date': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2014, 5, 20, 0, 0)'}),
            'entertainment': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'baseevents'", 'null': 'True', 'to': u"orm['entertainments.Entertainment']"}),
            'for_children': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_en': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'image_ru': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'is_entertainment': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'pub_date': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2014, 5, 20, 0, 0)', 'db_index': 'True'}),
            'restaurant': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'baseevents'", 'null': 'True', 'to': u"orm['shops.Restaurant']"}),
            'short_description_en': ('ckeditor.fields.RichTextField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'short_description_ru': ('ckeditor.fields.RichTextField', [], {'max_length': '100', 'null': 'True'}),
            'start_date': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2014, 5, 20, 0, 0)'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'})
        },
        u'events.event': {
            'Meta': {'ordering': "['-created_at']", 'object_name': 'Event', '_ormbases': [u'events.BaseEvent']},
            u'baseevent_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['events.BaseEvent']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'events.promotion': {
            'Meta': {'ordering': "['-created_at']", 'object_name': 'Promotion', '_ormbases': [u'events.BaseEvent']},
            u'baseevent_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['events.BaseEvent']", 'unique': 'True', 'primary_key': 'True'}),
            'discount': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'events.springevent': {
            'Meta': {'ordering': "['-created_at']", 'object_name': 'SpringEvent', '_ormbases': [u'events.BaseEvent']},
            'background': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            u'baseevent_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['events.BaseEvent']", 'unique': 'True', 'primary_key': 'True'}),
            'color': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['shops.Color']", 'null': 'True', 'blank': 'True'}),
            'every_day_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'every_day_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'is_cycled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_special': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'shops.brand': {
            'Meta': {'ordering': "['place_number']", 'object_name': 'Brand'},
            'categories': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'brands'", 'symmetrical': 'False', 'to': u"orm['shops.BrandCategory']"}),
            'color': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'brands'", 'null': 'True', 'to': u"orm['shops.Color']"}),
            'contacts': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'description_en': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'description_ru': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo_en': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'logo_ru': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'number': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '256'}),
            'open_soon': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'place_number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1000', 'db_index': 'True'}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'url': ('spring.shops.fields.NullableURLField', [], {'default': 'None', 'max_length': '128', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'views_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'work_time': ('django.db.models.fields.CharField', [], {'default': "'10:00 - 19:00'", 'max_length': '13'})
        },
        u'shops.brandcategory': {
            'Meta': {'ordering': "('position',)", 'object_name': 'BrandCategory'},
            'banner': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'banner_url': ('django.db.models.fields.URLField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'for_children': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iphone_banner': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'}),
            'position': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        },
        u'shops.color': {
            'Meta': {'object_name': 'Color'},
            'circle_color': ('colorful.fields.RGBColorField', [], {'max_length': '7'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'promotion_color': ('colorful.fields.RGBColorField', [], {'max_length': '7'})
        },
        u'shops.restaurant': {
            'Meta': {'ordering': "['place_number']", 'object_name': 'Restaurant'},
            'categories': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'restaurants'", 'symmetrical': 'False', 'to': u"orm['shops.RestaurantCategory']"}),
            'color': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'restaurants'", 'null': 'True', 'to': u"orm['shops.Color']"}),
            'contacts': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'description_en': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'description_ru': ('ckeditor.fields.RichTextField', [], {'max_length': '1000', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo_en': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'logo_ru': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'number': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '256'}),
            'open_soon': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'place_number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1000', 'db_index': 'True'}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'url': ('spring.shops.fields.NullableURLField', [], {'default': 'None', 'max_length': '128', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'views_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'work_time': ('django.db.models.fields.CharField', [], {'default': "'10:00 - 19:00'", 'max_length': '13'})
        },
        u'shops.restaurantcategory': {
            'Meta': {'ordering': "('position',)", 'object_name': 'RestaurantCategory'},
            'banner': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'banner_url': ('django.db.models.fields.URLField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iphone_banner': ('filebrowser.fields.FileBrowseField', [], {'default': "''", 'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True'}),
            'position': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['events']