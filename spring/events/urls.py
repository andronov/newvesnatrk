from django.conf.urls import patterns, url


urlpatterns = patterns(
    'spring.events.views',
    url(r'^$', 'event_list'),
    url(r'^(?P<pk>\d+)/$', 'event_detail'),
)
