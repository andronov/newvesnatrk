# -*- coding: utf-8 -*-
from django.contrib import admin
from spring.events.models import Promotion, SpringEvent, CinemaEvent, BaseEvent


class CinemaEventAdmin(admin.ModelAdmin):
    exclude = ('brand', 'restaurant', 'entertainment')
    ordering = ('-id',)


class SpringEventAdmin(admin.ModelAdmin):
    exclude = ('brand', 'restaurant', 'entertainment')
    ordering = ('-id',)


class EventAdmin(admin.ModelAdmin):
    list_display = ('title', 'pub_date', 'brand')
    list_filter = ('pub_date', 'brand')
    search_fields = ('title_en', 'title_ru', 'pub_date', 'brand__name_en', 'brand__name_ru')

    raw_id_fields = ('brand', 'restaurant', 'entertainment')
    autocomplete_lookup_fields = {
        'fk': ['brand', 'restaurant', 'entertainment'],
    }
    ordering = ('-id',)


admin.site.register(BaseEvent)
admin.site.register(Promotion, EventAdmin)
admin.site.register(SpringEvent, SpringEventAdmin)
admin.site.register(CinemaEvent, CinemaEventAdmin)
