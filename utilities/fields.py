# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os
from urllib2 import unquote
from django.core.exceptions import ValidationError
from django.utils.translation import get_language
from rest_framework.fields import Field, FileField
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.conf import settings


class SerializerLocaleField(Field):
    def field_to_native(self, obj, field_name):
        value = getattr(obj, '{}_{}'.format(field_name, get_language()))
        try:
            value = value.replace('\r\n', '')
        except AttributeError:
            pass
        return self.to_native(value)


class FileBrowseField(FileField):
    def from_native(self, value):
        if value is None or value == '':
            return None
        uploaded_file = default_storage.save(
            'about/presentation/{}'.format(value.name),
            ContentFile(value.read())
        )
        return default_storage.url(uploaded_file).replace('/data/', '')

    def to_native(self, value):
        return default_storage.url(value)

    def validate(self, value):
        super(FileBrowseField, self).validate(value)
        try:
            uploaded_file = default_storage.open(unquote(value))

            if uploaded_file.size > settings.FILEBROWSER_MAX_UPLOAD_SIZE:
                raise ValidationError('Too large size')

            _, extension = os.path.splitext(uploaded_file.name)

            if extension not in ['.pdf', '.ppt', '.jpg', '.png']:
                raise ValidationError('Invalid extension')
        except (IOError, AttributeError):
            return super(FileBrowseField, self).validate(value)
