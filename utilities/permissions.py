from rest_framework.authentication import SessionAuthentication


class SessionDisableCSRFAuthentication(SessionAuthentication):
    def enforce_csrf(self, request):
        pass