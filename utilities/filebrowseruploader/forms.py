from django import forms
from filebrowser.fields import FileBrowseFormField
from filebrowser.widgets import FileInput
from filebrowser.sites import site
from filebrowser.functions import handle_file_upload
from filebrowser.base import FileObject


class FileBrowserModelForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):

        super(FileBrowserModelForm, self).__init__(*args, **kwargs)

        for field_name in self.fields:
            if type(self.fields[field_name]) is FileBrowseFormField:
                self.fields[field_name].widget = FileInput()

    def save(self, commit=True):
        model = super(FileBrowserModelForm, self).save()
        for field in self.fields:
            if type(self.fields[field]) is FileBrowseFormField:
                tmp_file = self.files.get(self.add_prefix(field), None)
                if tmp_file:
                    uploaded_file = FileObject(handle_file_upload(self.fields[field].directory, tmp_file, site))
                    setattr(model, field, uploaded_file)

        if commit:
            model.save()
            #self.save_m2m()

        return model

class FileBrowserForm(forms.Form):

    def __init__(self, *args, **kwargs):

        super(FileBrowserForm, self).__init__(*args, **kwargs)

        for field_name in self.fields:
            if type(self.fields[field_name]) is FileBrowseFormField:
                self.fields[field_name].widget = FileInput()

    def upload(self):
        for field in self.fields:
            if type(self.fields[field]) is FileBrowseFormField:
                tmp_file = self.files.get(self.add_prefix(field), None)
                if tmp_file:
                    uploaded_file = FileObject(handle_file_upload(self.fields[field].directory, tmp_file, site))
                    setattr(self, field, uploaded_file)

        return self