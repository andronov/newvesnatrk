__author__ = 'suhanov_v'

from django.utils.translation import ugettext_lazy as _

from filebrowser.fields import FileObject
from filebrowser.functions import handle_file_upload, get_settings_var, convert_filename, is_selectable
from filebrowser.sites import get_default_site
from filebrowser import signals
from filebrowser.settings import *
import os
import codecs

import uuid

from django.utils.encoding import smart_unicode
from django import forms

# from utilities.uploadrename import post_upload_callback

class FileUploader(object):

    error_messages = {
        'extension': _(u'Extension %(ext)s is not allowed. Only %(allowed)s is allowed.'),
        'one_file': _(u'More than one file uploaded'),
        'is_dir': _('File is a directory')
    }

    def __init__(self, format=None, extensions=None, site=get_default_site()):
        self.site = site
        self.MEDIA_ROOT = MEDIA_ROOT
        self.extensions = extensions
        self.is_uploaded = False
        self.uploaded_file = None
        self.error_message = None

        if format:
            self.format = format or ''
            self.extensions = extensions or EXTENSIONS.get(format)

    def clean(self, value):
        if value == '':
            return value
        file_extension = os.path.splitext(value)[1].lower()
        if self.extensions and not file_extension in self.extensions:
            raise forms.ValidationError(self.error_messages['extension'] % {'ext': file_extension, 'allowed': ", ".join(self.extensions)})
        return value


    def upload(self, request, folder='', filename=None):
        if request.method == "POST":

            if filename:
                filedata = codecs.EncodedFile(request.FILES[filename], "utf-8")
            else:
                filedata = codecs.EncodedFile(request.FILES.values()[0], "utf-8")
            filedata.name = self.get_file_name(filedata.name)

            try:
                filedata.name = self.clean(filedata.name)
            except forms.ValidationError, e:
                self.error_message = e.messages[0]
                return self

            path = os.path.join(self.MEDIA_ROOT, folder)
            file_name = os.path.join(path, filedata.name)
            file_already_exists = self.site.storage.exists(file_name)

            # Check for name collision with a directory
            if file_already_exists and self.site.storage.isdir(file_name):
                self.error_message = self.error_messages['is_dir']
                return self

            signals.filebrowser_pre_upload.send(sender=request, path=request.POST.get('folder'), file=filedata, site=self.site)
            uploadedfile = handle_file_upload(path, filedata, site=self.site)

            if file_already_exists and OVERWRITE_EXISTING:
                old_file = smart_unicode(file_name)
                new_file = smart_unicode(uploadedfile)
                self.site.storage.move(new_file, old_file, allow_overwrite=True)
            else:
                file_name = smart_unicode(uploadedfile)

            signals.filebrowser_post_upload.send(sender=request, path=request.POST.get('folder'), file=FileObject(smart_unicode(file_name), site=self.site), site=self.site)


            self.uploaded_file = FileObject(os.path.join(folder, filedata.name))
            self.is_uploaded = True
            return self

    def get_file_name(self, filename):
        ext = filename.split('.')[-1]
        filename = "%s.%s" % (uuid.uuid4(), ext)
        return filename

    def get_uploaded_file(self):
        return self.uploaded_file