import os
from filebrowser import settings, signals
from pytils.translit import slugify


def post_upload_callback(sender, **kwargs):
    upload_dir = os.path.dirname(kwargs['file'].path)
    old_name = os.path.join(settings.MEDIA_ROOT, upload_dir, kwargs['file'].filename)
    new_name = os.path.join(settings.MEDIA_ROOT, upload_dir, slugify(kwargs['file'].filename) + kwargs['file'].extension)
    os.rename(old_name, new_name)

signals.filebrowser_post_upload.connect(post_upload_callback)
