from rest_framework.renderers import JSONPRenderer


class AngularJSONPRenderer(JSONPRenderer):
    default_callback = 'angular.callbacks._0'